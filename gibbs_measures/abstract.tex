\section*{Introdução}

  \begin{abstract}
    A forma mais comum de se especificar um espaço de medida
    é definindo uma função de conjunto $\sigma$-aditiva
    em uma álgebra, semianel, classe compacta, etc.
    e através de variantes
    do \emph{Teorema de Extensão de Carathéodory},
    estendendo essa função para a $\sigma$-álgebra gerada.
    A ideia é especificar o tamanho dos \emph{``retângulos''}
    e obter uma medida em toda a $\sigma$-álgebra.

    As famosas \emph{medidas de Gibbs},
    utilizadas na \emph{mecânica estatística},
    são especificadas de outra maneira.
    Do ponto de vista físico,
    conhecemos o comportamento da medida de probabilidade
    quando fixamos uma condição de contorno.
    Não conhecemos o \emph{``tamanho''} dos elementos de um semianel gerador.
    Mas conhecemos \emph{medidas condicionais},
    onde o condicionamento corresponde às condições de contorno.
    E esperamos poder determinar distribuições de probabilidade
    associadas a essas \emph{medidas condicionais}.
    Abordaremos os capítulos $1$, $2$ e $4$ de \cite{georgii},
    mas em nosso tratamento,
    enfatizamos um pouco mais o ponto de vista funcional.
  \end{abstract}


\section*{Pré-Requesitos}

  É desejável conhecimento básico de
  \emph{Teoria da Medida e Probabilidade}.
  Em especial,
  com relação aos conceitos de
  \emph{$\sigma$-álgebra},
  \emph{$\sigma$-aditividade},
  \emph{função mensurável},
  \emph{integral}
  e
  \emph{esperança condicional}.
  Veja
  \cite{jesus,sheldon_ross}.


\section*{Objetivos}

  Dada uma medida de probabilidade $\function{\mu}{\family{B}}{[0,1]}$
  e uma sub-$\sigma$-álgebra $\family{F} \subset \family{B}$,
  a \emph{esperança condicional}
  $\conditionalexpectation[\mu]{\,\cdot}{\family{F}}$
  pode ser vista como uma aplicação linear
  \begin{equation*}
    \functionarray{\conditionalexpectation[\mu]{\,\cdot}{\family{F}}}
                  {\integrable{\family{B}}}{\integrable{\family{F}}}
                  {f}{\conditionalexpectation[\mu]{f}{\family{F}}}
  \end{equation*}
  que projeta funções $\mu$-integráveis $\family{B}$-mensuráveis
  a menos de equivalência $\mu$-qtp,
  em funções $\family{F}$-mensuráveis.
  A \emph{esperança condicional} possui as seguintes propriedades.
  \begin{enumerate}
    %% \item
    %%   Se
    %%   $f \in \integrable{\family{F}}$,
    %%   \begin{equation*}
    %%     f
    %%     =
    %%     \conditionalexpectation[\mu]{f}{\family{F}}.
    %%   \end{equation*}

    \item
      Se
      $f \in \integrable{\family{F}}$
      e
      $g \in \integrable{\family{B}}$,
      então
      \begin{equation*}
        \conditionalexpectation[\mu]{fg}{\family{F}}
        =
        f
        \cdot
        \conditionalexpectation[\mu]{g}{\family{F}}.
      \end{equation*}

    \item
      Se $\family{F} \subset \family{G} \subset \family{B}$,
      então
      \begin{equation*}
        \conditionalexpectation[\mu]{\conditionalexpectation[\mu]{\,\cdot}{\family{G}}}{\family{F}}
        =
        \conditionalexpectation[\mu]{\,\cdot}{\family{F}}.
      \end{equation*}
  \end{enumerate}

  O objetivo deste minicurso
  é mostrar que podemos especificar medidas de probabilidade utilizando essas propriedades.
  Ou seja,
  consideramos uma família de sub-$\sigma$-álgebras
  $\family{F}_\Lambda \subset \family{B}$
  ($\Lambda \in \family{S}$)
  e uma família de operadores (\emph{núcleos de probabilidade})
  $\function{\gamma_\Lambda}{\family{M}_\family{B}}{\family{M}_\Lambda}$
  que leva funções
  $\family{B}$-mensuráveis
  em funções
  $\family{F}_\Lambda$-mensuráveis,
  satisfazendo, dentre outras propriedades,
  \begin{enumerate}
    \item
      para
      $f \in \family{M}_\Lambda$
      e
      $g \in \family{M}_\family{B}$,
      \begin{equation*}
        \gamma_\Lambda(fg)
        =
        f \cdot \gamma_\Lambda(g);\text{ e}
      \end{equation*}

    \item
      para
      $\family{F}_\Lambda \subset \family{F}_\Gamma$,
      \begin{equation*}
        \gamma_\Lambda
        =
        \gamma_\Lambda \circ \gamma_\Gamma.
      \end{equation*}
  \end{enumerate}
  Então,
  estudamos a existência medidas de probabilidade
  $\function{\mu}{\family{B}}{[0,1]}$
  tal que para toda $f$ $\mu$-integrável,
  \begin{equation*}
    \gamma_\Lambda(f) = \conditionalexpectation[\mu]{f}{\family{F}_\Lambda}
    \quad(\text{$\mu$-qtp})
  \end{equation*}
  para todo $\Lambda \in \family{S}$.

  Dizemos que a família
  $\gamma = (\gamma_\Lambda)_{\Lambda \in \family{S}}$
  é uma \emph{especificação}.
  As técnicas desenvolvidas serão utilizadas para a construção das \emph{medidas de Gibbs},
  que são um caso especial das chamadas
  \emph{$\lambda$-especificações}.
  A \emph{$\lambda$-especificação},
  por sua vez,
  é um caso especial de \emph{especificação} construído
  através da modificação de uma especificação mais simples
  $\lambda = (\lambda_\Lambda)_{\Lambda \in \mathscr{S}}$.


\section*{Sumário}
  \begin{enumerate}
    \item Conceitos Básicos
      \begin{enumerate}
        \item Medida e $\sigma$-álgebra
        \item Funções Mensuráveis e Integral
        \item Medida como Funcional
        \item Esperança Condicional
        \item Medida Condicional
        \item Diferentes Manerias de Especificar Medidas
      \end{enumerate}

    \item Especificação com Núcleos de Probabilidade
      \begin{enumerate}
        \item Núcleos de Probabilidade
        \item Núcleos como Operadores
        \item Composição de Núcleos
        \item Espaço de \emph{Spins}
        \item Especificações
        \item Exemplo: $\lambda_\Lambda$
        \item Existência de Medida de Gibbs
      \end{enumerate}

    \item Especificação Gibbsiana
      \begin{enumerate}
        \item $\lambda$-Especificação
        \item Condição de Consistência
        \item Potenciais Gibbsianos
        \item Existência de Medida de Gibbs
      \end{enumerate}
  \end{enumerate}

\begin{thebibliography}{99}
  \bibitem{jesus}
    FERNANDEZ, P.~J.
    \emph{Medida e Integração}.
    Projeto Euclides, 2ª edição.
    IMPA, 1996.

  \bibitem{georgii}
    GEORGII, H.~O.
    \emph{Gibbs Measures and Phase Transitions}.
    De Gruyter studies in mathematics, v. 9.
    De Gruyter, 1988.

  \bibitem{sheldon_ross}
    ROSS, S.~M.
    \emph{A First Course in Probability}.
    Prentice Hall, 1998.
\end{thebibliography}
