\section{Matrizes como Funções}

  Uma matriz $p \times q$
  \begin{equation*}
    A
    =
    \matrixrepresentation{\vector{a_1} \cdots \vector{a_q}}
  \end{equation*}
  pode ser vista como uma função
  \begin{equation*}
    \functionarray{T_A}{\reals^q}{\reals^p}{(x_1, \dotsc, x_q)}{x_1 \vector{a_1} + \dotsb + x_q \vector{a_q}}.
  \end{equation*}
  E como toda função,
  pode ser vista como um \emph{``dispositivo''}{\ldots}
  uma \emph{``caixinha''}.
  Se pensarmos nos espaços $\reals^p$ e $\reals^q$ como matrizes coluna,
  a transformação $T_A$ fica ainda mais parecida com as \emph{``caixinhas''}.
  \begin{equation*}
    \matrixrepresentation{T_A \vector{v}}
    =
    A
    \matrixrepresentation{\vector{v}}.
  \end{equation*}

  \nftikzpic{01/02/figs/matrix_as_a_function.tikz}

  \begin{example}
    A matriz da tabela nutricional do \cref{example:potato_and_carrot_as_vectors}
    pode ser usada para construir um dispositivo{\ldots}
    um \emph{APP}!!!
    Se nosso \emph{app} bombar{\ldots} vamos ficar ricos!!!
    Veja o projeto do \emph{app} na \cref{figure:nutrition_app}.

    Você coloca a quantidade de batatas e cenouras
    e o \emph{nutrition app} mostra pra você a quantidade de
    nutrientes X, Y e Z.

    \nftikzpic[figure:nutrition_app]
        {01/02/figs/nutrition_app.tikz}
  \end{example}


  \begin{intriguing question}
    \label{question:has_matrix_representation}
    Dada uma função
    $\function{T}{\reals^q}{\reals^p}$,
    existe um critério simples pra saber se esta função
    pode ser representada por uma matriz $p \times q$?
  \end{intriguing question}

  Suponha que por algum motivo saibamos que a função
  $\function{T}{\reals^q}{\reals^p}$
  pode ser representada por uma matriz.
  Nesse caso,
  chamamos $T$ de \emph{transformação linear}.
  Que matriz é essa?

  Repare que dada a matriz
  $A = \matrixrepresentation{\vector{a_1} \cdots \vector{a_q}}$,
  a primeira coluna da $A$
  \begin{equation*}
    \matrixrepresentation{\vector{a_1}}
    =
    A
    \matrixrepresentation{1 \\ 0 \\ \vdots \\ 0}
  \end{equation*}
  é igual ao produto de $A$
  pelo primeiro vetor da base canônica,
  $\matrixrepresentation{\vector{e_1}}$.
  De modo geral,
  \begin{equation*}
    \matrixrepresentation{\vector{a_j}}
    =
    A
    \matrixrepresentation{\vector{e_j}}.
  \end{equation*}

  Assim,
  a matriz correspondente à transformação $T$ é dada por
  \begin{equation*}
    \matrixrepresentation{T}
    =
    \matrixrepresentation{T\vector{e_1} \cdots T\vector{e_q}}.
  \end{equation*}
  Em outras palavras,
  é a matriz cuja $j$-ésima coluna é dada pelo vetor
  $T\vector{e_j}$.
  Dessa forma,
  sabendo que $T$ é uma \emph{transformação linear},
  é \emph{``fácil''} determinar a matriz correspondente
  $\matrixrepresentation{T}$:
  é a matriz cuja primeira coluna
  é dada por $T\vector{e_1}$,
  a segunda é dada por $T\vector{e_2}$,
  etc.

  \begin{example}
    \label{example:table_composition:soup}
    Suponha agora
    que lá na cidade de \emph{fimdomundo}
    tem uma loja que vende dois tipos de sopa.
    A sopa de verão, com mais cenoura
    (pra ajudar no bronzeado),
    e a sopa de inverno, com mais batata
    (pra reserva de lipídios, pra proteção contra o frio).
    Do \cref{example:potato_and_carrot_as_vectors},
    sabemos a quantidade de nutrientes em cada grama de batata e cada grama de cenoura.
    A quantidade de batata e cenoura por tijela de sopa
    também pode ser representada em uma tabela.

    \begin{center}
      \begin{tabular}{lcc}
        \firsthline
        & \textbf{verão} & \textbf{inverno}
        \\\hline
        \textbf{batata}  & 10 & 20
        \\
        \textbf{cenoura} & 30 & 15
        \\\lasthline
      \end{tabular}
    \end{center}

    Ou então,
    uma matriz:
    \begin{equation*}
      S
      =
      \matrixrepresentation{10 & 20 \\ 30 & 15}.
    \end{equation*}
    Podemos encarar $S$ como uma função de $\reals^2$ em $\reals^2$,
    que nos diz a quantidade de batatas e cenouras correspondente
    a determinadas quantidade de sopas de verão e inverno.

    \nftikzpic{01/02/figs/soup_app.tikz}

    Agora,
    se você quisesse saber a quantidade todal de cada nutriente
    nas sopas que você comprou,
    você pode abrir o \emph{soup app},
    ver a quantidade de batatas e cenouras,
    copiar--e--colar o resultado no \emph{nutri app}
    para saber a quantidade de nutrientes.
    Ou{\ldots}
    você pode baixar o \emph{soup nutri app}!!!
    Para fazer esse \emph{app},
    podemos fazer um programa que pegue as quantidades de sopa
    (\emph{input} do usuário),
    aplique na matriz $S$
    e depois aplique a matriz $N$.
    Mas será que não existe uma matriz $P$ que faça os cálculos de uma vez só?

    Se existir a matriz $P$,
    sua primeira coluna deve ser o vetor que representa a quantidade de nutrientes
    em uma tijela de sopa de verão.
    Lembre-se que a quantidade de batatas e cenouras na tijela de sopa de verão é
    $S\matrixrepresentation{\vector{e_1}}$.
    Ou seja,
    a primeira coluna deve ser igual a $N(S\matrixrepresentation{\vector{e_1}})$.
    A segunda coluna deve ser igual a $N(S\matrixrepresentation{\vector{e_2}})$.
    Ora,
    mas pela \cref{equation:matrix_product_with_ej},
    isso significa que $P = NS$.

    \begin{center}
      \begin{tabular}{lcc}
        \firsthline
        & \textbf{verão} & \textbf{inverno}
        \\\hline
        \textbf{nutriente X} & 480 & 285
        \\
        \textbf{nutriente Y} & 310 & 305
        \\
        \textbf{nutriente Z} & 410 & 280
        \\\lasthline
      \end{tabular}
    \end{center}
  \end{example}

  No \cref{example:table_composition:soup},
  vimos que o produto de matrizes $NS$ nada mais é do que a tabela que representa
  o \emph{dispositivo} resultante de se concatenar os \emph{dispositivos} $S$ e $N$.
  A pergunta que falta responder é:

  \begin{intriguing question}
    \label{question:composition_of_matrix_is_matrix}
    Considere as transformações
    $T_N$ e $T_S$ correspondentes às matrizes $N$ e $S$.
    Será que $T_N \circ T_S$ é uma função que pode ser representada por uma matriz?
  \end{intriguing question}

  \begin{example}
    A função
    \begin{equation*}
      \functionarray{f}{\reals^2}{\reals^2}{(x,y)}{(x+2, x+y)}
    \end{equation*}
    não pode ser representada por uma matriz.
    Para qualquer matriz $M$ de tamanho $2 \times 2$,
    é verdade que
    \begin{equation*}
      M
      \matrixrepresentation{0 \\ 0}
      =
      (0,0)
      \neq
      (2,0)
      =
      f(0,0).
    \end{equation*}
    De modo geral,
    toda função $f$ que pode ser representada por uma mariz tem que satisfazer
    $f(\vector{0}) = \vector{0}$.
  \end{example}

  \begin{example}
    A função
    \begin{equation*}
      \functionarray{f}{\reals^2}{\reals^2}{(x,y)}{(xy, x+y)}
    \end{equation*}
    não pode ser representada por uma matriz.

    Note que toda matriz $M$ de tamanho $2 \times 2$ satisfaz o seguinte:
    \begin{equation*}
      M
      \matrixrepresentation{2 \\ 2}
      =
      2
      M
      \matrixrepresentation{1 \\ 1}.
    \end{equation*}
    No entanto,
    \begin{equation*}
      f(2,2)
      =
      (4,4)
      \neq
      2
      (1,2)
      =
      f(1,1).
    \end{equation*}
  \end{example}
