\section{Funções Representadas por Matrizes}

  \begin{quote}
    Por que acreditamos que basta uma tabela para representarmos
    as quantidades de nutrientes em uma determinada quantidade de batatas e cenouras?
  \end{quote}

  \begin{quote}
    Por que acreditamos que basta uma tabela para sabermos os custos de produção
    de uma certa quantidade de bicicletas e motos?
  \end{quote}

  \begin{example}
    \label{example:non_linear_price}
    O preço de certos produtos pode ser representado por uma tabela?
    Por exemplo{\ldots}

    \begin{center}
      \begin{tabular}{lcccc}
        \firsthline
        & \textbf{batata} & \textbf{cenoura} & \textbf{inhame} & \textbf{couve}
        \\\hline
        \textbf{Preço (R\$)} & 3 & 5 & 7 & 10
        \\\lasthline
      \end{tabular}
    \end{center}

    Chegando lá na quitanda o preço da batata está assim:
    \begin{quote}
      Levando mais de um Kg de batata,
      você tem 10\% de desconto.
    \end{quote}

    Se levar 500g de batata, 500g de cenoura,
    500g de inhame e 250g de couve,
    sai tudo por \textbf{R\$ 9}.
    E se levar 1Kg de batata, 1Kg de cenoura,
    1Kg de inhame e 500g de couve,
    sai tudo por \textbf{R\$ 15}.

    Neste caso,
    a função preço não pode ser representada por uma matriz
    no espírito do que estamos fazendo até agora.
  \end{example}

  O que torna o \cref{example:non_linear_price}
  diferente daquilo que temos feito até agora,
  é a falta de consistência
  entre os preços dos diferentes \emph{``pacotes''} de legumes e verduras.
  Por consistência,
  queremos dizer que o preço de uma combinação qualquer de legumes e verduras
  deve ser igual à combinação dos preços.
  A combinação de $x$ quilos de batata,
  $y$ quilos de cenoura,
  $z$ quilos de inhame
  e
  $w$ quilos de couve
  pode ser expressa pelo vetor $(x,y,z,w) \in \reals^4$.
  Mas pode ser expressa também na base canônica
  \begin{equation*}
    (x,y,z,w)
    =
    x \vector{e_1}
    +
    y \vector{e_2}
    +
    z \vector{e_3}
    +
    w \vector{e_4}.
  \end{equation*}
  Se $\function{T}{\reals^4}{\reals^1}$
  é uma função que tem a \emph{``consistência desejada''},
  então,
  \begin{equation*}
    T
    (
      x \vector{e_1}
      +
      y \vector{e_2}
      +
      z \vector{e_3}
      +
      w \vector{e_4}
    )
    =
    x T\vector{e_1}
    +
    y T\vector{e_2}
    +
    z T\vector{e_3}
    +
    w T\vector{e_4}.
  \end{equation*}
  O preço da combinação é a combinação dos preços!
  Nesse caso,
  dizemos que $T$ é uma \emph{transformação linear}.

  Agora,
  ficar falando de
  $\vector{e_1}$,
  $\vector{e_2}$,
  $\vector{e_3}$,
  {etc.}
  pode ser meio inconveniente.
  Mais do que combinações dos vetores da base canônica,
  podemos falar da combinação de quaisquer vetores.

  \begin{example}
    Se o preço na quitanda não tem complicações
    como no \cref{example:non_linear_price}.
    Ou seja,
    se o preço é \emph{linear}.
    Então,
    se Vinícius encheu uma sacola
    com quantidade de itens representada pelo vetor $\vector{v}$,
    e se Walter encheu uma sacola $\vector{w}$,
    Vinícius pagará $T\vector{v}$ e Walter pagará $T\vector{w}$.

    Se Gabriel quer comprar o equivalente a
    $5$ sacolas de Vinícius e $3.5$ sacolas de Walter,
    ou seja,
    $\vector{g} = 5\vector{v} + 3.5\vector{w}$,
    então Gabriel pagará
    \begin{equation*}
      T(\vector{g})
      =
      T(5\vector{v} + 3.5\vector{w})
      =
      5 T\vector{v} + 3.5 T\vector{w}.
    \end{equation*}
    Mas se Cinthia,
    a mãe de Gabriel,
    disser que tem muita coisa no carrinho de compras,
    e disser pra ele remover o equivalente a uma sacola de Walter,
    então Gabriel pagará
    \begin{equation*}
      T(\vector{g} - \vector{w})
      =
      T\vector{g} - T\vector{w}.
    \end{equation*}
    Ou seja,
    $T\vector{w}$ a menos do que pagaria
    se sua mão não tivesse pedido pra remover uma sacola do tipo $\vector{w}$.
  \end{example}

  \begin{definition}[linearidade em espaços $\reals^n$]
    \label{definition:linear_transform:rn}
    Uma função
    \begin{equation*}
      \function{T}{\reals^p}{\reals^q}
    \end{equation*}
    é uma \emph{transformação linear} quando
    \begin{enumerate}
      \item
        $\vector{v}, \vector{w} \in \reals^p$
        $\Rightarrow$
        $T(\vector{v} + \vector{w}) = T\vector{v} + T\vector{w}$.

      \item
        $\alpha \in \reals$,
        $\vector{v} \in \reals^p$
        $\Rightarrow$
        $T(\alpha \vector{v}) = \alpha T\vector{v}$.
    \end{enumerate}
  \end{definition}

  Existem várias maneiras de entender o que é uma \emph{transformação linear}.
  Vamos resumir isso em uma \emph{proposição}.
  Mas antes, precisamos de algumas definições.

  \begin{definition}[combinação linear]
    \label{definition:linear_combination}
    Seja $B \subset \reals^n$ um conjunto de vetores.
    O vetor $\vector{v}$
    é uma \emph{combinação linear} de elementos de $B$
    quando existirem
    $\vector{b_1}, \dotsc, \vector{b_k} \in B$
    e
    $\alpha_1, \dotsc, \alpha_k \in \reals$
    tais que
    \begin{equation*}
      \vector{v}
      =
      \alpha_1 \vector{b_1}
      + \dotsb +
      \alpha_k \vector{b_k}.
    \end{equation*}
  \end{definition}

  \begin{definition}[conjunto gerador]
    Um subconjunto $B \subset \reals^n$ é chamado de \emph{gerador}
    quando \emph{todo} $\vector{v} \in \reals^n$ pode ser escrito
    como combinação linear de elementos de $B$.
  \end{definition}

  \begin{proposition}[caracterizações de linearidade]
    \label{proposition:linearity:rn}
    Considere
    $\function{T}{\reals^p}{\reals^q}$.
    As condições a seguir são equivalentes.
    \begin{enumerate}
      \item
        \label{it:proposition:linearity:rn:linear}
        $T$ é linear (\cref{definition:linear_transform:rn}).

      \item
        \label{it:proposition:linearity:rn:one_liner}
        $a \in \reals$,
        $\vector{v}, \vector{w} \in \reals^p$
        $\Rightarrow$
        $T(a \vector{v} + \vector{w}) = a T\vector{v} + T\vector{w}$.

      \item
        \label{it:proposition:linearity:rn:linear_combination}
        Para toda combinação linear
        $\vector{v} = \alpha_1 \vector{v_1} + \dotsb + \alpha_k \vector{v_k}$
        de elementos de $\reals^p$,
        vale que
        \begin{equation*}
          T(\vector{v})
          =
          \alpha_1 T\vector{v_1}
          + \dotsb +
          \alpha_k T\vector{v_k}.
        \end{equation*}

      \item
        \label{it:proposition:linearity:rn:linear_combination_restricted}
        Para algum $B \subset \reals^p$ gerador
        vale que
        \begin{equation*}
          T(\vector{v})
          =
          \alpha_1 T\vector{b_1}
          + \dotsb +
          \alpha_k T\vector{b_k}.
        \end{equation*}
        para toda combinação linear
        $\vector{v} = \alpha_1 \vector{b_1} + \dotsb + \alpha_k \vector{b_k}$
        de elementos de $B$.

      \item
        \label{it:proposition:linearity:rn:canonical_basis}
        Para qualquer $(v_1, \dotsc, v_p) \in \reals^p$,
        \begin{equation*}
          T(v_1, \dotsc, v_p)
          =
          v_1 T\vector{e_1}
          + \dotsb +
          v_p T\vector{e_p}.
        \end{equation*}

      \item
        \label{it:proposition:linearity:rn:matrix_representation}
        Existem $a_1, \dotsc, a_p \in \reals^q$ tais que
        \begin{equation*}
          T(v_1, \dotsc, v_p)
          =
          v_1 \vector{a_1}
          + \dotsb +
          v_p \vector{a_p}.
        \end{equation*}
    \end{enumerate}
  \end{proposition}

  TODO: comentários.

  \begin{proof}
    TODO: demonstrar!
  \end{proof}

  \begin{example}
    Se
    $A = \matrixrepresentation{\vector{a_1} \cdots \vector{a_q}}$
    é uma matriz $p \times q$,
    \begin{equation*}
      \function{T_A}{\reals^q}{\reals^p}
               {(v_1, \dotsc, v_q)}{v_1\vector{a_1} + \dotsb + v_q\vector{a_q}}
    \end{equation*}
    é a \emph{transformação linear} associada a $A$.
    Assim,
    o \cref{it:proposition:linearity:rn:matrix_representation}
    da \cref{proposition:linearity:rn}
    afirma essencialmente que dizer que uma função
    $\function{f}{\reals^q}{\reals^p}$
    é linear é a mesma coisa que dizer que pode ser representada por uma matriz $p \times q$.

    Dessa forma,
    a \cref{proposition:linearity:rn} responde à \cref{question:has_matrix_representation}.
  \end{example}


  E respondendo a uma pergunta intrigante{\ldots}

  \begin{proposition}
    \label{proposition:composition_of_linear_is_linear}
    Se
    $\function{f}{\reals^p}{\reals^q}$
    e
    $\function{g}{\reals^r}{\reals^p}$
    são lineares,
    então
    $f \circ g$ também é linear.
  \end{proposition}

  \begin{proof}
    Vamos verificar o \cref{it:proposition:linearity:rn:one_liner}
    da \cref{proposition:linearity:rn}.

    Sejam $a \in \reals$
    e
    $\vector{v}, \vector{w} \in \reals^r$.
    Então,
    \begin{align*}
      (f \circ g)(a \vector{v} + \vector{w})
      &=
      f(g(a \vector{v} + \vector{w}))
      \\
      &=
      f(a g(\vector{v}) + g(\vector{w}))
      \\
      &=
      a f(g(\vector{v})) + f(g(\vector{w}))
      \\
      &=
      a (f \circ g)(\vector{v}) + (f \circ g)(\vector{w}).
    \end{align*}
    E portanto,
    $f \circ g$ é linear.
  \end{proof}

  A \cref{proposition:composition_of_linear_is_linear}
  nos permite responder à \cref{question:composition_of_matrix_is_matrix}.
  Consequentemente,
  juntando isso com o comentário que vem depois
  da \cref{question:has_matrix_representation}
  e antes do \cref{example:table_composition:soup},
  podemos concluir que se $A$ é uma matriz $p \times q$
  e $B$ é uma matriz $q \times r$,
  então
  \begin{equation*}
    T_A \circ T_B
    =
    T_{AB}.
  \end{equation*}
  Ou seja,
  $AB$ é a matriz que representa a composição das transformações
  $T_A$ e $T_B$.
