\subsection{Matriz Inversa}

  Para cada $n = 1, 2, \dotsc$,
  existe uma matriz $n \times n$ especial,
  chamada de matriz identidade.
  É a matriz
  \begin{equation*}
    I_n
    =
    \matrixrepresentation
        {
          1 & 0 & \cdots & 0
          \\
          0 & 1 & \cdots & 0
          \\
          \vdots & \vdots & \ddots & \vdots
          \\
          0 & 0 & \cdots & 1
        },
  \end{equation*}
  que tem todas as entradas da \emph{diagonal principal}
  iguais a $1$, e as demais iguais a $0$.
  Por comodidade,
  é comum omitirmos o índice $n$ e escrevermos apenas $I$
  para denotar a matriz identidade de tamanho adequado ao contexto.

  A matriz identidade pode ser caracterizada como a única matriz tal que
  \begin{equation*}
    \matrixrepresentation{\vector{v}}
    =
    I_n
    \matrixrepresentation{\vector{v}}
  \end{equation*}
  para todo $\vector{v} \in \reals^n$.
  Ou então,
  \begin{equation*}
    A = I_n A
    \quad\text{e}\quad
    B = B I_n
  \end{equation*}
  para toda matriz $A$ de tamanho $n \times p$,
  e toda matriz $B$ de tamanho $q \times n$,
  para $p$ e $q$ quaisquer.

  Para representar as entradas da matriz identidade,
  usamos o chamado \emph{delta de Kronecker}:
  \begin{equation*}
    I = \matrixrepresentation{\delta_{jk}},
  \end{equation*}
  onde
  \begin{equation*}
    \delta_{jk}
    =
    \begin{cases}
      1, & j = k
      \\
      0, & j \neq k.
    \end{cases}
  \end{equation*}


  \begin{example}
    Sejam
    \begin{equation*}
      A
      =
      \matrixrepresentation{1 & 2 & 3 \\ 4 & 5 & 6}
      \quad\text{e}\quad
      B
      =
      \matrixrepresentation{1 & 2 \\ 5 & 6 \\ 8 & 9}.
    \end{equation*}
    Então,
    \begin{align*}
      I_2 A
      &=
      \matrixrepresentation{1 & 0 \\ 0 & 1}
      \matrixrepresentation{1 & 2 & 3 \\ 4 & 5 & 6}
      \\
      &=
      \matrixrepresentation{1 & 2 & 3 \\ 4 & 5 & 6}.
    \end{align*}
    Da mesma forma,
    \begin{align*}
      B I_2
      &=
      \matrixrepresentation{1 & 2 \\ 5 & 6 \\ 8 & 9}
      \matrixrepresentation{1 & 0 \\ 0 & 1}
      \\
      &=
      \matrixrepresentation{1 & 2 \\ 5 & 6 \\ 8 & 9}.
    \end{align*}
    Faça as contas{\ldots} :-)

    De fato,
    a entrada de coordenadas $jk$ do produto $I_2 A$ é
    \begin{equation*}
      a_{j1} \delta_{1k}
      +
      a_{j2} \delta_{2k}
      =
      a_{jk},
    \end{equation*}
    pois o único termo
    $a_{jr} \delta_{rk}$
    não nulo dessa soma é quando $r = k$.
    Um cálculo análogo pode ser feito para $B I_2$.
  \end{example}

  % TODO: exercício.
  % Faça a conta I A e B I usando o símbolo de somatório.

  A matriz identidade parece meio inútil{\ldots}
  mas sem ela,
  não conseguimos falar, por exemplo, da matriz inversa!

  Quando $A$ é uma matriz $n \times n$,
  por vezes existe uma matrix $X$ tal que
  \begin{equation*}
    AX
    =
    XA
    =
    I.
  \end{equation*}
  Quando isso acontece,
  dizemos que $X$ é a \emph{inversa} de $A$.
  Consequentemente,
  $A$ é a \emph{inversa} de $X$.
  Muitos problemas (da vida!)
  podem ser respondidos se conseguirmos inverter uma determinada matriz.

  \begin{example}
    Seja
    \begin{equation*}
      A
      =
      \matrixrepresentation{1 & 2 & 3 \\ 0 & 1 & 4 \\ 5 & 6 & 0}.
    \end{equation*}
    A inversa de $A$ é
    \begin{equation*}
      A^{-1}
      =
      \matrixrepresentation{-24 & 18 & 5 \\ 20 & -15 & -4 \\ -5 & 4 & 1}.
    \end{equation*}
    Suponha que você esteja procurando um vetor $\vector{v} \in \reals^3$
    tal que
    \begin{equation*}
      \matrixrepresentation{1 & 2 & 3 \\ 0 & 1 & 4 \\ 5 & 6 & 0}
      \matrixrepresentation{\vector{v}}
      =
      \matrixrepresentation{7 \\ 8 \\ 9}.
    \end{equation*}
    Se multiplicarmos tudo por $A^{-1}$,
    \begin{equation*}
      \matrixrepresentation{-24 & 18 & 5 \\ 20 & -15 & -4 \\ -5 & 4 & 1}
      \left(
        \matrixrepresentation{1 & 2 & 3 \\ 0 & 1 & 4 \\ 5 & 6 & 0}
        \matrixrepresentation{\vector{v}}
      \right)
      =
      \matrixrepresentation{-24 & 18 & 5 \\ 20 & -15 & -4 \\ -5 & 4 & 1}
      \matrixrepresentation{7 \\ 8 \\ 9}.
    \end{equation*}
    Usando a associatividade do produto de matrizes,
    \begin{align*}
      \matrixrepresentation{\vector{v}}
      &=
      I
      \matrixrepresentation{\vector{v}}
      \\
      &=
      \left(
        \matrixrepresentation{-24 & 18 & 5 \\ 20 & -15 & -4 \\ -5 & 4 & 1}
        \matrixrepresentation{1 & 2 & 3 \\ 0 & 1 & 4 \\ 5 & 6 & 0}
      \right)
      \matrixrepresentation{\vector{v}}
      \\
      &=
      \matrixrepresentation{-24 & 18 & 5 \\ 20 & -15 & -4 \\ -5 & 4 & 1}
      \left(
        \matrixrepresentation{1 & 2 & 3 \\ 0 & 1 & 4 \\ 5 & 6 & 0}
        \matrixrepresentation{\vector{v}}
      \right)
      \\
      &=
      \matrixrepresentation{-24 & 18 & 5 \\ 20 & -15 & -4 \\ -5 & 4 & 1}
      \matrixrepresentation{7 \\ 8 \\ 9}.
    \end{align*}
    Ou seja,
    para encontrar $\vector{v}$,
    basta fazer o produto
    \begin{align*}
      \matrixrepresentation{\vector{v}}
      &=
      \matrixrepresentation{-24 & 18 & 5 \\ 20 & -15 & -4 \\ -5 & 4 & 1}
      \matrixrepresentation{7 \\ 8 \\ 9}
      \\
      &=
      7
      \matrixrepresentation{-24 \\ 20 \\ -5}
      +
      8
      \matrixrepresentation{18 \\ -15 \\ 4}
      +
      9
      \matrixrepresentation{5 \\ -4 \\ 1}
      \\
      &=
      \matrixrepresentation{-168 \\ 140 \\ -35}
      +
      \matrixrepresentation{144 \\ -120 \\ 32}
      +
      \matrixrepresentation{45 \\ -36 \\ 9}
      \\
      &=
      \matrixrepresentation{21 \\ -16 \\ 6}.
    \end{align*}
    Assim,
    descobrimos que $\vector{v} = (21, -16, 6)$.
    De fato,
    \begin{align*}
      \matrixrepresentation{1 & 2 & 3 \\ 0 & 1 & 4 \\ 5 & 6 & 0}
      \matrixrepresentation{21 \\ -16 \\ 6}
      &=
      21
      \matrixrepresentation{1 \\ 0 \\ 5}
      +
      -16
      \matrixrepresentation{2 \\ 1 \\ 6}
      +
      6
      \matrixrepresentation{3 \\ 4 \\ 0}
      \\
      &=
      \matrixrepresentation{21 \\ 0 \\ 105}
      +
      \matrixrepresentation{-32 \\ -16 \\ -96}
      +
      \matrixrepresentation{18 \\ 24 \\ 0}
      \\
      &=
      \matrixrepresentation{7 \\ 8 \\ 9}.
    \end{align*}
  \end{example}


  \begin{example}
    Alguns exemplos de matrizes com inversa que são fáceis de fazer as contas. :-)
    \begin{align*}
      \matrixrepresentation{0 & 1 \\ 1 & 0}
      \matrixrepresentation{0 & 1 \\ 1 & 0}
      &=
      \matrixrepresentation{0 & 1 \\ 1 & 0}
      \matrixrepresentation{0 & 1 \\ 1 & 0}
      =
      \matrixrepresentation{1 & 0 \\ 0 & 1}
      \\
      \matrixrepresentation{1 & 1 \\ 1 & 0}
      \matrixrepresentation{0 & 1 \\ 1 & -1}
      &=
      \matrixrepresentation{0 & 1 \\ 1 & -1}
      \matrixrepresentation{1 & 1 \\ 1 & 0}
      =
      \matrixrepresentation{1 & 0 \\ 0 & 1}
    \end{align*}
  \end{example}

  Existem matrizes que não tem inversa.

  \begin{example}
    As seguintes matrizes não tem inversa.
    \begin{align*}
      \matrixrepresentation{0 & 0 \\ 0 & 0}
      \quad&\quad
      \matrixrepresentation{0 & 0 \\ 1 & 0}
      \\
      \matrixrepresentation{1 & 1 \\ 5 & 5}
      \quad&\quad
      \matrixrepresentation{3 & -2 \\ 6 & -4}
      \\
      \matrixrepresentation{1 & 2 & 3 \\ 2 & 4 & -1 \\ 4 & 8 & 5}
      \quad&\quad
      \matrixrepresentation{3 & 1 & 2 \\ 1 & 5 & -4 \\ 2 & 1 & 1}.
    \end{align*}
  \end{example}

  \begin{intriguing question}
    Quando uma matriz $A$ tem,
    e quando não tem inversa?
  \end{intriguing question}

  \begin{intriguing question}
    Por que não falamos da inversa de uma matriz
    que não é \emph{quadrada}?
  \end{intriguing question}

  \begin{intriguing question}
    Que técnicas podemos usar para encontrar a inversa de uma determinada matriz?
    Tem uma maneira simples de saber se determinada matriz tem ou não inversa?
  \end{intriguing question}
