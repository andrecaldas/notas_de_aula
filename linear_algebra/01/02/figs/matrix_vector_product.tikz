\begin{tikzpicture}
  [
    caption =
    {%
      Multiplicando a matriz
      $M = \matrixrepresentation{1 & 5 \\ -2 & 7}$
      e a matriz coluna
      $N = \matrixrepresentation{3 \\ 8}$.
    }
  ]

  \matrix(T)
  [
    matrix of math nodes,
    anchor=north east,
    nodes={minimum size=1.5em},
    inner sep=0pt,
    column sep=0.5em,
    left delimiter={[},
    right delimiter={]},
  ]
  at (-3em, 0em)
  {%
    1 & 5 \\
    -2 & 7 \\
  };

  \matrix(v)
  [
    matrix of math nodes,
    anchor=south west,
    nodes={minimum size=1.5em},
    inner sep=0pt,
    row sep=0.5em,
    left delimiter={[},
    right delimiter={]},
  ]
  at (0em, 2em)
  {%
    3 \\
    8 \\
  };

  \node[rounded corners=10, opacity=.2, fill=blue,  fit=(T-1-1) (T-2-1), name=column v1, inner sep=0, outer sep=2pt] {};
  \node[rounded corners=10, opacity=.2, fill=green, fit=(T-1-2) (T-2-2), name=column v2, inner sep=0, outer sep=2pt] {};

  \node[rounded corners=10, opacity=.2, fill=blue,  fit=(v-1-1), name=scalar v1, inner sep=0, outer sep=2pt] {};
  \node[rounded corners=10, opacity=.2, fill=green,  fit=(v-2-1), name=scalar v2, inner sep=0, outer sep=2pt] {};

  \draw[red, ->, bend right] (scalar v1) to node[black, above, sloped] {$*$} (column v1);
  \draw[red, ->, bend right] (scalar v2) to node[black, above, sloped] {$*$} (column v2);

  \matrix
  [
    matrix of math nodes,
    anchor=north west,
    nodes={minimum size=1.5em},
    inner sep=0pt,
  ]
  at (-1em, 0em)
  {%
    =&
    3
    \begin{bmatrix}
      1 \\ -2
    \end{bmatrix}
    &+
    &
    8
    \begin{bmatrix}
      5 \\ 7
    \end{bmatrix}
    \\
  };
\end{tikzpicture}
