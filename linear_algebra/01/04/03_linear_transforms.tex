\section{Transformações Lineares}

  Já verificamos o que é,
  do ponto de vista geométrico,
  uma \emph{transformação linear} em $\reals^2$.
  Podemos fazer o mesmo para $\reals^3$,
  ou mesmo para $\reals^n$.
  Na verdade,
  podemos fazer o mesmo para \emph{qualquer}
  espaço vetorial
  (\cref{definition:vector_space:general}).

  \begin{definition}[Transformação Linear]
    Sejam $V$, $W$ espaços vetoriais.
    Uma \emph{transformação linear} de $V$ em $W$
    é uma aplicação
    $\function{T}{V}{W}$ tal que
    \begin{enumerate}
      \item
        $\vector{u}, \vector{v} \in V$
        $\Rightarrow$
        $T(\vector{u} + \vector{v}) = T\vector{u} + T\vector{v}$.

      \item
        $\alpha \in \reals$,
        $\vector{v} \in V$
        $\Rightarrow$
        $T(\alpha \vector{v}) = \alpha T\vector{v}$.
    \end{enumerate}
  \end{definition}

  \begin{example}
    Para representarmos uma \emph{transformação linear} em $\reals^2$,
    desenhavamos um quadriculado e um reticulado no plano,
    e \emph{``transportávamos''} o desenho do quadriculado para o reticulado.
    Em $\reals^3$ é muito parecido,
    mas agora não é mais um quadriculado.
    Quem sabe um \emph{``cubiculado''}!!! :-)

    \nftikzpic[figure:many_3d_houses]
      {01/04/figs/many_3d_houses.tikz}

    Para especificarmos as \emph{transformações lineares} da \cref{figure:many_3d_houses},
    precisamos apenas dizer qual é a imagem dos vetores
    $\vector{e_1}$,
    $\vector{e_2}$
    e
    $\vector{e_3}$.
    Essas imagens estão indicadas em vermelho.
    Para \emph{``esticar''} a casinha como na parte direita da figura,
    foi aplicada a transformação
    \begin{equation*}
      \functionarray{T}{\reals^3}{\reals^3}
                    {(a,b,c)}{(a,2.7b,c)}.
    \end{equation*}
    O outro caso é mais complicado.
    É dado pela transformação
    \begin{equation*}
      \functionarray{T}{\reals^3}{\reals^3}
                    {(a,b,c)}
                    {(1.2a + 0.3b - 0.3c, b + 0.2c, 0.7a + c)}.
    \end{equation*}
    Repare que os vetores
    $T\vector{e_1} = (1.2, 0, 0.7)$,
    $T\vector{e_2} = (0.3, 1, 0)$
    e
    $T\vector{e_3} = (-0.3, 0.2, 1)$
    é que determinam os números \emph{``mágicos''} que aparecem na expressão
    $(1.2a + 0.3b - 0.3c, b + 0.2c, 0.7a + c)$.
  \end{example}


  Se $\function{T}{V}{W}$ é uma transformação linear,
  então sua imagem, $T(V)$, é um subespaço de $W$.
  De fato,
  se $\vector{a}, \vector{b} \in T(V)$,
  então existem $\vector{u}, \vector{v} \in V$
  tais que
  \begin{equation*}
    \vector{a} = T\vector{u}
    \quad\text{e}\quad
    \vector{b} = T\vector{v}.
  \end{equation*}
  Portanto,
  dado $\alpha \in \reals$,
  \begin{align*}
    \vector{a}
    +
    \vector{b}
    &=
    T\vector{u}
    +
    T\vector{v}
    \\
    &=
    T(\vector{u} + \vector{v})
    \in
    T(V)
    \\
    \alpha \vector{a}
    &=
    \alpha T\vector{v}
    \\
    &=
    T(\alpha \vector{v})
    \in
    T(V).
  \end{align*}
  Ou seja,
  $T(V)$ é fechado por soma e produto por escalar.

  \begin{example}
    Um plano em $\reals^3$ é a imagem de uma aplicação linear injetiva
    \begin{equation*}
      \function{T}{\reals^2}{\reals^3}.
    \end{equation*}
    De fato,
    já sabemos que a imagem de $T$ é um subespaço de $\reals^3$.
    Se a imagem não for um plano,
    isso significa que
    $T\vector{e_1}$
    e
    $T\vector{e_2}$
    são paralelos.
    Ou seja,
    existem $\alpha, \beta \in \reals$,
    com pelo menos um deles não nulo,
    tais que
    \begin{equation*}
      \alpha T\vector{e_1}
      =
      \beta T\vector{e_2}.
    \end{equation*}
    Mas isso é o mesmo que dizer que
    \begin{equation*}
      T(\alpha \vector{e_1})
      =
      T(\beta \vector{e_2}),
    \end{equation*}
    com
    $\alpha \vector{e_1} \neq \beta \vector{e_2}$.
    Ou seja,
    quando a imagem não é um plano,
    a aplicação $T$ não é injetiva.
    Assim,
    se $T$ é injetiva,
    $T(V)$ é um plano.

    Por outro lado,
    se $T(V)$ não é injetiva,
    então existem dois vetores,
    $\vector{u} = (u_1, u_2)$
    e
    $\vector{v} = (v_1, v_2)$
    tais que
    $T\vector{u} = T\vector{v}$.
    Mas isso é o mesmo que
    dizer que
    \begin{equation*}
      T((u_1 - v_1)\vector{e_1})
      =
      T((v_2 - u_2)\vector{e_2}).
    \end{equation*}
    Ou seja,
    $T\vector{e_1}$
    e
    $T\vector{e_2}$
    são paralelos.
    Por consequência,
    como todos os vetores são combinação linear de
    $\vector{e_1}$
    e
    $\vector{e_2}$,
    todos os pontos de $T(V)$ estão em uma mesma reta.

    Ficou demonstrado, então,
    que a imagem de $T$ é um plano
    se, e somente se,
    $T$ é injetiva.
    Ainda resta responder à pergunta:
    \begin{quote}
      Todo plano de $\reals^3$ que passa pela origem
      é imagem de uma \emph{transformação linear}
      $\function{T}{\reals^2}{\reals^3}$?
    \end{quote}
    A resposta é afirmativa,
    já que todo plano $P$ é \emph{``gerado''}
    por dois vetores
    $\vector{v_1}$
    e
    $\vector{v_2}$.
    E portanto,
    $P$ é a imagem de
    \begin{equation*}
      \functionarray{T}{\reals^2}{\reals^3}
                    {(a,b)}{a\vector{v_1} + b\vector{v_2}}.
    \end{equation*}
  \end{example}
