\section{A Inversa}

  Apesar de uma transformação linear
  $\function{T}{V}{W}$ ser, em geral,
  uma função entre conjuntos infinitos,
  ela fica completamente determinada se conhecermos seu valor em um conjunto gerador.
  Todas as propriedades das trasnformações lineares
  ficam completamente determinadas se soubermos como a transformação age em uma base.
  Assim como no \cref{example:linear_transform:r2:define},
  não apenas conhecemos uma trasformação se soubermos seus valores em uma base{\ldots}
  podemos construir uma transformação escolhendo da forma que quisermos
  o valor que essa transformação deve assumir em uma determinada base.
  A grande preocupação é sabermos
  se a função que estamos contruindo está ou não \emph{bem-definida}.

  \begin{example}[injetividade e sobrejetividade]
    \label{example:injective_and_surjective:rn}
    Seja $\function{T}{V}{W}$ uma transformação linear.
    Seja
    $\vector{v_1}, \dotsc, \vector{v_n}$
    uma base de $V$.
    E faça
    $\vector{w_1} = T\vector{v_1}$,
    $\vector{w_2} = T\vector{v_2}$,
    $\dotsc$,
    $\vector{w_n} = T\vector{v_n}$.
    Então,
    dizer que $T$ é sobrejetiva é o mesmo que dizer que
    $\vector{w_1}, \dotsc, \vector{w_n}$
    gera o espaço $W$.

    Por outro lado,
    suponha que $T$ é injetiva.
    Então,
    para todo $\vector{u}, \vector{v} \in V$,
    \begin{equation*}
      T\vector{u}
      =
      T\vector{v}
      \Rightarrow
      \vector{u}
      =
      \vector{v}.
    \end{equation*}
    Em particular,
    fazendo $\vector{u} = \vector{0}$,
    \begin{equation*}
      T\vector{v}
      =
      \vector{0}
      \Rightarrow
      \vector{v}
      =
      \vector{0}.
    \end{equation*}
    Assim,
    se $\alpha_1, \dotsc, \alpha_n \in \reals$,
    tomando
    $\vector{v} = \alpha_1 \vector{v_1} + \dotsb + \alpha_n \vector{v_n}$,
    temos que,
    sempre que
    $\alpha_1 \vector{w_1} + \dotsb + \alpha_n \vector{w_n} = 0$,
    \begin{equation*}
      T
      \left(
        \alpha_1 \vector{v_1}
        + \dotsb +
        \alpha_n \vector{v_n}
      \right)
      =
      \alpha_1 \vector{w_1}
      + \dotsb +
      \alpha_n \vector{w_n}
      =
      0.
    \end{equation*}
    Mas isso implica,
    pela \emph{injetividade} de $T$,
    que
    \begin{equation*}
      \alpha_1 \vector{v_1}
      + \dotsb +
      \alpha_n \vector{v_n}
      =
      0
    \end{equation*}
    E como $\vector{v_1}, \dotsc, \vector{v_n}$ é uma base,
    isso implica que
    \begin{equation*}
      \alpha_1 = \dotsb = \alpha_n = 0.
    \end{equation*}
    Ou seja,
    \begin{equation*}
      \alpha_1 \vector{w_1}
      + \dotsb +
      \alpha_n \vector{w_n}
      =
      0
      \Rightarrow
      \alpha_1 = \dotsb = \alpha_n = 0.
    \end{equation*}
    Ou seja,
    $\vector{w_1}, \dotsc, \vector{w_n}$
    são \emph{linearmente independentes}.

    Em particular,
    esse exemplo mostra que uma transformação linear
    $\function{T}{V}{W}$
    é inversível quando leva bases de $V$
    em bases de $W$.
  \end{example}

  Seja $\function{T}{V}{W}$ uma \emph{transformação linear inversível}.
  Então,
  a inversa $T^{-1}$ também é linear.
  De fato,
  para quaisquer $x, y \in W$,
  como $T$ é \emph{sobrejetiva},
  existem $v, w \in V$,
  tais que $x = Tv$ e $y = Tw$.
  Então,
  \begin{align*}
    T^{-1}(x + y)
    &=
    T^{-1}(Tv + Tw)
    \\
    &=
    T^{-1}(T(v + w))
    \\
    &=
    v + w
    \\
    &=
    T^{-1}(x) + T^{-1}(y).
  \end{align*}
  E se $\alpha \in \reals$,
  \begin{align*}
    T^{-1}(\alpha x)
    &=
    T^{-1}(\alpha Tv)
    \\
    &=
    T^{-1}(T(\alpha v))
    \\
    &=
    \alpha v
    \\
    &=
    \alpha T^{-1}(x).
  \end{align*}

  O \cref{example:injective_and_surjective:rn}
  mostra que,
  se $\vector{b_1}, \dotsc, \vector{b_n}$
  é uma base para $V$,
  $T^{-1}$ pode ser caracterizada como a transformação linear
  que leva (a base)
  $T\vector{b_1}, \dotsc, T\vector{b_n}$
  em
  $\vector{b_1}, \dotsc, \vector{b_n}$.
  Em particular,
  se $V = W = \reals^n$,
  e se $\vector{b_j} = \vector{e_j}$,
  como será que podemos determinar
  a \emph{representação matricial} da transformação $T^{-1}$?
