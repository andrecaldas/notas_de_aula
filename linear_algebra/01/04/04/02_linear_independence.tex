\subsection{Independência Linear}

  No \cref{example:subspaces:plane},
  precisamos impor condições para que
  $\generatedsubspace{\vector{v}, \vector{w}}$,
  o espaço gerado pelos vetores
  $\vector{v}$ e $\vector{w}$
  fosse de fato um plano,
  e não uma reta, por exemplo.
  Mas pensando bem{\ldots}
  o que é \emph{um plano que passa na origem}?
  Não seria o mesmo que um subespaço bidimensional?
  Mas{\ldots} \emph{bidimensional} significa o quê?
  Que tem \emph{dimensão} $2$?
  E o que significa isso tudo?
  Uma reta passando pela origem,
  é um subespaço de dimensão $1$.
  O subespaço $\set{\vector{0}}$ tem dimensão $0$.

  Podemos dar um significado preciso aos conceitos de plano,
  reta, \emph{dimensão}?
  E se estivermos falando de $\reals^n$?
  Podemos generalizar esses conceitos e falar
  dos subespaços de \emph{dimensão} $k$?

  \begin{definition}[Independência Linear]
    \label{definition:linear_independence:rn}
    Dizemos que um vetor $\vector{v} \in \reals^n$
    é \emph{(linearmente) independente} de $A \subset \reals^n$,
    quando $\vector{v} \not \in \generatedsubspace{A}$.

    Dizemos que $A$ é \emph{linearmente independente}
    (ou simplesmente \emph{LI})
    quando todo $\vector{v} \in A$ é idependente de $A \setminus \set{\vector{v}}$.
    Quando $A$ não é \emph{linearmente independente},
    dizemos que é \emph{linearmente dependente},
    ou simplesmente, \emph{LD}.

    Diremos também que uma coleção \emph{indexada} de vetores
    $\vector{v_1}, \dotsc, \vector{v_k}$ é
    \emph{linearmente independente}
    quando forem todos distintos e o conjunto
    $\set{\vector{v_1}, \dotsc, \vector{v_k}}$
    for \emph{LI}.
  \end{definition}

  Dizer que vetores são linearmente independentes,
  é dizer que um não pode ser escrito como combinação dos outros.

  \begin{example}[dois vetores que não geram um plano]
    Quando dois vetores distintos não nulos,
    $\vector{v}$
    e
    $\vector{w}$
    não geram um plano,
    é porque um é \emph{linearmente dependente do outro}.
    Em um certo sentido,
    o conjunto $S = \set{\vector{v}, \vector{w}}$
    tem elementos \emph{``desnecessários''}.
    O espaço gerado por $S$ é o espaço gerado pelo vetor $\vector{v}$.

    Quando falamos que $\vector{0}$ e mais dois pontos $\vector{v}$ e $\vector{w}$,
    quando não são três pontos colineares,
    determinam um plano,
    estamos justamente falando da independência linear de $\vector{v}$ e $\vector{w}$.
    Quando tem \emph{``desperdício''} no conjunto gerador,
    o espaço gerado acaba menor do que o \emph{``esperado''}.
    É por isso que o estudante prudente não diz que dois vetores geram um plano que passa pela origem!
    Ele diz:
    \begin{quote}
      Dois vetores \textbf{linearmente independentes} geram um plano que passa pela origem.
    \end{quote}
  \end{example}

  \begin{obs}
    Se
    $\vector{v_1} = \vector{v_2} = \vector{e_1}$,
    então diremos que
    $\vector{v_1}$ e $\vector{v_2}$ são \emph{LD}.
    No entanto,
    o conjunto
    $\set{\vector{v_1}, \vector{v_2}}$ \textbf{NÃO} é \emph{LD}.
    Por quê?
  \end{obs}


  Existem outras maneiras de se pensar em
  \emph{independência linear}.

  \begin{proposition}[Caracterizações de Independência Linear]
    \label{proposition:linear_independence:characterization}
    Seja $A \subset \reals^n$.
    Então,
    são equivalentes:
    \begin{enumerate}
      \item
        \label{item:proposition:linear_independence:characterization:li}
        $A$ é \emph{linearmente independente}.

      \item
        \label{item:proposition:linear_independence:characterization:gerador_minimal}
        O conjunto $A$ é um gerador minimal para $\generatedsubspace{A}$.
        Ou seja,
        se $B \subsetneq A$,
        então
        \begin{equation*}
          \generatedsubspace{B}
          \subsetneq
          \generatedsubspace{A}.
        \end{equation*}

      \item
        \label{item:proposition:linear_independence:characterization:all_null}
        Sempre que
        $\vector{v_1}, \dotsc, \vector{v_k} \in A$ são distintos
        e
        \begin{equation*}
          \alpha_1 \vector{v_1}
          + \dotsb +
          \alpha_k \vector{v_k}
          =
          \vector{0},
        \end{equation*}
        então
        $\alpha_1 = \dotsb = \alpha_k = 0$.

      \item
        \label{item:proposition:linear_independence:characterization:unique:same_vectors}
        Toda combinação linear de elementos de $A$ é \emph{única},
        no sentido de que se
        $\vector{v_1}, \dotsc, \vector{v_k} \in A$ são distintos,
        então
        \begin{equation*}
          \alpha_1 \vector{v_1}
          + \dotsb +
          \alpha_k \vector{v_k}
          =
          \beta_1 \vector{v_1}
          + \dotsb +
          \beta_k \vector{v_k}
        \end{equation*}
        implica que
        $\alpha_1 = \beta_1, \dotsc, \alpha_k = \beta_k$.

      \item
        \label{item:proposition:linear_independence:characterization:unique}
        Toda combinação linear de elementos de $A$ é \emph{única},
        no sentido de que se
        \begin{align*}
          \vector{v}
          &=
          \alpha_1 \vector{v_1}
          + \dotsb +
          \alpha_k \vector{v_k},
          \\
          \vector{v}
          &=
          \beta_1 \vector{w_1}
          + \dotsb +
          \beta_m \vector{w_m},
        \end{align*}
        com todos os
        $\vector{v_1}, \dotsc, \vector{v_k} \in A$ distintos entre si,
        todos os
        $\vector{w_1}, \dotsc, \vector{w_m} \in A$ distintos entre si,
        e com todos os
        $\alpha_1, \dotsc, \alpha_k$
        e
        $\beta_1, \dotsc, \beta_k$ não nulos,
        então $k = m$,
        e os índices de $\beta_j$ e $\vector{w_j}$ podem ser rearranjados
        de modo que
        \begin{align*}
          \vector{v_1} = \vector{w_1}
          \quad&\text{e}\quad
          \alpha_1 = \beta_1
          \\
          &\vdots
          \\
          \vector{v_k} = \vector{w_k}
          \quad&\text{e}\quad
          \alpha_k = \beta_k.
        \end{align*}
    \end{enumerate}
  \end{proposition}

  \begin{proof}
    TODO.
  \end{proof}


  O \cref{item:proposition:linear_independence:characterization:gerador_minimal}
  da \cref{proposition:linear_independence:characterization}
  mostra que $A \subset \reals^n$ é linearmente independente quando
  não possui nenhum elemento \emph{``sobrando''},
  ao contrário do exemplo a seguir.

  \begin{example}
    Seja
    \begin{equation*}
      A
      =
      \set{(1,1,0), (1,0,1), (0,1,-1)}.
    \end{equation*}
    Então $A$ é \emph{linearmente dependente}.
    De fato,
    \begin{equation*}
      (1,1,0) = (1,0,1) + (0,1,-1).
    \end{equation*}
    O vetor $(4,1,3)$,
    por exemplo,
    pode ser escrito como infinitas combinações lineares diferentes.
    Algumas delas, são
    \begin{align*}
      (4,1,3)
      &=
      (1,1,0) + 3(1,0,1)
      \\
      (4,1,3)
      &=
      4(1,1,0) - 3(0,1,-1)
      \\
      (4,1,3)
      &=
      2(1,1,0) + 2(1,0,1) - (0,1,-1).
    \end{align*}
  \end{example}

  \begin{example}
    Continuando o \cref{example:generated_subspace:infinite_set},
    o conjunto
    \begin{equation*}
      A
      =
      \setsuchthat{\left(\frac{1}{n+1}, 1, 0\right) \in \reals^3}{n \in \naturals}
    \end{equation*}
    possui vários subconjuntos de dois elementos que são
    geradores de do plano $xy$.
    De fato,
    qualquer subconjunto de $A$ com dois elementos é um gerador para o mesmo subespaço.
    Por exemplo,
    \begin{equation*}
      B
      =
      \set{(1, 1, 0), \left(\frac{1}{2}, 1, 0\right)}
    \end{equation*}
    gera o plano $xy$.
    O leitor é convidado a mostrar que
    $\vector{e_1}, \vector{e_2} \in \generatedsubspace{B}$.
  \end{example}

  O exemplo a seguir,
  é mais uma proposição do que um exemplo.

  \begin{example}
    Os subconjuntos de $\reals^3$ que são linearmente independentes
    tem cardinalidade $0$, $1$, $2$ ou $3$.

    De fato,
    se $A \subset \reals^3$ tiver $4$ elementos distintos
    $\vector{a} = (a_1, a_2, a_3)$,
    $\vector{b} = (b_1, b_2, b_3)$,
    $\vector{c} = (c_1, c_2, c_3)$ e
    $\vector{d} = (d_1, d_2, d_3)$,
    então
    o sistema de equações
    \begin{equation*}
      \left\{
        \begin{array}{rl}
          a_1 x + b_1 y + c_1 z + d_1 w &= 0 \\
          a_2 x + b_2 y + c_2 z + d_2 w &= 0 \\
          a_3 x + b_3 y + c_3 z + d_3 w &= 0
        \end{array}
      \right.
    \end{equation*}
    tem $3$ equações e $4$ incógnitas,
    e portanto,
    tem infinitas soluções.
    Em particular,
    existem $\alpha_1, \alpha_2, \alpha_3, \alpha_4 \in \reals$,
    nem todos nulos, tais que
    \begin{equation*}
      \alpha_1 \vector{a}
      +
      \alpha_2 \vector{b}
      +
      \alpha_3 \vector{c}
      +
      \alpha_4 \vector{d}
      =
      \vector{0}.
    \end{equation*}

    Agora,
    como é que você sabe que um sistema homogêneo
    com $3$ equações e $4$ incógnitas
    tem infinitas soluções{\ldots}
    isso é com você!!!
    Como é que você sabe disso?
    :-)
  \end{example}

  Já vimos no \cref{lemma:generator_comutes_with_transform}
  que uma transformação linear sobrejetiva
  leva conjunto gerador em conjunto gerador.

  \begin{lemma}
    \label{lemma:linear_independence:injectivity}
    Seja
    $\function{T}{E}{F}$
    uma transformação linear injetiva.
    Se $S \subset E$ é linearmente independente,
    então $T(S) \subset F$ também é.
  \end{lemma}

  \begin{proof}
    Suponha que
    \begin{equation*}
      \alpha_1 T(\vector{v_1})
      + \dotsb +
      \alpha_k T(\vector{v_k})
      =
      \vector{0},
    \end{equation*}
    com $\vector{v_1}, \dotsc, \vector{v_k} \in S$.
    Para concluir que $T(S)$ é linearmente independente,
    basta mostrar que
    $\alpha_1 = \dotsb = \alpha_k = 0$.

    Note que,
    \begin{align*}
      T
      \left(
        \alpha_1 \vector{v_1}
        + \dotsb +
        \alpha_k \vector{v_k}
      \right)
      &=
      \alpha_1 T(\vector{v_1})
      + \dotsb +
      \alpha_k T(\vector{v_k})
      \\
      &=
      \vector{0}.
    \end{align*}
    Mas pela injetividade de $T$,
    isso implica que
    \begin{equation*}
      \alpha_1 T(\vector{v_1})
      + \dotsb +
      \alpha_k T(\vector{v_k})
      =
      \vector{0}.
    \end{equation*}
    Pela independência linear de $S$,
    isso implica que
    $\alpha_1 = \dotsb = \alpha_k = 0$.
  \end{proof}
