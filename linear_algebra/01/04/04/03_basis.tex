\subsection{Bases e Dimensão}

  O falarmos de vetores em $\reals^2$,
  podíamos descrevê-los como combinação linear dos vetores
  $\vector{e_1}$ e $\vector{e_2}$.
  Todo vetor $\vector{v} \in \reals^2$
  é da forma
  \begin{equation*}
    \vector{v}
    =
    \alpha \vector{e_1}
    +
    \beta \vector{e_2}.
  \end{equation*}
  Além disso,
  essa representação é única.
  Dizer que todo vetor é combinação linear de
  $\vector{e_1}$ e $\vector{e_2}$,
  é o mesmo que dizer que esses dois vetores formam um conjunto
  gerador do espaço $\reals^2$.
  E dizer que a representação é única,
  é o mesmo que dizer que são linearmente independentes.
  O mesmo vale para
  \begin{equation*}
    \vector{e_1},
    \dotsc,
    \vector{e_n}
    \in
    \reals^n.
  \end{equation*}
  São geradores e linearmente independentes.
  Em outras palavras,
  formam uma \emph{base} para $\reals^n$.

  \begin{definition}
    Uma \emph{base} de um espaço vetorial
    é um conjunto de vetores linearmente independentes
    que gera todo o espaço.
  \end{definition}


  \begin{example}
    \label{example:linear_transform:r2:determine}
    Em $\reals^2$,
    para especificarmos uma \emph{transformação linear}
    $\function{T}{\reals^2}{\reals^2}$,
    bastava especificar
    $\vector{v_1} = T\vector{e_1}$
    e
    $\vector{v_2} = T\vector{e_2}$.
    Nesse caso,
    \begin{equation*}
      \functionarray{T}{\reals^2}{\reals^2}
                    {\alpha \vector{e_1} + \beta \vector{e_2}}
                    {\alpha \vector{v_1} + \beta \vector{v_2}}.
    \end{equation*}
    Na verdade,
    poderíamos substituir
    $\vector{e_1}$
    e
    $\vector{e_2}$
    por uma base qualquer.
    De fato,
    se soubermos que
    \begin{equation*}
      \vector{w_1} = T\vector{b_1}
      \quad\text{e}\quad
      \vector{w_2} = T\vector{b_2},
    \end{equation*}
    onde
    $\vector{b_1}$
    e
    $\vector{b_2}$
    formam uma base para $\reals^2$,
    então sabemos calcular $T\vector{u}$
    para qualquer $\vector{u} \in \reals^2$,
    pois $\vector{u}$ pode ser escrito na forma
    $\alpha \vector{b_1} + \beta \vector{b_2}$.
    Nesse caso,
    \begin{equation*}
      \functionarray{T}{\reals^2}{\reals^2}
                    {\alpha \vector{b_1} + \beta \vector{b_2}}
                    {\alpha \vector{w_1} + \beta \vector{w_2}}.
    \end{equation*}
  \end{example}

  % TODO: comentar depois de dizer que LI tem cardinalidade máxima.
  %% No \cref{example:linear_transform:r2:determine},
  %% não usamos o fato de os vetores
  %% $\vector{b_1}$
  %% e
  %% $\vector{b_2}$
  %% formarem uma base!
  %% Usamos apenas o fato de serem geradores de $\reals^2$.

  \begin{example}
    \label{example:linear_transform:r2:define}
    Considere os vetores
    $\vector{e_1}$,
    $\vector{e_2}$
    e
    $\vector{b} = (1,1)$
    em $\reals^2$.
    É possível definir uma \emph{transformação linear}
    $\function{T}{\reals^2}{\reals^2}$
    tal que
    \begin{equation*}
      T\vector{e_1} = \vector{0},
      \quad
      T\vector{e_2} = \vector{e_1}
      \quad\text{e}\quad
      T\vector{b} = \vector{e_2}?
    \end{equation*}
    A resposta é: \textbf{Não!}
    Mas por que motivo isso não é possível?

    E se fossem só os vetores
    $\vector{e_1}$
    e
    $\vector{e_2}$,
    sem a condição no vetor $\vector{b}$?
    Ou seja,
    será que existe uma \emph{transformação linear}
    $T$ tal que
    \begin{equation*}
      T\vector{e_1} = \vector{0}
      \quad\text{e}\quad
      T\vector{e_2} = \vector{e_1}?
    \end{equation*}
    E,
    pra complicar um pouco,
    e se fossem só os vetores
    $\vector{e_1}$
    e
    $\vector{b}$,
    sem impor uma condição no vetor
    $\vector{e_2}$?
    Seria possível?
    Será que conseguimos definir,
    sem nenhuma inconsistência,
    uma \emph{transformação linear} $T$ tal que
    \begin{equation*}
      T\vector{e_1} = \vector{0}
      \quad\text{e}\quad
      T\vector{b} = \vector{e_2}?
    \end{equation*}

    É fácil explicar os casos em que sabemos
    que existe um problema na definição de $T$.
    Basta apontar a contradição e dizer:
    \textbf{impossível!}
    Por exemplo,
    não é possível fazer
    \begin{equation*}
      T\vector{e_1} = \vector{0},
      \quad
      T\vector{e_2} = \vector{e_1}
      \quad\text{e}\quad
      T\vector{b} = \vector{e_2}.
    \end{equation*}
    Pois ao mesmo tempo que sabemos que
    \begin{equation*}
      T\vector{b}
      =
      \vector{e_2},
    \end{equation*}
    também é verdade que
    \begin{equation*}
      T\vector{b}
      =
      T(\vector{e_1} + \vector{e_2})
      =
      T\vector{e_1} + T\vector{e_2}
      =
      \vector{e_1}.
    \end{equation*}
    Se encontrarmos uma contradição como essa,
    sabemos que $T$ \textbf{não está bem definida}.
    Mas como podemos \textbf{garantir que não há uma contradição}?
    Como podemos gararntir que a transformação $T$ está bem definida?
    Sabendo o valor de $T$ em
    $\vector{e_1}$,
    $\vector{e_2}$
    e
    $\vector{b}$,
    concluímos que $T$ deve satisfazer:
    \begin{equation*}
      \functionarray{T}{\reals^2}{\reals^2}
                    {\alpha_1 \vector{e_1} + \alpha_2 \vector{e_2} + \alpha_3 \vector{b}}
                    {\alpha_1 T\vector{e_1} + \alpha_2 T\vector{e_2} + \alpha_3 T\vector{b}}.
    \end{equation*}
    Para que dessa forma $T$ esteja bem definida,
    é necessário e suficiente que:
    \begin{enumerate}
      \item
        \label{item:example:linear_transform:r2:define:generate}
        Todo vetor $\vector{v} \in \reals^2$
        possa ser expresso da forma
        $\alpha_1 \vector{e_1} + \alpha_2 \vector{e_2} + \alpha_3 \vector{b}$,
        para que $T$ esteja definido em todo domínio.

      \item
        \label{item:example:linear_transform:r2:define:independent}
        Não haja ambiguidade na definição do valor de $T\vector{v}$
        para nenhum $\vector{v} \in \reals^2$.
        Ou seja,
        mesmo que $\vector{v}$ possa ser expresso de duas maneiras diferentes,
        \begin{align*}
          \vector{v}
          &=
          \alpha_1 \vector{e_1} + \alpha_2 \vector{e_2} + \alpha_3 \vector{b}
          \\
          \vector{v}
          &=
          \beta_1 \vector{e_1} + \beta_2 \vector{e_2} + \beta_3 \vector{b},
        \end{align*}
        é importante que o valor de $T\vector{v}$ seja o mesmo se calculado utilizando
        qualquer uma das duas maneiras distintas.
    \end{enumerate}

    A condição
    do \cref{item:example:linear_transform:r2:define:generate}
    equivale a dizer que os vetores
    $\vector{e_1}$,
    $\vector{e_2}$
    e
    $\vector{b}$
    \emph{geram} o domínio de $T$.

    Agora,
    segundo a \cref{proposition:linear_independence:characterization},
    uma maneira simples de garantir que
    o \cref{item:example:linear_transform:r2:define:independent}
    seja satisfeito,
    é fazendo com que
    $\vector{e_1}$,
    $\vector{e_2}$
    e
    $\vector{b}$
    sejam \emph{linearmente independentes}.

    Assim,
    concluímos que para construir uma \emph{transformação linear}
    podemos simplesmente escolher o valor de $T$
    para cada elemento de uma base preestabelecida.
    Como a base é \emph{geradora},
    estaremos definindo a transformação para todos os elementos do domínio.
    E como é \emph{linearmente independente},
    não existe ambiguidade na definição.
  \end{example}
