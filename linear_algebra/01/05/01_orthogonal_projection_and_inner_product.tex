\section{Projeção Ortogonal e o Produto Interno}
  \label{section:cannonical_inner_product}

  Em $\reals^2$ ou $\reals^3$,
  imagine uma reta $r$ e um vetor $\vector{v}$,
  com ponto base $O$ em cima da reta $r$,
  como mostra a \cref{figure:vector_and_straight_line}.
  Suponha que queiramos saber
  o \emph{``tamanho''} da \emph{``projeção''} de $\vector{v}$ em $r$,
  que vamos chamar de $P_r(\vector{v})$.

  \nftikzpic[figure:vector_and_straight_line]
    {01/05/01/figs/vector_and_straight_line.tikz}

  A transformação
  $\function{P_r}{\reals^3}{\reals}$
  tem algumas propriedades interessantes.
  A primeira delas diz respeito ao produto por escalar.
  \begin{equation*}
    P_r(\alpha \vector{v})
    =
    \abs{\alpha} P_r(\vector{v}).
  \end{equation*}

  \nftikzpic{01/05/01/figs/projection_to_straight_line-scalar.tikz}

  Às vezes,
  \begin{equation*}
    P_r(\vector{u} + \vector{v})
    =
    P_r(\vector{u}) + P_r(\vector{v}),
  \end{equation*}
  mas apenas quando
  as projeções de $\vector{u}$ e $\vector{v}$ em $r$
  tem o \emph{``mesmo sentido''}.

  \nftikzpic{01/05/01/figs/projection_to_straight_line-vector_sum-add.tikz}

  Quando não tem o mesmo sentido,
  as projeções se subtraem ao invés de se somarem.
  Assim,
  podemos ter
  \begin{equation*}
    P_r(\vector{u} + \vector{v})
    =
    \begin{cases}
      P_r(\vector{u}) + P_r(\vector{v}), & \text{mesmo sentido}
      \\
      \abs{P_r(\vector{u}) - P_r(\vector{v})}, & \text{sentidos opostos}.
    \end{cases}
  \end{equation*}

  \nftikzpic{01/05/01/figs/projection_to_straight_line-vector_sum-subtract.tikz}


  Essas propriedades de produto por escalar e soma
  são um indicativo de que
  talvez devessemos considerar uma projeção \emph{com sinal}.
  Uma projeção que possa ser negativa pra um lado e positiva para o outro.
  Talvez devessemos considerar um sentido para a reta $r$.
  Projeções com o mesmo sentido que o escolhido são positivas,
  e com sentido oposto são negativas.
  Sendo assim,
  ao invés de falarmos da reta $r$,
  poderíamos considerar um vetor $\vector{w}$ que indica
  a direção e o sentido de $r$.
  Como não estamos interessados no \emph{módulo} do vetor $\vector{w}$,
  apenas na direção e no sentido,
  vamos escolher $\vector{w}$ de norma $1$.
  Ou seja,
  vamos definir a projeção $P_{\vector{w}}$,
  onde $\norm{\vector{w}} = 1$.

  \nftikzpic[figure:projection_to_vector]
      {01/05/01/figs/projection_to_vector.tikz}

  O vetor $\vector{v}$ da \cref{figure:projection_to_vector}
  pode ser escrito como
  \begin{equation*}
    \vector{v}
    =
    P_{\vector{w}}(\vector{v})
    \vector{w}
    +
    \vector{z},
  \end{equation*}
  onde $\vector{z}$ é um vetor \emph{``ortogonal''} a $\vector{w}$.
  Assim,
  $P_{\vector{w}}$
  é uma transformação linear.

  \nftikzpic{01/05/01/figs/projection_to_vector-linearity.tikz}

  É claro que,
  se $\norm{\vector{v}} = 1$,
  então
  $P_{\vector{v}}(\vector{v}) = 1$.
  Além disso,
  a propriedade seguinte está relacionada com o ângulo
  entre os vetores $\vector{v}$ e $\vector{w}$.
  \begin{equation*}
    \norm{\vector{v}}
    =
    \norm{\vector{w}}
    =
    1
    \Rightarrow
    P_{\vector{w}}(\vector{v})
    =
    P_{\vector{v}}(\vector{w}).
  \end{equation*}

  \nftikzpic{01/05/01/figs/projection_to_vector-symmetry.tikz}


  Quanto à base canônica
  $\vector{e_1}$,
  $\vector{e_2}$
  e
  $\vector{e_3}$,
  note que
  \begin{equation*}
    P_{\vector{e_j}}(\vector{e_k})
    =
    \delta_{jk},
  \end{equation*}
  onde
  \begin{equation*}
    \delta_{jk}
    =
    \begin{cases}
      1, & j = k
      \\
      0, & j \neq k.
    \end{cases}
  \end{equation*}
  Dizemos que a base canônica é \emph{ortonormal},
  pois cada elemento tem norma $1$,
  e elementos distintos são ortogonais.

  \nftikzpic{01/05/01/figs/projection_to_vector-orthonormal.tikz}

  Observadas essas propriedades,
  vamos encontrar uma fórmula para $P_{\vector{w}}(\vector{v})$
  em termos das coordenadas de
  $\vector{w} = (w_1, w_2, w_3)$
  e
  $\vector{v} = (v_1, v_2, v_3)$.
  Lembre-se que $\norm{\vector{w}} = 1$,
  e note que
  $P_{\vector{e_j}}(\vector{w}) = w_j$.
  Vamos usar as propriedades discutidas anteriormente
  para concluir que
  \begin{align*}
    P_{\vector{w}}(\vector{v})
    &=
    v_1 P_{\vector{w}}(\vector{e_1})
    +
    v_2 P_{\vector{w}}(\vector{e_2})
    +
    v_3 P_{\vector{w}}(\vector{e_3})
    \\
    &=
    v_1 P_{\vector{e_1}}(\vector{w})
    +
    v_2 P_{\vector{e_2}}(\vector{w})
    +
    v_3 P_{\vector{e_3}}(\vector{w})
    \\
    &=
    v_1 w_1
    +
    v_2 w_2
    +
    v_3 w_3.
  \end{align*}

  Se estivéssemos tratando de $\reals^n$,
  com
  $\vector{v} = (v_1, \dotsc, v_n)$
  e
  $\vector{w} = (w_1, \dotsc, w_n)$,
  a fórmula seria
  \begin{align*}
    P_{\vector{w}}(\vector{v})
    &=
    v_1 P_{\vector{w}}(\vector{e_1})
    + \dotsb +
    v_n P_{\vector{w}}(\vector{e_n})
    \\
    &=
    v_1 P_{\vector{e_1}}(\vector{w})
    + \dotsb +
    v_n P_{\vector{e_n}}(\vector{w})
    \\
    &=
    v_1 w_1
    + \dotsb +
    v_n w_n.
  \end{align*}

  Uma maneira natural de se definir
  $P_{\vector{w}}(\vector{v})$
  quando $\vector{w}$ não é unitário,
  seria simplesmente ignorando a norma de $\vector{w}$:
  \begin{equation*}
    P_{\vector{w}}(\vector{v})
    =
    P_{\frac{\vector{w}}{\norm{\vector{w}}}}(\vector{v}).
  \end{equation*}
  Mas pelas considerações feitas até agora,
  uma maneira mais interessante,
  é definindo o \emph{produto interno canônico}.

  \begin{definition}[Produto Interno (Canônico)]
    \label{definition:produto_interno_canonico}
    Sejam
    $\vector{v} = (v_1, \dotsc, v_n)$
    e
    $\vector{w} = (w_1, \dotsc, w_n)$,
    vetores \textbf{quaisquer} de $\reals^n$.
    O \emph{produto interno (canônico)}
    entre $\vector{v}$ e $\vector{w}$,
    denotado por
    $\innerproduct{\vector{v}}{\vector{w}}$
    é dado por
    \begin{equation*}
      \innerproduct{\vector{v}}{\vector{w}}
      =
      \sum_{j=1}^n
      v_j w_j.
    \end{equation*}
  \end{definition}

  Dizemos \emph{``canônico''}
  porque existem outros tipos de \emph{produto interno}.
  Alguns autores chamam o \emph{produto interno canônico}
  de \emph{produto interno euclideano}.
  Outros chamam de \emph{produto escalar}.
  Neste livro trataremos apenas do \emph{produto interno canônico},
  por isso,
  quando dissermos \emph{produto interno}
  estamos nos referindo ao produto definido
  na \cref{definition:produto_interno_canonico}.

  Note que quando $\norm{\vector{w}} = 1$,
  $P_{\vector{w}}(\vector{v}) = \vector{v} \cdot \vector{w}$.
  Nesse sentido,
  o \emph{produto interno} extende a definição de projeção ortogonal
  em um vetor unitário.
  Uma outra maneira de estender a definição de $P_{\vector{w}}$ para o caso em que
  $\vector{w}$ não é necessariamente unitário:
  $P_{\vector{w}} = P_{\frac{\vector{w}}{\norm{\vector{w}}}}$,
  e usando o \emph{produto interno},
  podemos calcular $P_{\vector{w}}$,
  desde que $\vector{w} \neq \vector{0}$.
  O resultado é que para quaisquer
  $\vector{v}, \vector{w} \in \reals^n$,
  com $\vector{w} \neq \vector{0}$,
  \begin{equation*}
    P_{\vector{w}}(\vector{v})
    =
    \frac{\innerproduct{\vector{v}}{\vector{w}}}{\norm{\vector{w}}}.
  \end{equation*}

  O produto interno é mais versátil do que a projeção $P_{\vector{w}}$.
  Em primeiro lugar,
  $P_{\vector{w}}$ não está definido para $\vector{w} = \vector{0}$.
  E depois que $P_{\vector{w}}$ não é linear em $\vector{w}$.
  \begin{align*}
    P_{\alpha \vector{w}}(\vector{v})
    &\neq
    \alpha P_{\vector{w}}(\vector{v})
    \\
    P_{\vector{w} + \vector{z}}(\vector{v})
    &\neq
    P_{\vector{w}}(\vector{v})
    +
    P_{\vector{z}}(\vector{v}).
  \end{align*}

  {\input{01/05/01/01_inner_products_and_functionals}}
