\section{Determinante $n \times n$}
  \label{section:determinant:nxn}

  Na \cref{section:determinant:2x2},
  substituímos a função
  $A(\vector{v}, \vector{w})$
  pela função
  $\determinantop(\vector{w}, \vector{v})$,
  que além de ser linear em cada entrada
  (\emph{bilinear}),
  também é tal que
  a área de uma reta é $0$.
  Ou seja,
  \begin{equation*}
    \determinantop(\vector{w}, \vector{w})
    =
    0.
  \end{equation*}
  Se fosse em $\reals^3$,
  e estivéssemos falando do volume
  $V(\vector{u}, \vector{v}, \vector{w})$,
  talvez fosse mais interessante trabalhar,
  no lugar de $V$,
  com uma função
  $\function{\determinantop}{\reals^3 \times \reals^3 \times \reals^3}{\reals}$,
  \emph{trilinear},
  e tal que se
  $\vector{u}$,
  $\vector{v}$
  e
  $\vector{w}$
  estiverem em um mesmo plano,
  ou seja,
  se forem linearmente dependentes,
  então,
  \begin{equation*}
    \determinantop(\vector{u}, \vector{v}, \vector{w})
    =
    0.
  \end{equation*}


  \nftikzpic{01/05/figs/volume_is_multilinear.tikz}

  No caso $\reals^n$,
  estaríamos definindo uma espécie de
  \emph{volume $n$-dimensional}, com sinal.
  Essa aplicação
  ---
  que também vamos denotar por $\determinantop$
  ---
  atribui a cada sequência de $n$ vetores
  $\vector{v_1}, \dotsc, \vector{v_n} \in \reals^n$,
  o \emph{volume $n$-dimensional} sinalado
  do \emph{``paralelepípedo''} associado a
  $\vector{v_1}, \dotsc, \vector{v_n}$.
  Esperamos que a aplicação
  $\function{\determinantop}{\reals^n \times \dotsb \times \reals^n}{\reals}$
  tenha três propriedades elementares:
  \begin{enumerate}
    \item
      \label{item:determinant_properties:unit}
      Que
      $\determinantop(\vector{e_1}, \dotsc, \vector{e_n}) = 1$.
      Esse é o volume $n$-dimensional do $n$-cubo de lados de tamanho $1$.

    \item
      \label{item:determinant_properties:nlinear}
      Que seja $n$-linear.
      Se fixarmos $n-1$ vetores e os tratarmos como \emph{``base''} do paralelepípedo,
      o volume é ``base $\times$ altura''.
      Assim como no caso $2 \times 2$,
      a ``altura'' é a projeção do $n$-ésimo vetor
      (o que não faz parte da base),
      na direção ortogonal à base.
      E é por isso que o $n$-volume é $n$-linear.

    \item
      \label{item:determinant_properties:linear_dependence}
      Para $\vector{v_1}, \dotsc, \vector{v_n}$,
      se $\vector{v_i} = \vector{v_j}$ para algum $i \neq j$,
      então
      \begin{equation*}
        \determinantop(\vector{v_1}, \dotsc, \vector{v_n})
        =
        0.
      \end{equation*}
  \end{enumerate}

  Será que tal aplicação existe?
  Uma maneira de mostrarmos que existe é exibindo uma fórmula.
  A \cref{proposition:determinant_under_transposition}
  nos permitirá encontrar uma fórmula para o \emph{determinante}.
  A proposição é uma consequência
  do \cref{item:determinant_properties:nlinear,item:determinant_properties:linear_dependence},
  que implica que o sinal do \emph{determinante} se inverte quando permutamos um vetor
  $\vector{v_j}$ com um outro vetor $\vector{v_k}$.

  \begin{proposition}
    \label{proposition:determinant_under_transposition}
    Seja
    \begin{equation*}
      \function{T}{\reals^n \times \dotsb \times \reals^n}{\reals}
    \end{equation*}
    uma aplicação $n$-linear
    tal que
    se $\vector{v_1}, \dotsc, \vector{v_n} \in \reals^n$,
    são \emph{linearmente dependentes},
    então
    \begin{equation*}
      T(\vector{v_1}, \dotsc, \vector{v_n})
      =
      0.
    \end{equation*}
    Nesse caso,
    $T$ é \emph{alternada}.
    Ou seja,
    se $\sigma \in P_n$ é uma permutação de $\set{1, \dotsc, n}$,
    então
    \begin{equation*}
      T(\vector{v_{\sigma(1)}}, \dotsc, \vector{v_{\sigma(n)}})
      =
      \sign(\sigma)
      T(\vector{v_1}, \dotsc, \vector{v_n}).
    \end{equation*}
  \end{proposition}

  \begin{proof}
    Suponha que $\sigma = \transposition{j}{k}$,
    com $j < k$.
    Se,
    no lugar de $v_j$ e $v_k$
    colocarmos $v_j + v_k$,
    o \emph{determinante} será zero,
    pois as entradas $j$ e $k$ serão iguais.
    Assim,
    \begin{align*}
      0
      &=
      T
      (
        \vector{v_1},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_j} + \vector{v_k}$}},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_j} + \vector{v_k}$}},
        \dotsc, \vector{v_n}
      )
      \\
      &=
      T
      (
        \vector{v_1},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[cross out, draw=red!50, thick] (n)
        {$\vector{v_j}$}},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[cross out, draw=red!50, thick] (n)
        {$\vector{v_j}$}},
        \dotsc, \vector{v_n}
      )
      \\&\phantom{=}
      +
      T
      (
        \vector{v_1},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_j}$}},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_k}$}},
        \dotsc, \vector{v_n}
      )
      \\&\phantom{=}
      +
      T
      (
        \vector{v_1},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_k}$}},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_j}$}},
        \dotsc, \vector{v_n}
      )
      \\&\phantom{=}
      +
      T
      (
        \vector{v_1},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[cross out, draw=red!50, thick] (n)
        {$\vector{v_k}$}},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[cross out, draw=red!50, thick] (n)
        {$\vector{v_k}$}},
        \dotsc, \vector{v_n}
      )
      \\
      &=
      T
      (
        \vector{v_1},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_j}$}},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_k}$}},
        \dotsc, \vector{v_n}
      )
      \\&\phantom{=}
      +
      T
      (
        \vector{v_1},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_k}$}},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_j}$}},
        \dotsc, \vector{v_n}
      ).
    \end{align*}
    Mas isso é o mesmo que dizer que
    \begin{equation*}
      T
      (
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_j}$}},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_k}$}},
        \dotsc
      )
      =
      \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n) {$-$}}
      T
      (
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_k}$}},
        \dotsc,
        \tikz[baseline=(n.base)]{\node[rounded corners=10, draw=red!50, thick] (n)
        {$\vector{v_j}$}},
        \dotsc
      ).
    \end{equation*}
    No caso de uma permutação $\sigma$ qualquer,
    podemos escrevê-la como uma sequência de transposições.
    A cada transposição o sinal se inverte,
    de modo que o sinal do resultado final será
    exatamente $\sign(\sigma)$.
  \end{proof}


  Se
  \begin{align*}
    \vector{v_1}
    &=
    (v_{11}, v_{21}, \dotsc, v_{n1})
    \\
    \vector{v_2}
    &=
    (v_{12}, v_{22}, \dotsc, v_{n2})
    \\
    &\vdots
    \\
    \vector{v_n}
    &=
    (v_{1n}, v_{2n}, \dotsc, v_{nn}),
  \end{align*}
  então,
  as propriedades de
  $\function{\determinantop}{\left(\reals^n\right)^n}{\reals}$
  que foram discutidas
  implicam que
  \begin{align*}
    \determinantop(\vector{v_1}, \dotsc, \vector{v_n})
    &=
    \sum_{\sigma \in \set{1, \dotsc, n}^n}
    v_{\sigma(1),1}
    \dotsb
    v_{\sigma(n),n}
    \determinantop(\vector{e_{\sigma(1)}}, \dotsc, \vector{e_{\sigma(n)}})
    \\
    &=
    \sum_{\sigma \in P_n}
    v_{\sigma(1),1}
    \dotsb
    v_{\sigma(n),n}
    \determinantop(\vector{e_{\sigma(1)}}, \dotsc, \vector{e_{\sigma(n)}})
    \\
    &=
    \sum_{\sigma \in P_n}
    \sign(\sigma)
    v_{\sigma(1),1}
    \dotsb
    v_{\sigma(n),n}.
  \end{align*}

  Esta fórmula está \emph{bem definida} porque,
  pela \cref{proposition:permutation_sign_well_defined},
  a aplicação
  $\function{\sign}{P_n}{\set{+1, -1}}$
  está bem definida.
  Assim,
  chegamos a uma fórmula
  que utilizaremos para definir o \emph{determinante}.

  \begin{definition}[Determinante]
    \label{definition:determinant}
    Para cada $n = 1, 2, \dotsc$,
    o \emph{determinante} é a aplicação
    \begin{equation*}
      \functionarray{\determinantop}{\left(\reals^n\right)^n}{\reals}
                    {(\vector{v_1}, \dotsc, \vector{v_n})}
                    {\sum_{\sigma \in P_n} \sign(\sigma) v_{\sigma(1),1} \dotsb v_{\sigma(n),n}}.
    \end{equation*}
    Se $\function{T}{\reals^n}{\reals^n}$ é uma transformação linear,
    \begin{equation*}
      \determinantop(T)
      =
      \determinantop(T\vector{e_1}, \dotsc, T\vector{e_n}).
    \end{equation*}
    E o \emph{determinante} de uma matriz é o \emph{determinante}
    da transformação linear correspondente.
  \end{definition}

  A notação da \cref{definition:determinant}
  usando permutações é bastante útil se soubermos trabalhar com ela.
  A melhor maneira de entender direitinho o significado dessa notação,
  é fazendo alguns casos manualmente.

  \begin{example}[caso $2 \times 2$]
    Seja
    \begin{equation*}
      A
      =
      \matrixrepresentation{a & b \\ c & d}.
    \end{equation*}
    Na notação da \cref{definition:determinant},
    $a_{11} = a$,
    $a_{12} = b$,
    $a_{21} = c$
    e
    $a_{22} = d$.

    As permutações de $\set{1,2}$ são
    $(1,2)$ e $(2,1)$.
    O sinal da primeira é $1$ e o da segunda é $-1$.
    Então,
    \begin{align*}
      \determinantop(A)
      &=
      a_{11}a_{22} + (-1) a_{21} a_{12}
      \\
      &=
      ad - cb.
    \end{align*}
  \end{example}

  Vejamos o caso $3 \times 3$.

  \begin{example}
    Considere a matriz
    \begin{equation*}
      A
      =
      \transposedmatrix{\matrixrepresentation{a & b & c \\ d & e & f \\ g & h & i}}.
    \end{equation*}
    As permutações de $\set{1,2,3}$ que tem sinal positivo
    (permutações pares)
    são as permutações circulares:
    $\rho^0 = (1,2,3)$,
    $\rho^1 = (2,3,1)$
    e
    $\rho^2 = (3,1,2)$.
    A permutação $\rho^0$ não faz nada,
    e equivale a somar $0$.
    Assim,
    $\rho^1$ corresponde a somar $1$,
    e $\rho^2$ corresponde a somar $2$.

    Ao fazer o produto dos números nas entradas das matrizes,
    $\rho^0$ corresponde à \emph{diagonal principal}.
    E de modo geral,
    aplicar $\rho$ significa,
    na matriz \textbf{transposta},
    deslocar a diagonal para a direita.
    Aplicar duas vezes significa deslocar para a direita duas vezes.
    Se não estivéssemos tratando da transposta,
    deveríamos deslocar para baixo,
    já que em nossa fórmula a permutação é aplicada no primeiro índice,
    que se refere à linha da matriz.
    Na \cref{proposition:transposed_matrix_determinant},
    veremos que o determinante de uma matriz é igual ao determinante de sua transposta.

    \nftikzpic{01/05/figs/sarrus_rule.tikz}

    As permutações ímpares são da forma
    $\gamma = (3,2,1)$,
    $\rho^1 \gamma = (1,3,2)$
    e
    $\rho^2 \gamma = (2,1,3)$.
    A primeira, $\gamma$, corresponde à diagonal não principal.
    A segunda e a terceira correspondem
    a deslocar a diagonal principal para a direita $1$ ou $2$ vezes.
  \end{example}

  Para ver que o \emph{determinante} é realmente a função que procurávamos,
  precisamos verificar os três itens
  enumerados antes da \cref{proposition:determinant_under_transposition}.
  Isso fica como dever de casa! :-)

  \begin{proposition}
    \label{proposition:basis_and_determinant}
    Vetores $\vector{v_1}, \dotsc, \vector{v_n} \in \reals^n$
    são uma base de $\reals^n$
    se, e somente se,
    \begin{equation*}
      \determinantop(\vector{v_1}, \dotsc, \vector{v_n})
      =
      0.
    \end{equation*}
  \end{proposition}

  \begin{proof}
    Já sabemos da \cref{proposition:base_in_rn_iff_li_iff_generator},
    que um conjunto de vetores de $\reals^n$ é uma base se,
    e somente se,
    for linearmente independente.
    E isso acontece se, se somente se,
    o conjunto for gerador.

    Se não for uma base,
    ou seja,
    se são linearmente dependentes,
    então
    um dos vetores é combinação linear dos demais.
    Se,
    por exemplo,
    \begin{equation*}
      \vector{v_n}
      =
      \alpha_1 \vector{v_1}
      + \dotsb +
      \alpha_{n-1} \vector{v_{n-1}},
    \end{equation*}
    então
    \begin{align*}
      \determinantop(\vector{v_1}, \dotsc, \vector{v_n})
      &=
      \determinantop(\vector{v_1}, \dotsc, \vector{v_{n-1}}, \alpha_1 \vector{v_1} + \dotsb + \alpha_{n-1} \vector{v_{n-1}})
      \\
      &=
      \alpha_1
      \determinantop(\vector{v_1}, \dotsc, \vector{v_{n-1}}, \vector{v_1})
      + \dotsb
      \\
      &\phantom{+++}
      \dotsb +
      \alpha_{n-1}
      \determinantop(\vector{v_1}, \dotsc, \vector{v_{n-1}}, \vector{v_{n-1}})
      \\
      &=
      \alpha_1
      0
      + \dotsb +
      \alpha_{n-1}
      0
      \\
      &=
      0.
    \end{align*}

    Por outro lado,
    se os vetores formam uma base,
    então podemos escrever
    \begin{align*}
      \vector{e_1}
      &=
      a_{11} \vector{v_1}
      + \dotsb +
      a_{1n} \vector{v_n}
      \\
      &\vdots
      \\
      \vector{e_n}
      &=
      a_{n1} \vector{v_1}
      + \dotsb +
      a_{nn} \vector{v_n}.
    \end{align*}
    Nesse caso,
    \begin{align*}
      1
      &=
      \determinantop(\vector{e_1}, \dotsc, \vector{e_n})
      \\
      &=
      \sum_{\sigma \in \set{1, \dotsc, n}^n}
      a_{\sigma(1),1}
      \dotsb
      a_{\sigma(n),n}
      \determinantop(\vector{v_{\sigma(1)}}, \dotsc, \vector{v_{\sigma(n)}})
      \\
      &=
      \sum_{\sigma \in P_n}
      a_{\sigma(1),1}
      \dotsb
      a_{\sigma(n),n}
      \determinantop(\vector{v_{\sigma(1)}}, \dotsc, \vector{v_{\sigma(n)}})
      \\
      &=
      \left(
        \sum_{\sigma \in P_n}
        \sign(\sigma)
        a_{\sigma(1),1}
        \dotsb
        a_{\sigma(n),n}
      \right)
      \determinantop(\vector{v_1}, \dotsc, \vector{v_n}).
    \end{align*}
    E portanto,
    $\determinantop(\vector{v_1}, \dotsc, \vector{v_n})$ não pode ser nulo.
  \end{proof}

  Temos então um critério simples pra saber se uma matriz
  ou equivalentemente, uma transformação linear,
  é inversível.

  \begin{corollary}
    Uma transformação linear
    $\function{T}{\reals^n}{\reals^n}$
    é inversível se, e somente se,
    $\determinantop(T) \neq 0$.
  \end{corollary}

  \begin{proof}
    Basta juntar a \cref{proposition:basis_and_determinant}
    com o \cref{example:injective_and_surjective:rn},
    que diz que $T$ é inversível se, e somente se,
    $T\vector{e_1}, \dotsc, T\vector{e_n}$ for uma base.
  \end{proof}


  Por vezes,
  o ponto de vista geométrico é mais adequado pra entender uma propriedade das matrizes.
  Por vezes,
  o ponto de vista algébrico é melhor.
  Agora que temos uma fórmula,
  podemos relacionar o determinante de uma matriz
  com o determinante de sua transposta.

  \begin{proposition}
    \label{proposition:transposed_matrix_determinant}
    Seja $A$ uma matriz $n \times n$.
    Então
    \begin{equation*}
      \determinantop\left(\transposedmatrix{A}\right)
      =
      \determinantop(A).
    \end{equation*}
  \end{proposition}

  \begin{proof}
    As permutações tem três propriedades interessantes.
    O leitor é convidado a demonstrá-las.
    \begin{enumerate}
      \item
        A operação
        \begin{equation*}
          \functionarray{\iota}{P_n}{P_n}{\sigma}{\sigma^{-1}}
        \end{equation*}
        é uma bijeção.

      \item
        $\sign(\sigma) = \sign\left(\sigma^{-1}\right)$.

      \item
        Se trocarmos a ordem do produto
        $a_{\sigma(1),1} \dotsb a_{\sigma(n),n}$
        usando a permutação $\gamma$,
        obtemos o mesmo resultado,
        pois o produto de números reais é comutativo:
        \begin{equation*}
          a_{\sigma(\gamma(1)),\gamma(1)} \dotsb a_{\sigma(\gamma(n)),\gamma(n)}
          =
          a_{\sigma(1),1} \dotsb a_{\sigma(n),n}.
        \end{equation*}
    \end{enumerate}

    Usando essas propriedades,
    \begin{align*}
      \determinantop(A)
      &=
      \sum_{\sigma \in P_n}
      \sign(\sigma)
      a_{\sigma(1),1} \dotsb a_{\sigma(n),n}
      \\
      &=
      \sum_{\sigma \in P_n}
      \sign(\sigma)
      a_{\sigma(\sigma^{-1}(1)),\sigma^{-1}(1)} \dotsb a_{\sigma(\sigma^{-1}(n)),\sigma^{-1}(n)}
      \\
      &=
      \sum_{\sigma \in P_n}
      \sign(\sigma)
      a_{1,\sigma^{-1}(1)} \dotsb a_{n,\sigma^{-1}(n)}
      \\
      &=
      \sum_{\sigma \in P_n}
      \sign\left(\sigma^{-1}\right)
      a_{1,\sigma^{-1}(1)} \dotsb a_{n,\sigma^{-1}(n)}
      \\
      &=
      \sum_{\sigma \in P_n}
      \sign(\sigma)
      a_{1,\sigma(1)} \dotsb a_{n,\sigma(n)}
      \\
      &=
      \determinantop\left(\transposedmatrix{A}\right).
    \end{align*}
  \end{proof}
