\section{Determinante $2 \times 2$}
  \label{section:determinant:2x2}

  Nesta seção,
  vamos tratar apenas do espaço $\reals^2$.

  Dois vetores determinam um paralelogramo.
  Assim como o \emph{tamanho da projeção} de um vetor em outro
  tem propriedades interessantes que nos levaram a definir
  o \emph{produto interno},
  a \emph{área do paralelogramo} determinado por dois vetores
  também tem propriedades semelhantes,
  e nos levarão a definir o conceito de \emph{determinante}.
  Apenas nesta seção,
  vamos denotar por $A(\vector{v}, \vector{w})$
  a área do paralelogramo de lados
  $\vector{v}$
  e
  $\vector{w}$.

  \nftikzpic{01/05/02/figs/parallelogram_area.tikz}

  Se $\alpha \geq 0$,
  então
  \begin{equation*}
    A(\alpha \vector{v}, \vector{w})
    =
    \alpha
    A(\vector{v}, \vector{w})
    =
    A(\vector{v}, \alpha \vector{w}).
  \end{equation*}
  Consequentemente,
  \begin{equation*}
    A(\alpha \vector{v}, \beta \vector{w})
    =
    \alpha
    \beta
    A(\vector{v}, \vector{w}).
  \end{equation*}
  E se $\vector{v}$ e $\vector{w}$ são paralelos,
  \begin{equation*}
    A(\vector{v}, \vector{w})
    =
    0.
  \end{equation*}

  \nftikzpic{01/05/02/figs/parallelogram_area-scalar.tikz}


  Quanto à \emph{linearidade},
  fixado o vetor $\vector{w}$,
  a aplicação
  $A(\vector{v}, \vector{w})$
  é \emph{``praticamente'' linear}.
  Lembre-se que os desenhos
  das \cref{figure:area:vector_sum:add,figure:area:vector_sum:subtract}
  \textbf{NÃO} são tridimensionais.
  Como mostrado na \cref{figure:area:vector_sum:add},
  existem casos tais que
  \begin{equation*}
    A(\vector{u} + \vector{v}, \vector{w})
    =
    A(\vector{u}, \vector{w})
    +
    A(\vector{v}, \vector{w}).
  \end{equation*}

  \nftikzpic[figure:area:vector_sum:add]
    {01/05/02/figs/area-vector_sum-add.tikz}

  Mas,
  assim como no caso da projeção,
  existem casos tais que
  \begin{equation*}
    A(\vector{u} + \vector{v}, \vector{w})
    =
    \abs
    {
      A(\vector{u}, \vector{w})
      -
      A(\vector{v}, \vector{w})
    }.
  \end{equation*}

  \nftikzpic[figure:area:vector_sum:subtract]
    {01/05/02/figs/area-vector_sum-subtract.tikz}

  Em analogia com a projeção ortogonal,
  uma pergunta que precisa ser respondida é:
  \begin{quote}
    Quando é que
    $A(\vector{u} + \vector{v}, \vector{w})$
    é igual a
    $A(\vector{u}, \vector{w}) + A(\vector{v}, \vector{w})$,
    e quando é que é igual a
    $\abs{A(\vector{u}, \vector{w}) - A(\vector{v}, \vector{w})}$?
  \end{quote}
  Ou seja,
  se fossemos atribuir um sinal a
  $A(\vector{v}, \vector{w})$,
  qual seria o critério para escolher
  entre \emph{positivo} e \emph{negativo}?

  A área é
  \begin{equation*}
    \text{comprimento da base}
    \times
    \text{altura}.
  \end{equation*}
  O \emph{comprimento da base} é $\norm{\vector{w}}$.
  E a \emph{altura},
  com sinal, é
  \begin{equation*}
    P_{R_{\frac{\pi}{2}}\vector{w}}(\vector{v}),
  \end{equation*}
  onde
  $R_{\frac{\pi}{2}}\vector{w} = (-w_2, w_1)$
  é a rotação por um ângulo $\frac{\pi}{2}$ no sentido anti-horário.
  Assim,
  \begin{align*}
    A(\vector{w}, \vector{v})
    &=
    \norm{\vector{w}}
    \abs{P_{R_{\frac{\pi}{2}}\vector{w}}(\vector{v})}
    \\
    &=
    \norm{\vector{w}}
    \abs{\frac{\vector{v} \cdot R_{\frac{\pi}{2}}\vector{w}}{\norm{R_{\frac{\pi}{2}}\vector{w}}}}
    \\
    &=
    \abs{\innerproduct{\vector{v}}{R_{\frac{\pi}{2}}\vector{w}}}.
  \end{align*}
  Vamos então,
  definir a \emph{``área com sinal''}
  (o \emph{determinante})
  como sendo
  \begin{align*}
    \determinantop(\vector{w}, \vector{v})
    &=
    \innerproduct{\vector{v}}{R_{\frac{\pi}{2}}\vector{w}}
    \\
    &=
    w_1 v_2 - w_2 v_1.
  \end{align*}
  Se o leitor já ouviu falar do determinante,
  provavelmente aprendeu essa fórmula.

  \nftikzpic{01/05/02/figs/area-inner_product.tikz}


  Definido o \emph{determinante},
  vamos entender de modo mais geométrico
  o significado de seu sinal.
  No cálculo de
  $A(\vector{u} + \vector{v}, \vector{w})$,
  quando é que as áreas se somam,
  e quando é que elas se cancelam?

  \nftikzpic{01/05/02/figs/area-vector_sum-parallel.tikz}

  A resposta é simples.
  Se considerarmos a reta $s$ da \cref{figure:area:parallel_lines},
  vemos que quando o vetor $\vector{u}$ está
  \emph{``acima''} de $s$,
  então as áreas se somam.
  Quando está \emph{``abaixo''},
  as áreas se cancelam.

  \nftikzpic[figure:area:parallel_lines]
    {01/05/02/figs/area-vector_sum-compare_to_parallel.tikz}


  E se ao invés de olharmos para a reta $s$,
  olharmos para a reta $r$,
  e se transportarmos o vetor $\vector{u}$
  para o mesmo ponto base que $\vector{v}$,
  vemos que quando
  $\vector{u}$
  e 
  $\vector{v}$
  estão do mesmo lado de $r$,
  as áreas se somam.
  Quando estão de lados opostos,
  as áreas se cancelam.

  \nftikzpic{01/05/02/figs/area-vector_sum-rotate_base.tikz}


  Na notação matricial,
  quando
  $\vector{v} = (v_1, v_2)$
  e
  $\vector{w} = (w_1, w_2)$,
  vamos usar a notação matricial para determinante e escrever
  \begin{equation*}
    \determinantop(\vector{w}, \vector{v})
    =
    \determinantmatrix{w_1 & v_1 \\ w_2 & v_2}.
  \end{equation*}
  Quanto ao sinal,
  $\determinantop(\vector{w}, \vector{v})$ é positivo
  quando
  $\innerproduct{\vector{v}}{R_{\frac{\pi}{2}}\vector{w}}$ é positivo.
  Ou seja,
  quando $\vector{v}$ estiver no semiplano à \emph{``esquerda''} de $\vector{w}$.
  Ou então,
  quando a rotação mais curta de $\vector{w}$ a $\vector{v}$
  for no sentido anti-horário.

  Com toda essa discussão,
  sabemos que
  $\determinantop(\vector{w}, \vector{v})$,
  é linear em $\vector{v}$ (quando fixamos $\vector{w}$).
  Pela fórmula encontrada,
  \begin{equation*}
    \determinantop(\vector{w}, \vector{v})
    =
    -\determinantop(\vector{v}, \vector{w}).
  \end{equation*}
  Se $\vector{v}$ está \emph{``à esquerda''} de $\vector{w}$,
  então,
  $\vector{w}$ está \emph{``à direita''} de $\vector{v}$.
  Isso mostra que
  $\determinantop(\vector{w}, \vector{v})$
  é linear tanto em $\vector{v}$ quanto em $\vector{w}$.
  Ou seja,
  além das propriedades já mostradas,
  também vale que
  \begin{equation*}
    \determinantop(\vector{w} + \vector{z}, \vector{v})
    =
    \determinantop(\vector{w}, \vector{v})
    +
    \determinantop(\vector{z}, \vector{v}).
  \end{equation*}
  Em outras palavras,
  $\determinantop$ é \emph{bilinear}.
  Repare também,
  que
  \begin{equation*}
    \determinantop(\vector{e_1}, \vector{e_2})
    =
    1.
  \end{equation*}

  Pra finalizar essa discussão,
  vamos (re)encontrar a fórmula para
  $\determinantop(\vector{w}, \vector{v})$
  usando apenas as equações
  \begin{align*}
    \determinantop(\vector{e_1}, \vector{e_2})
    &=
    1
    \\
    \determinantop(\vector{e_2}, \vector{e_1})
    &=
    -1
    \\
    \determinantop(\vector{e_1}, \vector{e_1})
    &=
    \determinantop(\vector{e_2}, \vector{e_2})
    =
    0,
  \end{align*}
  e mais o fato de $\determinantop$ ser \emph{bilinear}.

  \begin{example}[fórmula com manipulações algébricas]
    Note que
    $\vector{v} = v_1 \vector{e_1} + v_2 \vector{e_2}$,
    e
    $\vector{w} = w_1 \vector{e_1} + w_2 \vector{e_2}$.
    Assim,
    \begin{align*}
      \determinantop(\vector{w}, \vector{v})
      &=
      w_1
      \determinantop(\vector{e_1}, \vector{v})
      +
      w_2
      \determinantop(\vector{e_2}, \vector{v})
      \\
      &=
      w_1
      \left(
        v_1
        \determinantop(\vector{e_1}, \vector{e_1})
        +
        v_2
        \determinantop(\vector{e_1}, \vector{e_2})
      \right)\\&\phantom{=}
      +
      w_2
      \left(
        v_1
        \determinantop(\vector{e_2}, \vector{e_1})
        +
        v_2
        \determinantop(\vector{e_2}, \vector{e_2})
      \right)
      \\
      &=
      w_1
      \left(
        0
        +
        v_2
      \right)
     +
      w_2
      \left(
        -v_1
        +
        0
      \right)
      \\
      &=
      w_1 v_2 - w_2 v_1.
    \end{align*}
  \end{example}

  \begin{definition}[Determinante $2 \times 2$]
    Se
    $\vector{v} = (v_1, v_2)$
    e
    $\vector{w} = (w_1, w_2)$
    são vetores de $\reals^2$,
    então a quantidade
    \begin{equation*}
      \determinantop(\vector{w}, \vector{v})
      =
      w_1 v_2 - w_2 v_1
    \end{equation*}
    é chamada de \emph{determinante} da matriz
    \begin{equation*}
      \matrixrepresentation{w_1 & v_1 \\ w_2 & v_2}.
    \end{equation*}
    O \emph{determinante} também é denotado por
    \begin{equation*}
      \determinantmatrix{w_1 & v_1 \\ w_2 & v_2}.
    \end{equation*}
  \end{definition}
