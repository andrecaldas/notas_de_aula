\section{Sistemas de Coordenadas}

  Vetores de $\reals^2$ são simplesmente pares de números reais
  que podem ser somados (e subtraídos), e multiplicados por escalar.
  Quando falamos de \emph{pontos e vetores no plano},
  a princípio,
  não estamos falando de $\reals^2$.
  O ponto é esse \emph{``pontinho''} que a gente desenha.

  \nftikzpic{01/01/figs/a_dot.tikz}

  O vetor é essa \emph{``setinha''} que liga dois pontos.

  \nftikzpic{01/01/figs/a_vector_from_a_to_b.tikz}

  Se $A$ e $B$ são pontos e $\vector{v}$ é um vetor,
  faz sentido falar do ponto $A + \vector{v}$,
  e faz sentido falar do vetor $B - A$.
  Mas não faz muito sentido falar de $A + B$.
  A princípio,
  pontos e vetores são coisas diferentes,
  de natureza diferente,
  e muitas vezes é melhor deixá-los assim.
  Mas se quisermos,
  podemos escolher um \emph{ponto base} $O$,
  e então identificar os pontos do plano com os vetores do plano.
  Se falarmos do ponto (correspondente ao vetor) $\vector{v}$,
  na verdade,
  estamos falando do ponto $O + \vector{v}$.
  E se falarmos do vetor (correspondente ao ponto) $A$,
  na verdade,
  estamos nos referindo ao vetor $A - O$.

  \nftikzpic{01/01/figs/identify_dots_and_vectors.tikz}

  Com essa interpretação geométrica feita até agora,
  não faz muito sentido somar um vetor $B - A$,
  que sai de $A$ e vai pra $B$,
  com um vetor $D - C$,
  que sai de um outro ponto $C \neq B$,
  e vai até $D$!
  

  \nftikzpic{01/01/figs/summing_with_different_base_points.tikz}


  Tudo o que fizemos para pontos no plano,
  poderia ter sido feito para pontos no espaço.
  Estamos então,
  com duas noções diferentes de \emph{vetores}.
  Se pensarmos em $\reals^2$
  ---
  ou de modo mais geral,
  $\reals^n$
  ---
  temos pares de números,
  com operação de soma e produto por escalar
  feitos \emph{coordenada a coordenada}.
  Se pensarmos no plano
  ---
  ou no espaço
  ---
  temos \emph{``pontinhos''} e \emph{``setinhas''}.
  O coneceito que vai fazer a ligação entre o plano e $\reals^2$,
  é o \emph{sistema de coordenadas}.
  O \emph{eixo coordenado} serve para especificarmos um vetor $\vector{v}$
  em termos de suas \emph{coordenadas}:
  $\vector{v} = (v_1, v_2)$.

  \nftikzpic{01/01/figs/axis.tikz}


  Com a interpretação geométrica usando \emph{``pontos''} e \emph{``setas''},
  mesmo sem eixos coordenados,
  podíamos falar de adição e produto por escalar.
  Com um \emph{sistema de coordenadas},
  fica mais fácil identificar vetores com diferentes \emph{pontos base}
  e tratá-los como se fossem a mesma coisa.
  Até então,
  pensando sobre \emph{``deslocamento resultante''},
  fazia sentido \emph{somar} deslocamentos quando o ponto final de um
  fosse igual ao ponto inicial do próximo.

  \nftikzpic{01/01/figs/two_consecutive_displacements.tikz}

  O vetor $\vector{v}$,
  saindo do ponto $A$ com coordenadas $(a_1, a_2)$
  e indo para o ponto $B$ de coordenadas $(b_1, b_2)$
  será identificado com o vetor $(b_1 - a_1, b_2 - a_2) \in \reals^2$.
  Usando essa identificação com $\reals^2$,
  fica mais fácil falar de soma de vetores do plano
  sem nos preocuparmos se o ponto final de um vetor é igual ao ponto inicial do outro.

  \nftikzpic{01/01/figs/addition_is_commutative.tikz}

  Sem essa preocupação,
  se podemos falar de $\vector{v} + \vector{w}$,
  podemos falar em $\vector{w} + \vector{v}$.
  Os eixos coordenados podem ser muito úteis 
  para fazermos contas e especificarmos os vetores de uma maneira concreta.
  É útil perceber que
  \begin{align*}
    \alpha (v_1, v_2)
    &=
    (\alpha v_1, \alpha v_2)
    \\
    (v_1, v_2) + (w_1, w_2)
    &=
    (v_1 + w_1, v_2 + w_2).
  \end{align*}
  Ou seja,
  as operações são feitas coordenada a coordenada.
  E portanto,
  com toda essa construção no plano,
  a soma de vetores do plano
  é exatamente a mesma coisa que a soma de vetores de $\reals^2$,
  que já havíamos definido.
  Somar vetores em um mesmo ponto base corresponde a desenhar um paralelogramo.
  Veja a figura \ref{figure:vector:addition_with_translation}.

  \nftikzpic[figure:vector:addition_with_translation]
    {01/01/figs/addition_with_a_parallelogram.tikz}


  Agora temos intuição \emph{geométrica} (desenho)
  e intuição \emph{algébrica} (cálculos) sobre um mesmo conceito.
  A intuição \emph{geométrica} não é melhor que a \emph{algébrica},
  ou vice-versa.
  É importante conhecermos as duas e lançarmos mão da que for mais adequada
  ao problema sendo tratado.
  Por exemplo,
  algebricamente é muito fácil ver que a soma de vetores é comutativa.
  Geometricamente,
  na opinião do autor,
  é um pouco mais complicado.

  \nftikzpic{01/01/figs/basic_operations_using_coordinates.tikz}

  Por outro lado,
  suponha que você esteja interessado em investigar combinações de dois vetores
  $\vector{v}$ e $\vector{w}$ que sejam do tipo
  \begin{equation*}
    \alpha \vector{v}
    +
    \beta \vector{w},
  \end{equation*}
  onde $\alpha \geq 0$ e $\beta \geq 0$.
  Geometricamente é bastante fácil ver
  quais vetores podem ou não podem serem escritos dessa maneira.

  \nftikzpic{01/01/figs/cone.tikz}


  Também é fácil imaginar uma representação geométrica para $\reals^3$.
  Para $\reals^4$,
  muita gente gosta de pensar na quarta dimensão como se fosse o \emph{``tempo''}.
  O leitor pode pensar no tempo, se preferir.
  Mas lembre-se que $\reals^n$ é simplesmente uma $n$-upla ordenada
  com operações de \emph{soma} e \emph{produto por escalar} definidos.
  E,
  como no \cref{example:potato_and_carrot_as_vectors},
  é bem possível que a quarta dimensão seja a \emph{``couve''}!!!
  Quando o número de dimensões aumenta muito,
  a geometria continua nos fornecendo ideias e intuição.
  Mas às vezes, o mais fácil é pensar
  nos elementos de $\reals^4$ ou $\reals^{23}$
  como sendo sequências de $4$ ou $23$ números, respectivamente.
