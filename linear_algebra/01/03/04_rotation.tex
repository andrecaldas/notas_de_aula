\section{Rotação}

  A transformação
  $\function{R_\alpha}{\reals^2}{\reals^2}$,
  que leva um vetor $\vector{v}$ em sua rotação por um ângulo $\alpha$,
  é uma transformação linear.
  Você consegue se convencer,
  ou convencer alguém disso?

  \nftikzpic{01/03/figs/matrix_representation-rotation.tikz}

  De fato,
  a rotação é uma transformação muito mais simples que a da
  \emph{``casa inclinada''}
  (da \cref{section:slanted_house}),
  já que os eixos são apenas rotacionados.
  Os eixos não são esticados
  e o ângulo entre eles continua sendo reto.
  Na \cref{section:slanted_house},
  os \emph{quadradinhos} do quadriculado foram transformados em paralelogramos.
  Agora,
  os \emph{quadradinhos} continuam \emph{quadradinhos},
  só que rotacionados.
  Vamos chamar essa transformação de $R_\alpha$.
  A representação matricial de $R_\alpha$ é
  \begin{equation*}
    \matrixrepresentation{R_\alpha}
    =
    \matrixrepresentation{\cos(\alpha) & -\sin(\alpha) \\ \sin(\alpha) & \cos(\alpha)}.
  \end{equation*}
  De fato,
  como $\vector{v_1}$ faz um ângulo $\alpha$ com o eixo das abcissas,
  \begin{equation*}
    \vector{v_1}
    =
    (\cos(\alpha), \sin(\alpha)).
  \end{equation*}
  Agora,
  é só observar que $\vector{v_2}$ é o $\vector{v_1}$ rotacionado $90°$,
  como na \cref{section:90_degree_rotation},
  para concluir que
  \begin{equation*}
    \vector{v_2}
    =
    (-\sin(\alpha), \cos(\alpha)).
  \end{equation*}


  Para o leitor que ainda não está convencido da utilidade
  de tudo isso que tem sido discutido,
  o próximo exemplo é,
  na opinião do autor,
  bastante impressionante.

  \begin{example}[Soma de ângulos]
    \label{example:agle_sum:matrix_vector}

    Dados dois ângulos, $\alpha$ e $\beta$,
    existe uma fórmula para o cálculo de
    \begin{equation*}
      \sin(\alpha + \beta)
      \quad\text{e}\quad
      \cos(\alpha + \beta).
    \end{equation*}
    Vamos deduzir essas fórumlas. :-)


    Por um lado (lado esquerdo da igualdade)
    sabemos que ao rotacionar por um ângulo $\alpha$
    o vetor $\vector{v} = (\cos(\beta), \sin(\beta))$,
    obtemos o vetor $(\cos(\alpha+\beta), \sin(\alpha+\beta))$.
    Por outro lado (lado direito),
    usando a \emph{linearidade} da aplicação $R_\alpha$,
    \begin{align*}
      \matrixrepresentation
      {\cos(\alpha+\beta) \\ \sin(\alpha+\beta)}
      &=
      \matrixrepresentation{R_\alpha \vector{v}}
      \\
      &=
      \matrixrepresentation{\cos(\alpha) & -\sin(\alpha) \\ \sin(\alpha) & \cos(\alpha)}
      \matrixrepresentation{\cos(\beta) \\ \sin(\beta)}
      \\
      &=
      \cos(\beta)
      \matrixrepresentation{\cos(\alpha) \\ \sin(\alpha)}
      +
      \sin(\beta)
      \matrixrepresentation{-\sin(\alpha) \\ \cos(\alpha)}
      \\
      &=
      \matrixrepresentation
      {
        \cos(\alpha)
        \cos(\beta)
        -
        \sin(\alpha)
        \sin(\beta)
        \\
        \cos(\beta)
        \sin(\alpha)
        +
        \sin(\beta)
        \cos(\alpha)
      }.
    \end{align*}
    Ou seja,
    \begin{align*}
      \cos(\alpha+\beta)
      &=
      \cos(\alpha)
      \cos(\beta)
      -
      \sin(\alpha)
      \sin(\beta)
      \\
      \sin(\alpha+\beta)
      &=
      \sin(\alpha)
      \cos(\beta)
      +
      \cos(\alpha)
      \sin(\beta).
    \end{align*}
    Agora,
    o leitor é convidado a procurar a demonstração dessas fórmulas
    em qualquer livro do ensino médio!
  \end{example}

  Poderíamos fazer esses cálcuos apenas com matrizes de rotação.

  \begin{example}
    Uma outra maneira de expressar a soma de ângulos em termos de matrizes,
    é calculando
    $R_{\alpha + \beta} = R_\alpha \circ R_\beta$.
    \begin{equation*}
      \matrixrepresentation{\cos(\alpha+\beta) & -\sin(\alpha+\beta) \\ \sin(\alpha+\beta) & \cos(\alpha+\beta)}
      =
      \matrixrepresentation{\cos(\alpha) & -\sin(\alpha) \\ \sin(\alpha) & \cos(\alpha)}
      \matrixrepresentation{\cos(\beta) & -\sin(\beta) \\ \sin(\beta) & \cos(\beta)}.
    \end{equation*}
    Os cálculos são essencialmente os mesmos do \cref{example:agle_sum:matrix_vector}.
  \end{example}

  \begin{example}[Inversa da rotação]
    Às vezes,
    usamos a álgebra de matrizes para calcular coisas geométricas,
    como a soma de ângulos.
    E às vezes usamos geometria para não precisarmos calcular as matrizes{\ldots}
    
    Mesmo sem fazer cálculos com matrizes,
    sabemos que
    \begin{equation*}
      \matrixrepresentation{\cos(\alpha) & -\sin(\alpha) \\ \sin(\alpha) & \cos(\alpha)}
      \matrixrepresentation{\cos(\alpha) & \sin(\alpha) \\ -\sin(\alpha) & \cos(\alpha)}
      =
      \matrixrepresentation{1 & 0 \\ 0 & 1}.
    \end{equation*}
    Isso, porque
    $R_\alpha^{-1} = R_{-\alpha}$.
    E também, porque
    $\cos(-\alpha) = \cos(\alpha)$
    e
    $\sin(-\alpha) = -\sin(\alpha)$.
  \end{example}
