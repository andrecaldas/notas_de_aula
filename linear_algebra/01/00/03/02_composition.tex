\subsection{Composição de funções}

  É importante que o leitor tenha em mente,
  que uma função não é uma fórmula,
  mas uma correspondência.
  Por vezes,
  podemos expressar essa correspondência usando uma fórmula.
  A rigor,
  não deveríamos dizer que $f(x)$ é uma função.
  A função é $f$.
  Enquanto que $f(x)$ é o valor que $f$ toma em $x$.

  Podemos imaginar uma função
  $\function{f}{A}{B}$
  como uma máquina,
  um dispositivo{\ldots}
  uma \emph{``caixinha''}!

  \nftikzpic[function_as_a_box]
    {01/00/03/figs/function_as_a_box.tikz}

  Um dipositivo que sempre que recebe uma entrada (input) $x$,
  emite uma resposta (output) $f(x)$.
  Se é,
  por exemplo,
  uma máquina onde você coloca uma fruta e sai suco dessa fruta,
  não podemos colocar uma bola de tênis!
  Os únicos \emph{inputs} permitidos são as frutas.
  Ou seja,
  o \emph{domínio} de $f$ é o conjunto das frutas.
  É bom também deixarmos claro quem é o contradomínio.
  Poderia ser o conjunto de todos os tipos de bebida,
  ou o conjunto de todos os tipos de líquido,
  ou simplesmente,
  o conjunto de todos os tipos de suco de fruta.

  \nftikzpic[fruit_juice_function]
    {01/00/03/figs/fruit_juice_function.tikz}

  Imagine uma função
  \begin{equation*}
    \function{g}{\text{SUCOS}}{\text{PICOLÉS}}.
  \end{equation*}
  Se ligássemos a saída de $f$ com a entrada de $g$,
  teríamos uma máquina
  \begin{equation*}
    \function{g \circ f}{\text{FRUTAS}}{\text{PICOLÉS}}.
  \end{equation*}

  \nftikzpic[fruit_to_popsicle]
    {01/00/03/figs/fruit_to_popsicle.tikz}

  Dadas duas funções
  $\function{f}{A}{B}$
  e
  $\function{g}{B}{C}$.
  Definimos a composição $g \circ f$,
  como sendo a função de $A$ em $C$ que toma $a \in A$,
  determina o elemento de $B$ correspondente, $b = f(a)$,
  e a partir desse elemento,
  encontra seu correspondente em $C$
  através de $g$: $g(f(a)) = g(b)$.
  De modo mais rigoroso,
  \begin{equation*}
    \functionarray{g \circ f}{A}{C}{x}{g(f(x))}.
  \end{equation*}

  Se $\function{f}{A}{B}$ é uma bijeção,
  então $\function{f^{-1}}{B}{A}$ é justamente a função que satisfaz
  \begin{equation*}
    \functionarray{f \circ f^{-1}}{B}{B}{x}{x}
    \quad\text{e}\quad
    \functionarray{f^{-1} \circ f}{A}{A}{x}{x}.
  \end{equation*}
  Para cada conjunto $A$,
  a função
  \begin{equation*}
    \functionarray{\identity}{A}{A}{x}{x}
  \end{equation*}
  é chamada de função identidade.
  Usamos a mesma notação $\identity$ para todas essas funções.
  Mas cada $A$ corresponde a uma função $\identity$ diferente.

  A função \emph{``suco de fruta''} é inversível?
  E a função \emph{``picolé''},
  que pega um suco e leva num picolé?
  Bem{\ldots}
  a função \emph{``picolé''} parece ser inversível,
  já que basta derreter o picolé para ter o suco de volta.

  \nftikzpic[popsicle_to_juice]
    {01/00/03/figs/popsicle_to_juice.tikz}

  \emph{Fisicamente},
  parece ser impossível inverter a função \emph{``suco de fruta''}.
  Mas os matemáticos não estão muito interessados em saber se é
  \emph{fisicamente} possível ou não.
  No nosso caso,
  inverter significa fazer a correspondência inversa.
  No caso do suco,
  significa dizer de qual fruta veio o suco ``$x$''.
  Ora{\ldots}
  veio da fruta ``$x$''!!!

  \begin{example}
    A função
    \begin{equation*}
      \functionarray{f}{\reals}{\reals}{x}{\abs{x}}
    \end{equation*}
    não é inversível.
    Mas a função
    \begin{equation*}
      \functionarray{g}{\setsuchthat{x \in \reals}{x \leq 0}}{\setsuchthat{x \in \reals}{x \geq 0}}{x}{\abs{x}}
    \end{equation*}
    é inversível.
  \end{example}


  Podemos complicar ainda mais,
  e pensar na composição de mais funções{\ldots}
  \\
  E se
  $\function{f}{C}{D}$,
  $\function{g}{B}{C}$
  e
  $\function{h}{A}{B}$,
  qual é o significado de
  $f \circ g \circ h$,
  $f \circ (g \circ h)$
  e
  $(f \circ g) \circ h$?
  O primeiro deles é a função que toma $x \in A$,
  calcula $h(x)$, depois $g(h(x))$ e então resulta em $f(g(h(x)))$.
  Por outro lado,
  fazendo $G = g \circ h$,
  \begin{equation*}
    f \circ (g \circ h)
    =
    f \circ G.
  \end{equation*}
  Ou seja,
  $f \circ (g \circ h)$
  é a função que pega $x \in A$,
  aplica $G$ e finalmente resulta em $f(G(x))$.
  Mas $G(x)$ é justamente $g(h(x))$.
  Portanto,
  para todo $x \in A$,
  \begin{align*}
    (f \circ (g \circ h))(x)
    &=
    f(G(x))
    \\
    &=
    f(g(h(x)))
    \\
    &=
    (f \circ g \circ h)(x).
  \end{align*}
  Ou seja,
  $f \circ (g \circ h) = f \circ g \circ h$.
  E por outro lado,
  fazendo $F = f \circ g$,
  temos que
  $(f \circ g) \circ h = F \circ h$
  é a função que toma $x \in A$,
  calcula $h(x)$ e depois,
  calcula $F(h(x))$.
  Mas qualquer que seja $b \in B$,
  $F(b) = f(g(b))$.
  Em particular,
  quando $b = h(x)$,
  temos que
  \begin{align*}
    ((f \circ g) \circ h)(x)
    &=
    F(h(x))
    \\
    &=
    F(b)
    \\
    &=
    f(g(b))
    \\
    &=
    f(g(h(x)))
    \\
    &=
    (f \circ g \circ h)(x).
  \end{align*}

  O que mostramos,
  é que a operação de $\circ$ é \emph{associativa}:
  \begin{equation*}
    (f \circ g) \circ h
    =
    f \circ (g \circ h).
  \end{equation*}
  Por isso,
  tanto faz escrever
  $(f \circ g) \circ h$,
  $f \circ (g \circ h)$
  ou
  $f \circ g \circ h$.

  \begin{obs}
    A composição de funções é \emph{associativa}:
    \begin{equation*}
      (f \circ g) \circ h
      =
      f \circ (g \circ h).
    \end{equation*}
    Mas a composição não é \emph{comutativa}!
    Ou seja,
    pode acontecer de $f \circ g$ ser diferente de $g \circ f$.

    Por exemplo,
    se
    \begin{align*}
      &\functionarray{f}{\reals}{\reals}{x}{\abs{x}}
      \\
      &\functionarray{g}{\reals}{\reals}{x}{-x},
    \end{align*}
    então
    \begin{align*}
      &\functionarray{f \circ g}{\reals}{\reals}{x}{\abs{x}}
      \\
      &\functionarray{g \circ f}{\reals}{\reals}{x}{-\abs{x}}.
    \end{align*}

    Na verdade,
    pode acontecer de $f \circ g$ fazer sentido,
    mas $g \circ f$ não estar bem definida.
    Por exemplo,
    \begin{align*}
      &\functionarray{f}{\reals}{\reals}{x}{-x}
      \\
      &\functionarray{g}{\setsuchthat{x \in \reals}{x \geq 0}}{\reals}{x}{\sqrt{x}}.
    \end{align*}
  \end{obs}

  % TODO: exercícios de composição e inversa.
  % TODO: exercícios de injetividade/sobrejetividade.
