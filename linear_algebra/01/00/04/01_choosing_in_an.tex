\subsection{Escolhendo em $A^n$}

  No \cref{example:sum_power}
  vimos como o conjunto $A^n$ representa todas as escolhas possíveis
  de uma sequência de $n$ elementos de $A$.
  Vamos exercitar um outra maneira de expressar o conjunto $A^n$.

  Quando o conjunto tem $k$ elementos e é da forma
  \begin{equation*}
    A = \set{x_1, x_2, \dotsc, x_k},
  \end{equation*}
  então cada $a \in A^n$ corresponde a uma função
  \begin{equation*}
    \function{f}{\set{1, \dotsc, n}}{\set{1, \dotsc, k}}.
  \end{equation*}
  Para cada $f$ temos o vetor
  \begin{equation*}
    a^f
    =
    (x_{f(1)}, \dotsc, x_{f(n)}).
  \end{equation*}
  Para facilitar,
  vamos denotar por
  \begin{equation*}
    [n]
    =
    \set{1, \dotsc, n}
  \end{equation*}
  o conjunto dos números naturais de $1$ até $n$.

  \begin{example}
    \label{example:sum_power:indexes}
    Continuando o \cref{example:sum_power},
    ao invés de escrevermos
    \begin{equation*}
      (x + y)^n
      =
      \sum_{a \in \set{x,y}^n} a_1 \dotsb a_n,
    \end{equation*}
    podemos escrever
    \begin{equation*}
      (x_1 + x_2)^n
      =
      \sum_{\function{f}{[n]}{[2]}} x_{f(1)} \dotsb x_{f(n)},
    \end{equation*}
    onde a soma é feita sobre todas as funções $\function{f}{[n]}{[2]}$.
  \end{example}

  O objetivo desta seção é que o leitor se acostume com esse tipo de notação.
  Vejamos um exemplo mais complicado.

  \begin{example}
    \label{example:multilinear:polynome}
    Seja $P$ o cojunto dos polinômios em $x$.
    Suponha que a função
    \begin{equation*}
      \function{G}{P \times P}{\reals}
    \end{equation*}
    satisfaça as seguintes propriedades:
    \begin{enumerate}
      \item
        $G(p(x) + q(x), r(x)) = G(p(x), r(x)) + G(q(x), r(x))$.

      \item
        $G(\alpha p(x), r(x)) = \alpha G(p(x), r(x))$.

      \item
        $G(p(x), r(x) + s(x)) = G(p(x), r(x)) + G(p(x), s(x))$.

      \item
        $G(p(x), \alpha r(x)) = \alpha G(p(x), r(x))$.

      \item
        $G(x^m, x^n) = m^4 - n^3$.
    \end{enumerate}
    Então,
    por exemplo,
    \begin{align*}
      G(2 x^2, 5 x^3)
      &=
      2 G(x^2, 5 x^3)
      \\
      &=
      10 G(x^2, x^3)
      \\
      &=
      10 (2^4 - 3^3)
      \\
      &=
      10 (16 - 27)
      =
      -110.
    \end{align*}
    Ou então,
    \begin{align*}
      G(2 x^2 + 3 x^3, 7 x + 5 x^3)
      &=
      G(2 x^2, 7 x + 5 x^3) + G(3 x^3, 7 x + 5 x^3)
      \\
      &=
      G(2 x^2, 7 x) + G(2 x^2, 5 x^3)
      +
      G(3 x^3, 7 x) + G(3 x^3, 5 x^3)
      \\
      &=
      14 G(x^2, x) + 10 G(x^2, x^3)
      +
      21 G(x^3, x) + 15 G(x^3, x^3)
      \\
      &=
      \dotsb
    \end{align*}

    De um modo geral,
    se
    $p(x) = a_1 x^{k-1} + a_2 x^{k-2} + \dotsb + a_k x^0$
    e
    $q(x) = b_1 x^{k-1} + b_2 x^{k-2} + \dotsb + b_k x^0$,
    onde $k$ é o máximo dos graus dos polinômios $p$ e $q$.
    Então,
    \begin{align*}
      G(p(x), q(x))
      &=
      \sum_{\function{f}{[2]}{[k]}}
      a_{f(1)}
      b_{f(2)}
      G(x^{(k-f(1))}, x^{(k-f(2))})
      \\
      &=
      \sum_{\function{f}{[2]}{[k]}}
      a_{f(1)}
      b_{f(2)}
      \left(
        (k-f(1))^4 - (k-f(2))^3
      \right).
    \end{align*}
  \end{example}

  Note que as aplicações de $[n]$ em $[k]$
  podem ser identificadas com os elementos de $[k]^n$.
  Por exemplo,
  $(3, 4, 7, 1, 1)$
  pode ser usada pra representar a aplicação
  $\function{f}{[5]}{[23]}$
  tal que
  $f(1) = 3$,
  $f(2) = 4$,
  $f(3) = 7$,
  $f(4) = 1$
  e
  $f(5) = 1$.
