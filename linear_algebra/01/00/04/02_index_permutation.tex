\subsection{Permutando Índices}

  Quando estudarmos \emph{determinante}
  (\cref{section:determinant:nxn}),
  iremos precisar de um caso particular
  do que acaba de ser discutido sobre $A^n$.

  Quando
  \begin{equation*}
    A
    =
    \set{x_1, x_2, \dotsc, x_n},
  \end{equation*}
  cada elementos de $A^n$ corresponde a uma função
  \begin{equation*}
    \function{f}{[n]}{[n]}.
  \end{equation*}
  Exigir que $f$ seja injetiva
  ---
  ou, equivalentemente, bijetiva
  ---
  é o mesmo que exigir que o elemento correspondente
  $a^f \in A^n$
  não possua entradas repetidas.
  Neste caso,
  dizemos que $a^f$ é uma das possíveis permutações dos elementos de $A$.
  Essencialmente,
  é como se estivéssemos reordenando os $n$ elementos de $A$.
  Vamos denotar por
  \begin{equation*}
    \permutations{n}
    =
    \setsuchthat{\function{f}{[n]}{[n]}}{\text{$f$ é bijetiva}}
  \end{equation*}
  o conjunto de todas as \emph{permutações} de $[n]$.

  \begin{example}
    \label{example:permutation_with_symbols}
    Ao invés de ficarmos falando só de números,
    podemos falar de permutação de um conjunto qualquer.
    Suponha que
    \begin{equation*}
      A
      =
      \set{\text{🐮}, \text{🐱}, \text{🐵}}.
    \end{equation*}
    Vamos denotar por
    \begin{equation*}
      \permutations{A}
      =
      \setsuchthat{\function{f}{A}{A}}{\text{$f$ é bijetiva}}
    \end{equation*}
    o conjunto de todas as permutações
    ---
    ou seja, bijeções
    ---
    de elementos de $A$.

    Cada elemento de $\permutations{A}$ pode ser representado
    graficamente de diversas formas.
    Vejamos duas formas diferentes para
    $f\left(\text{🐮}\right) = \text{🐵}$,
    $f\left(\text{🐱}\right) = \text{🐮}$
    e
    $f\left(\text{🐵}\right) = \text{🐱}$,
    na \cref{figure:graphical_representation:symbols:silly}
    e na \cref{figure:graphical_representation:symbols}.

    \nftikzpic[figure:graphical_representation:symbols:silly]{01/00/04/figs/graphical_representation_symbols_silly.tikz}
    \nftikzpic[figure:graphical_representation:symbols]{01/00/04/figs/graphical_representation_symbols.tikz}
  \end{example}

  Trabalhar com números naturais tem uma vantagem!
  É que podemos assumir que a ordem de ``referência'' é $(1, 2, \dotsc, n)$.
  Ao invés de escrever
  \begin{equation*}
    \sigma(1) = 2,
    \sigma(2) = 3
    \text{ e }
    \sigma(3) = 1,
  \end{equation*}
  vamos escrever $\sigma = (2, 3, 1)$.
  Dessa forma,
  estamos identificando os elementos de $\permutations{3}$
  com os elementos de $\set{1,2,3}^3$ que não possuem entradas repetidas.
  Ou seja,
  \begin{equation*}
    \permutations{n}
    \equiv
    \setsuchthat{\sigma \in [n]^n}{i \neq j \Rightarrow \sigma(i) \neq \sigma(j)}.
  \end{equation*}

  Essencialmente,
  estamos fazendo como na \cref{figure:graphical_representation:symbols:silly},
  de modo que a parte de cima da representação gráfica seja sempre a sequência $1, 2, 3$.

  \nftikzpic[figure:graphical_representation:numbers:silly]{01/00/04/figs/graphical_representation_numbers_silly.tikz}

  Mas isso também tem uma desvantagem.
  É que a representação da \cref{figure:graphical_representation:symbols}
  é melhor para concatenar duas permutações.
  Ou seja,
  queremos compor a função $\sigma$ do parágrafo anterior,
  com a função $\gamma = (1, 3, 2)$,
  por exemplo.
  No caso de permutações,
  vamos usar a notação $\gamma \sigma$ no lugar de $\gamma \circ \sigma$.

  Note que $\gamma$ leva $(1, 2, 3)$ em $(1, 3, 2)$
  pode ser representada graficamente como a permutação
  que leva $(2, 3, 1)$ em $(3, 2, 1)$.
  Agora sim,
  reorganizando a representação de $\gamma$ como feito
  na \cref{figure:graphical_representation:composition:silly},
  é fácil ver que
  \begin{equation*}
    \gamma \sigma
    =
    (3, 2, 1).
  \end{equation*}

  \nftikzpic[figure:graphical_representation:composition:silly]{01/00/04/figs/graphical_representation_composition_silly.tikz}

  Por outro lado,
  se usarmos uma representação gráfica como da \cref{figure:graphical_representation:composition},
  fica fácil concatenar duas representações gráficas e obter uma nova representação gráfica.

  \nftikzpic[figure:graphical_representation:composition]{01/00/04/figs/graphical_representation_composition.tikz}
