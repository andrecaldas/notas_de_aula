\subsection{Sinal da Permutação}

  Neste curso,
  será importante estudar permutações
  para que possamos entender com facilidade o conceito de \emph{determinante},
  que será apresentado na \cref{section:determinant:nxn}.
  Uma propriedade muito importante das permutações
  é que a cada permutação podemos associar um \emph{sinal}.

  Depois de compor várias permutações,
  a representação gráfica pode ficar bem ``trançada'',
  como na \cref{figure:many_compositions}.

  \nftikzpic[figure:many_compositions]{01/00/04/figs/many_compositions.tikz}

  Para que as observações da \cref{figure:many_compositions} sejam verdadeiras,
  precisamos sempre tomar o cuidado de não permitir
  que três linhas se cruzem em um mesmo ponto,
  ou que duas linhas se tangenciem sem se cruzar.
  Tomando tais cuidados,
  ao eliminar ou produzir novos cruzamentos,
  os cruzamentos sempre desaparecem ou aparecem em \textbf{pares}.

  \nftikzpic[figure:prohibited_graphical_representations]{01/00/04/figs/prohibited_graphical_representations.tikz}

  \begin{example}
    O número mínimo de interseções de $\sigma = (3, 2, 1)$ é $3$.
    O de $\gamma = (2, 3, 1)$ é $1$.
    Assim,
    o número mínimo de interseções de $\sigma \gamma = (2, 3, 1)$,
    apesar de não ser igual a $3 + 1$,
    deve ser um número par.
    De fato,
    podemos ver na \cref{figure:composition_intersection_counting}.

    \nftikzpic[figure:composition_intersection_counting]{01/00/04/figs/composition_intersection_counting.tikz}
  \end{example}

  Sendo assim,
  podemos definir o sinal de uma permutação de acordo com sua paridade.
  \begin{equation*}
    \sign(\sigma)
    =
    (-1)^{\text{Nº de cruzamentos em qualquer representação gráfica}}.
  \end{equation*}
  Essa fórmula está bem definida?

  % TODO: exercício -- e se permutarmos a sequência base?

  Pelas considerações feitas até aqui,
  \begin{align*}
    \sign(\sigma\gamma)
    &=
    (-1)^{\text{paridade de $\sigma$} + \text{paridade de $\gamma$}}
    \\
    &=
    (-1)^{\text{paridade de $\sigma$} + \text{paridade de $\gamma$}}
    \\
    &=
    (-1)^{\text{paridade de $\sigma$}} (-1)^{\text{paridade de $\gamma$}}
    \\
    &=
    \sign(\sigma) \sign(\gamma).
  \end{align*}

  Uma permutação $\sigma$ que troca dois números $i$ e $j$ e deixa os demais fixos
  é denotada por $\sigma = \transposition{i}{j}$
  e chamada de \emph{permutação simples} ou \emph{transposição}.
  As permutações simples tem duas propriedades especialmente interessantes.

  \begin{lemma}
    \label{lemma:transposition_sign}
    Se $\sigma$ é simples, então
    \begin{equation*}
      \sign(\sigma)
      =
      -1.
    \end{equation*}
  \end{lemma}

  \begin{proof}
    Assumindo que $i < j$,
    basta notar
    ---
    observando a \cref{figure:graphical_representation:transposition}
    ---
    que as linhas saindo de $i$ e $j$ se cruzam uma vez (ímpar)
    e depois cada uma das duas cruza as retas $i+1$ até $j-1$ (par).
    Assim,
    o número de cruzamentos é ímpar.
    Ou seja,
    $\sign(\sigma) = -1$.
  \end{proof}

  \begin{lemma}
    \label{lemma:permutation_is_product_of_transpositions}
    Toda permutação $\sigma$ pode ser escrita
    como composição de transposições.
  \end{lemma}

  \begin{proof}
    Observe que $\sigma$ pode ser decomposta em um produto de
    \emph{permutações cíclicas}:
    \begin{equation*}
      a
      \rightarrow
      \sigma(a)
      \rightarrow
      \dotsb
      \rightarrow
      \sigma^k(a) = a.
    \end{equation*}
    Essa permutação cíclica,
    por sua vez,
    pode ser escrita
    \begin{equation*}
      \transposition{a}{\sigma^{k-1}(a)}
      \dotsb
      \transposition{a}{\sigma^2(a)}
      \transposition{a}{\sigma(a)}.
    \end{equation*}
  \end{proof}

  % TODO: exercícios com casos particulares da demonstração.

  Juntos,
  os \cref{lemma:transposition_sign,lemma:permutation_is_product_of_transpositions}
  mostram que
  \begin{equation*}
    \function{\sign}{\permutations{n}}{\set{-1,1}}
  \end{equation*}
  é a \textbf{única} função de $\permutations{n}$ em $\set{-1,1}$
  tal que
  \begin{enumerate}
    \item
      \label{item:transposition_sign}
      $\sign(\sigma) = -1$
      quando $\sigma$ é uma transposição.

    \item
      \label{item:permutation_product}
      $\sign(\gamma\sigma) = \sign(\gamma) \sign(\sigma)$.
  \end{enumerate}
  De fato,
  o \cref{item:transposition_sign}
  determina o valor de $\sign$ em todas as transposições.
  Juntamente com o \cref{item:permutation_product},
  fica determinado o valor de $\sign$
  para todas as permutações que podem ser escritas como produto de transposições.
  Pelo \cref{lemma:permutation_is_product_of_transpositions},
  $\sign$ fica então determinada em todo o seu domínio.
  Fica demonstrado o seguinte corolário do \cref{lemma:permutation_is_product_of_transpositions}.

  \begin{corollary}
    Escolhido $n \in \nonzeronaturals$,
    existe uma única função
    \begin{equation*}
      \function{\sign}{\permutations{n}}{\set{-1,1}}
    \end{equation*}
    tal que
    \begin{enumerate}
      \item
        \label{item:corollary:transposition_sign}
        $\sign(\sigma) = -1$
        quando $\sigma$ é uma transposição.

      \item
        \label{item:corollary:permutation_product}
        $\sign(\gamma\sigma) = \sign(\gamma) \sign(\sigma)$.
    \end{enumerate}
  \end{corollary}

  A existência de $\sign$ tem implicações intrigantes!

  \begin{example}[Identidade é par]
    A permutação \emph{identidade}
    \begin{equation*}
      \functionarray{\identity}{[n]}{[n]}{x}{x}
    \end{equation*}
    é o elemento de $\permutations{n}$ que ``não faz nada''.
    Se
    \begin{equation*}
      \identity
      =
      \sigma_1 \dotsb \sigma_k,
    \end{equation*}
    onde $\sigma_1, \dotsc, \sigma_k$ são transposições,
    então $k$ é necessariamente par.
  \end{example}

  \begin{example}[Jogo do $15$]
    Existe um quebra-cabeça chamado de
    \emph{jogo do $15$}
    ou
    \emph{racha-cuca}
    é formado por 15 quadrados deslizantes dispostos
    dentro de uma caixa de tamanho $4 \times 4$,
    como na \cref{figure:fifteen_tile}.
    Veja o artigo
    \\
    \url{https://pt.wikipedia.org/wiki/O_jogo_do_15}
    \\
    da Wikipedia.

    \nftikzpic[figure:fifteen_tile]{01/00/04/figs/fifteen_tile.tikz}

    O desafio é a partir de um quebra-cabeça embaralhado,
    ordená-lo como mostrado no canto direito inferior da \cref{figure:fifteen_tile}.
    Será que o leitor consegue fazer isso com a configuração mostrada
    na \cref{figure:fifteen_tile:impossible}?

    \nftikzpic[figure:fifteen_tile:impossible]{01/00/04/figs/fifteen_tile_impossible.tikz}

    Associando o quadrado vazio ao número $16$,
    cada movimento do jogo pode ser representado como uma transposição do tipo
    $\transposition{j}{16}$.
    Cada transposição modifica a linha ou a coluna do quadrado $16$,
    somando ou subtraindo $1$.
    Dessa forma,
    uma sequência de transposições que mantenha o quadrado $16$ imóvel
    deve ser necessariamente par,
    pois o número de vezes que o quadrado subir, deve também descer;
    e o número de vezes que for para a direita, deve também ir para a esquerda.
    Assim,
    tal transformação deve ser par.

    Por outro lado,
    para sair do estado inicial
    e chegar ao estado da \cref{figure:fifteen_tile:impossible},
    podemos simplesmente aplicar a transposição $\transposition{14}{15}$.
    Isso significa que
    \textbf{qualquer} transposição que leve o estado inicial
    ao estado da \cref{figure:fifteen_tile:impossible}
    deve ser ímpar.

    Ou seja,
    é impossível levar o estado inicial do quebra-cabeça
    ao estado da \cref{figure:fifteen_tile:impossible}
    ou vice-versa!!! :-)
  \end{example}
