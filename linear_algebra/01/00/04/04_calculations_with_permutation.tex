\subsection{Fazendo Cálculos com Permutações}

  Um caso especial das manipulações algébricas
  dos \cref{example:sum_power:indexes,example:multilinear:polynome},
  é quando estamos tratando de $A^n$,
  onde $n$ é a cardinalidade (número de elementos) de $A$,
  e quando cada termo da soma for igual a zero quando existirem índices repetidos.

  \begin{example}[um pouquinho de matrizes]
    \label{example:nilpotent_condition}
    Para entender este exemplo,
    você precisa saber um pouquinho sobre
    o produto de matrizes
    (\cref{section:matrix_operations}).

    Sejam as matrizes
    \begin{equation*}
      X_1
      =
      \matrixrepresentation{0 & 1 \\ 0 & 0}
      \quad\text{e}\quad
      X_2
      =
      \matrixrepresentation{0 & 0 \\ 1 & 0}.
    \end{equation*}
    Note que
    \begin{equation}
      \label{equation:example:nilpotent_condition}
      X_1 X_1
      =
      X_2 X_2
      =
      \matrixrepresentation{0 & 0 \\ 0 & 0}.
    \end{equation}
    Vamos multiplicar duas matrizes da forma
    \begin{equation*}
      \matrixrepresentation{0 & a_{11} \\ a_{12} & 0}
      =
      a_{11} X_1 + a_{12} X_2
      \quad\text{e}\quad
      \matrixrepresentation{0 & a_{21} \\ a_{22} & 0}
      =
      a_{21} X_1 + a_{22} X_2,
    \end{equation*}
    usando a notação de permutação.
    Assim,
    \begin{align*}
      (a_{11} X_1 &+ a_{12} X_2)
      (a_{21} X_1 + a_{22} X_2)
      \\
      &=
      a_{11} a_{21} X_1 X_1
      +
      a_{11} a_{22} X_1 X_2
      +
      a_{12} a_{21} X_2 X_1
      +
      a_{12} a_{22} X_2 X_2
      \\
      &=
      \sum_{\function{f}{[2]}{[2]}}
      a_{1 f(1)} a_{2 f(2)}
      X_{f(1)} X_{f(2)}
      \\
      &=
      \sum_{f \in \permutations{2}}
      a_{1 f(1)} a_{2 f(2)}
      X_{f(1)} X_{f(2)}.
    \end{align*}
    Essencialmente,
    a \cref{equation:example:nilpotent_condition}
    faz com que possamos ignorar, no somatório,
    as funções $\function{f}{[2]}{[2]}$ que não são injetivas,
    sobrando apenas as permutações.

    Claro que era muito mais fácil simplesmente calcular o produto das matrizes.
    Mas queríamos utilizar a notação de somatório com permutações.
  \end{example}

  Vamos complicar ainda mais o \cref{example:nilpotent_condition}.
  Lembre-se que o objetivo é apenas
  que o leitor adquira familiaridade com a notação de somatório envolvendo permutações.

  \begin{example}
    Suponha que tenhamos uma sequência de símbolos
    $X_1$, $X_2$ e $X_3$,
    que podem ser multiplicados uns com os outros,
    somados
    e
    multiplicados por números reais
    (assim como as matrizes).
    E suponha que
    um produto desses símbolos seja igual a zero
    sempre que o mesmo síbolo aparecer duas vezes no produto.
    Por exemplo,
    \begin{equation*}
      X_1 X_1 = 0
      \quad
      X_1 X_3 X_1 = 0
      \quad
      X_1 X_3 X_3 = 0.
    \end{equation*}
    Sabendo disso,
    \begin{align*}
      (a_{11} X_1 &+ a_{12} X_2 + a_{13} X_3)
      (a_{21} X_1 + a_{22} X_2 + a_{23} X_3)
      (a_{31} X_1 + a_{32} X_2 + a_{33} X_3)
      \\
      &=
      \sum_{\function{f}{[3]}{[3]}}
      a_{1 f(1)} a_{2 f(2)} a_{3 f(3)}
      X_{f(1)} X_{f(2)} X_{f(3)}
      \\
      &=
      \sum_{f \in \permutations{3}}
      a_{1 f(1)} a_{2 f(2)} a_{3 f(3)}
      X_{f(1)} X_{f(2)} X_{f(3)}.
    \end{align*}
    Escrevendo termo a termo,
    sabendo que as permutações de $[3]$ são
    $(1, 2, 3)$,
    $(2, 3, 1)$,
    $(3, 1, 2)$,
    $(3, 2, 1)$,
    $(2, 1, 3)$
    e
    $(1, 3, 2)$,
    o produto resulta em
    \begin{multline*}
      a_{11} a_{22} a_{32}
      X_1 X_2 X_3
      +
      a_{12} a_{23} a_{31}
      X_2 X_3 X_1
      +
      a_{13} a_{21} a_{32}
      X_3 X_1 X_2
      + \\ +
      a_{13} a_{22} a_{31}
      X_3 X_2 X_1
      +
      a_{12} a_{21} a_{33}
      X_2 X_1 X_3
      +
      a_{11} a_{23} a_{32}
      X_1 X_3 X_2.
    \end{multline*}

    Ao invés de utilizar $a_{11}$, $a_{21}$ e etc.,
    poderíamos ter usado diferentes letras.
    A fórmula ficaria mais simples:
    \begin{align*}
      (a_1 X_1 &+ a_2 X_2 + a_3 X_3)
      (b_1 X_1 + b_2 X_2 + b_3 X_3)
      (c_1 X_1 + c_2 X_2 + c_3 X_3)
      \\
      &=
      \sum_{f \in \permutations{2}}
      a_{f(1)} b_{f(2)} c_{f(3)}
      X_{f(1)} X_{f(2)} X_{f(3)}.
    \end{align*}
    No entanto,
    com essa notação,
    fica mais fácil generalizar para
    $X_1$, $X_2$, $\dotsc$, $X_n$:
    \begin{align*}
      (a_{11} X_1 &+ a_{12} X_2 + \dotsb + a_{1n} X_n)
      \dotsb
      (a_{n1} X_1 + a_{n2} X_2 + \dotsb + a_{nn} X_n)
      \\
      &=
      \sum_{f \in \permutations{3}}
      a_{1 f(1)} a_{2 f(2)} \dotsb a_{n f(n)}
      X_{f(1)} X_{f(2)} \dotsb X_{f(n)}.
    \end{align*}
    Lembre-se que estamos utilizando apenas as permutações
    $f \in \permutations{n}$,
    por causa da hipótese de que qualquer produto que contenha dois $X_j$ repetidos
    é igual a zero.
  \end{example}
