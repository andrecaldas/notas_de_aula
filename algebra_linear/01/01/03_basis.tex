\section{Base de Vetores}

  Escolhido um ponto base e um sistema de coordenadas,
  os vetores no plano podem ser representados por pares ordenados.
  Diferentes escolhas levam a diferentes representações.

  \nftikzpic{01/01/figs/two_ortogonal_axes.tikz}


  O sistema de coordenadas não precisa nem mesmo ser formado por retas ortogonais!

  \nftikzpic{01/01/figs/non_ortogonal_axes.tikz}


  Lembre-se de que os vetores no plano não são o mesmo que os pontos do plano.
  E que para identificarmos os dois, fixamos um ponto base.
  Para representarmos os vetores em um sistema de coordenadas,
  também não precisamos nos preocupar com o ponto base do sistema.
  Veja a figura \ref{figure:vector:coordinate_without_base_point}.

  \nftikzpic[figure:vector:coordinate_without_base_point]
    {01/01/figs/vector_far_from_the_origin.tikz}


  A escolha de um sistema de coordenadas sem um ponto base
  corresponde à escolha de dois vetores $\vector{e_1}$ e $\vector{e_2}$,
  de modo que correspondam aos vetores de coordenadas $(1,0)$ e $(0,1)$.
  Por outro lado,
  se escolhermos dois vetores \emph{não paralelos} e \emph{não nulos}
  $\vector{b_1}$ e $\vector{b_2}$, temos um sistema de coordenadas tal que
  $\vector{b_1} = (1,0)$
  e
  $\vector{b_2} = (0,1)$.
  Nesse sistema de coordenadas,
  um vetor $\vector{v}$ tem coordenadas $(\alpha, \beta)$ exatamente quando
  $\vector{v} = \alpha \vector{b_1} + \beta \vector{b_2}$.
  Veja a figura \ref{figure:vector:basis_determines_coordinate_system}.

  \nftikzpic[figure:vector:basis_determines_coordinate_system]
      {01/01/figs/coordinate_system_and_basis.tikz}

  Possivelmente,
  o leitor achou estranho que
  na figura \ref{figure:vector:basis_determines_coordinate_system},
  as coordenadas de $\vector{v}$ são $(2,3)$ e não $(3,2)$.
  Isso nos mostra quão impreciso é o nosso entendimento
  sobre o significado da expressão \emph{``sistema de coordenadas''}.
  Alguns diriam que a primeira coordenada corresponde àquela
  cujo eixo é \emph{``mais horizontal''}  que o outro.
  Outros diriam que é o eixo tal que
  \emph{``girando um ângulo''} menor que $180$ graus no
  \emph{``sentido anti-horário''} pode ser levado no outro eixo.
  Outros talvez diriam que ao traçarmos os eixos de um sistema de coordenadas,
  precisamos especificar qual eixo corresponde à primeira
  e qual eixo corresponde à segunda coordenadas.
  Ou seja,
  precisamos dar nome aos eixos.
  Dizer qual é qual.
  Em outras palavras,
  é preciso \emph{ordenar} os eixos.
  Na figura \ref{figure:vector:basis_determines_coordinate_system},
  o eixo do vetor $\vector{b_1}$ é o primeiro eixo.
  E o eixo do vetor $\vector{b_2}$ é o segundo.

  \nftikzpic{01/01/figs/unordered_axes.tikz}


  Os vetores $\vector{b_1}$ e $\vector{b_2}$ formam uma base do espaço vetorial.
  Mais precisamente,
  o conjunto $\family{B} = \set{\vector{b_1}, \vector{b_2}}$ é uma \emph{base},
  e $\family{R} = (\vector{b_1}, \vector{b_2})$ é uma \emph{base ordenada}.
  Para que possamos escrever $\vector{v}$ na base $\family{B}$,
  não é necessário que a base esteja ordenada.
  Escrevemos
  $\vector{v} = \alpha \vector{b_1} + \beta \vector{b_2}$.
  Ou então,
  $\vector{v} = \beta \vector{b_2} + \alpha \vector{b_1}$.
  Mas se quisermos escrever
  $\vector{v} = (\alpha, \beta)$,
  precisamos que a base esteja ordenada.
  Para sermos mais precisos,
  podemos escrever
  \begin{equation*}
    \vector{v}
    =
    (\alpha, \beta)_{\family{R}},
  \end{equation*}
  indicando qual é a base ordenada que está sendo usada.

  Por fim,
  para concluir,
  repare que um conjunto de dois vetores pode não corresponder a uma base.
  Por exemplo,
  $\family{B} = \set{\vector{0}, \vector{v}}$
  não é uma base para os vetores do plano,
  pois os únicos vetores que podem ser escritos como
  \emph{combinação linear} de $\vector{0}$ e $\vector{v}$
  são os vetores da forma $\alpha \vector{v}$.
  Os vetores $\alpha \vector{v}$ são todos colineares a $\vector{v}$.
  Da mesma forma,
  $\family{B} = \set{\vector{v}, 2\vector{v}}$
  também não é uma base.
  Seria como querer um sistema de coordenadas no plano com dois eixos colineares!

  Se concordarmos em classificar o vetor $\vector{0}$ como colinear a qualquer outro,
  a condição para que $\family{B} = \set{\vector{v}, \vector{w}}$ seja uma base é que
  $\vector{v}$ e $\vector{w}$ não sejam colineares.
  Em um a primeira tentativa de expressar de modo mais preciso o significado de
  ``$\vector{v}$ e $\vector{w}$ não são colineares'',
  poderíamos dizer que não existe $\alpha \in \reals$ tal que
  \begin{equation*}
    \vector{v}
    =
    \alpha \vector{w}.
  \end{equation*}
  Mas espere!
  Os vetores $\vector{v}$ e $\vector{0}$ são colineares,
  mas não existe $\alpha \in \reals$
  $\vector{v} = \alpha \vector{0}$.
  No entanto,
  $\vector{v}$ e $\vector{0}$ são colineares.
  Nossa definição de \emph{``não colinear''} ainda não está boa!
  Poderíamos dizer que
  $\vector{v}$ e $\vector{w}$ não são colineares quando
  $\vector{w}$ é diferente de $\vector{0}$,
  e não existe $\alpha \in \reals$ tal que $\vector{v} = \alpha \vector{w}$.
  Ou então,
  que não existe $\alpha \in \reals$ tal que $\vector{v} = \alpha \vector{w}$,
  e não existe $\alpha \in \reals$ tal que $\vector{w} = \alpha \vector{v}$.
  No entanto,
  uma forma muito usada é dizer que
  \begin{equation*}
    \alpha \vector{v}
    =
    \beta \vector{w}
  \end{equation*}
  só é possível se $\alpha = \beta = 0$.
  Por um raciocínio muito similar,
  \begin{equation*}
    \alpha \vector{v}
    +
    \beta \vector{w}
    =
    \vector{0}
  \end{equation*}
  só é possível se $\alpha = \beta = 0$.

  \begin{definition}[Base de $\reals^2$]
    Dizemos que $\vector{v}$ e $\vector{w}$
    formam uma \emph{base} de $\reals^2$
    quando
    \begin{equation*}
      \alpha \vector{v}
      +
      \beta \vector{w}
      =
      \vector{0}
      \Rightarrow
      \alpha
      =
      \beta
      =
      0.
    \end{equation*}
  \end{definition}
