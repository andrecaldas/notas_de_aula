\subsection{Triângulo e Outros Convexos}

  Sejam
  $\vector{v_1}, \vector{v_2}, \vector{v_3} \in \reals^2$
  distintos.
  Vamos procurar uma maneira de expresar,
  em notação vetorial,
  o triângulo com vértices $\vector{v_1}$, $\vector{v_2}$ e $\vector{v_3}$.
  Vamos tratar do triângulo incluindo seus \emph{``pontos interiores''},
  e vamos denotar por $T$ o conjunto formado pelos pontos do triângulo.
  O conjunto $T$ é \emph{convexo}.
  Um conjunto \emph{convexo} é um conjunto $C$ tal que se
  $\vector{a}, \vector{b} \in C$,
  então todo o segmento de reta que liga os pontos $\vector{a}$ e $\vector{b}$ está contido em $C$.
  Assim como no caso do \emph{ponto médio},
  o leitor pode verificar facilmente que os pontos do segmento de reta
  que liga $\vector{a}$ e $\vector{b}$ são aqueles que podem ser expressos na forma
  \begin{equation}
    \label{equation:convex_combination_of_2}
    \vector{p}
    =
    \alpha \vector{a} + \beta \vector{b},
  \end{equation}
  com $\alpha, \beta \in [0,1]$ e $\alpha + \beta = 1$.
  No caso do ponto médio,
  $\alpha = \beta = \frac{1}{2}$.

  \nftikzpicmissing[figure:non_convexes]{Exemplos de conjuntos que não são convexos.}

  O círculo, o triângulo, o quadrado, etc.\ são exemplos de conjuntos convexos.


  \nftikzpicmissing[figure:convexes]{Exemplos de conjuntos que são convexos.}


  Os pontos $\vector{p}$ da forma da equação \ref{equation:convex_combination_of_2}
  são chamados de combinação convexa dos vetores $\vector{a}$ e $\vector{b}$.
  Um conjunto $C$ é convexo,
  quando é fechado por combinação convexa.
  Resumidamente,
  $C$ é convexo quando
  \begin{equation*}
    \alpha, \beta \in [0,1],\,
    \alpha + \beta = 1,\,
    \vector{v}, \vector{w} \in C
    \Rightarrow
    \alpha \vector{v} + \beta \vector{w}
    \in
    C.
  \end{equation*}

  O triângulo que estamos tratando é o menor convexo
  que contém os vértices $V = \set{\vector{v_1}, \vector{v_2}, \vector{v_3}}$.
  As arestas do triângulo são formadas
  pelos pontos que são combinação convexa de dois vértices.
  Se $A$ é o conjunto das arestas,
  então
  \begin{equation*}
    A
    =
    \setsuchthat{\alpha \vector{v} + \beta \vector{w}}
                {
                  \alpha, \beta \in [0,1],\,
                  \alpha + \beta = 1,\,
                  \vector{v}, \vector{w} \in V
                }.
  \end{equation*}
  Suponha que $\vector{p} = \alpha \vector{v_1} + \beta \vector{v_2} \in A$
  seja um ponto da aresta que liga $\vector{v_1}$ e $\vector{v_2}$.
  Então,
  qualquer combinação convexa de $\vector{p}$ e $\vector{v_3}$ também deve estar em $T$.
  Qual é a forma de uma combinação convexa $\vector{q}$, de $\vector{p}$ e $\vector{v_3}$?
  Por exemplo,
  \begin{align*}
    \vector{q}
    &=
    x \vector{p}
    +
    y \vector{v_3}
    \\
    &=
    x
    (\alpha \vector{v_1} + \beta \vector{v_2})
    +
    y \vector{v_3}
    \\
    &=
    x \alpha \vector{v_1}
    +
    x \beta \vector{v_2}
    +
    y \vector{v_3},
  \end{align*}
  onde $x,y \in [0,1]$ e $x + y = 1$.
  Note que
  $x \alpha, x \beta, y \in [0,1]$
  e
  $x \alpha + x \beta + y = x + y = 1$.
  Dessa forma,
  \begin{equation*}
    T
    =
    \setsuchthat{\alpha \vector{v_1} + \beta \vector{v_2} + \gamma \vector{v_3}}
                {\alpha, \beta, \gamma \in [0,1],\, \alpha + \beta + \gamma = 1}.
  \end{equation*}
  Repare que,
  \o invés de dizermos
  $\alpha, \beta, \gamma \in [0,1]$,
  bastava dizer que $\alpha, \beta, \gamma \geq 0$,
  já que juntamente com a condição $\alpha + \beta + \gamma = 1$,
  não podemos ter $\alpha$, $\beta$ ou $\gamma$ maiores que $1$.

  De um modo geral,
  combinações convexas de combinações convexas de combinações\ldots{} de combinações convexas %TODO: fade...
  de vetores de um conjunto $V$ assumem a seguinte forma
  \begin{equation*}
    \alpha_1 \vector{v_1}
    + \dotsb +
    \alpha_n \vector{v_n},
  \end{equation*}
  onde
  $\vector{v_1}, \dotsc, \vector{v_n} \in V$,
  $\alpha_1, \dotsc, \alpha_n \geq 0$
  e $\sum_{j=1}^n \alpha_j = 1$.
  Esses pontos formam o \emph{envoltório convexo} de $V$.
  Note que conjunto $V$ não precisa nem mesmo ter finitos elementos!

  \nftikzpicmissing[figure:convex_hull]{O \emph{fecho convexo} dos conjuntos da \cref{figure:non_convexes}.}
