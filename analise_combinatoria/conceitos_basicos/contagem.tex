\section{Significado de ``Contar''}

  Contar o número de elementos de um conjunto $A$
  significa encontrar $n \in \naturals$ e uma bijeção
  \begin{equation*}
    \function{f}{\nelements{n}}{A}.
  \end{equation*}
  Dizer que $\abs{A} = n$ é o mesmo que dizer que o conjunto $A$
  pode ser colocado em correspondência biunívoca com o conjunto $\nelements{n}$.
  Ou seja,
  é o mesmo que dizer que existe uma bijeção entre $A$ e $\nelements{n}$.
  E existe uma bijeção entre $A$ e $B$
  se, e somente se,
  $\abs{A} = \abs{B}$.

  Assim,
  para determinarmos a cardinalidade de um conjunto $A$,
  basta encontrar uma bijeção $\function{f}{A}{B}$,
  onde $B$ é um conjunto com cardinalidade conhecida.
  Apedar de dizermos \emph{``basta encontrar''},
  é claro que isso pode não ser uma tarefa fácil.

  \begin{example}[Identificando $A^n$ e $A^{\nelements{n}}$]
    \label{example:identify:nuples_and_nfunctions}
    Considere a função
    \begin{equation*}
      \functionarray{f}{A^{\nelements{n}}}{A^n}{x}{(x(1), \dotsc, x(n))}.
    \end{equation*}
    Então $f$ é injetiva.
    De fato, suponha que
    \begin{align*}
      &\function{x}{\nelements{n}}{A}
      \\
      &\function{y}{\nelements{n}}{A}
    \end{align*}
    são elementos de $A^{\nelements{n}}$ distintos.
    Então existe $k \in \nelements{n}$,
    tal que $x(k) \neq y(k)$.
    E portanto,
    as $k$-ésimas coordenadas de $f(x)$ e $f(y)$,
    que são iguais a $x(k)$ e $y(k)$,
    são distintas.
    Ou seja,
    $f(x) \neq f(y)$.

    A função $f$ também é sobrejetiva,
    pois dado $a = (a_1, \dotsc, a_n) \in A^n$,
    podemos definir
    \begin{equation*}
      \functionarray{x}{\nelements{n}}{A}{j}{a_j}.
    \end{equation*}
    É claro que para $x \in A^{\nelements{n}}$
    definida dessa maneira,
    \begin{equation*}
      f(x)
      =
      (x(1), \dotsc, x(n))
      =
      (a_1, \dotsc, a_n).
    \end{equation*}
    Portanto,
    $f$ é uma bijeção.

    Note que além de
    $A^n$ e $A^{\nelements{n}}$
    terem a mesma cardinalidade,
    a identificação $f$ é
    \emph{natural} (ou \emph{canônica}).
    E é por isso que em muitos momentos
    trataremos os conjuntos
    $A^n$ e $A^{\nelements{n}}$
    como se fossem a mesma coisa.
  \end{example}


  \begin{example}[Identificando $A^n$ e $A^B$]
    \label{example:identify:nuples_and_functions}
    Suponha que $A$ e $B$ sejam conjuntos,
    e que $\abs{B} = n$.
    Nesse caso,
    existe uma bijeção
    \begin{equation*}
      \function{g}{\nelements{n}}{B}.
    \end{equation*}
    Então,
    assim como no Exemplo \ref{example:identify:nuples_and_nfunctions},
    podemos construir a bijeção
    \begin{equation*}
      \functionarray{f}{A^B}{A^n}{x}{\left(x(g(1)), \dotsc, x(g(n))\right)}.
    \end{equation*}
    Note,
    no entanto,
    que $f$ não é \emph{natural},
    no sentido de que a construção de $f$
    depende da escolha (arbitrária) de $g$.

    Assim,
    quando $B$ é finito,
    $\abs{A^B} = \abs{A^{\abs{B}}}$.
    E de modo análogo,
    mesmo que $B$ não seja finito,
    \begin{equation*}
      \abs{B} = \abs{C}
      \Rightarrow
      \abs{A^B} = \abs{A^C}.
    \end{equation*}
  \end{example}


  \begin{example}[Identificando $A^{\nelements{n}}$ e $A^B$]
    Assim como
    no Exemplo \ref{example:identify:nuples_and_functions},
    seja
    \begin{equation*}
      \function{g}{\nelements{n}}{B}
    \end{equation*}
    uma bijeção.
    Então,
    \begin{equation*}
      \functionarray{f}{A^B}{A^{\nelements{n}}}{x}{x \circ f}
    \end{equation*}
    é uma bijeção.
  \end{example}
% TODO: exercício -- demonstrar o exemplo.


  O exemplo a seguir mostra que
  a cardinalidade de $\parts{A}$
  é igual à cardinalidade de $\set{0,1}^A$.
  Já o conjunto $\set{0,1}^A$ tem,
  pelo Exemplo \ref{example:identify:nuples_and_functions},
  $\set{0,1}^A$
  tem a mesma cardinalidade que
  $\set{0,1}^{\abs{A}}$.
  Na Seção \ref{section:principio_multiplicativo},
  mostraremos que $\set{0,1}^{\abs{A}}$
  tem cardinalidade $2^{\abs{A}}$.

  \begin{example}[Cardinalidade de $\parts{A}$]
    Vamos construir uma bijeção entre
    $\parts{A}$ e $\set{0,1}^A$.
    Lembre-se que os elementos de $\set{0,1}^A$ são
    as funções
    \begin{equation*}
      \function{I}{A}{\set{0,1}}.
    \end{equation*}
    Por outro lado,
    para cada $B \subset A$
    ---
    ou melhor, para cada $B \in \parts{A}$
    ---,
    temos a função
    \begin{equation*}
      \functionarray{I_B}{A}{\set{0,1}}
                    {x}
                    {
                      I_B(x)
                      =
                      \begin{cases}
                        0, & x \not \in B
                        \\
                        1, & x \in B
                      \end{cases}
                    }.
    \end{equation*}
    Note que a correspondência
    \begin{equation*}
      \functionarray{f}{\parts{A}}{\set{0,1}^A}{B}{I_B}
    \end{equation*}
    é uma bijeção.
    De fato,
    se $B \neq D$,
    então $I_B \neq I_D$ (correspondência injetiva).
    E além disso,
    para qualquer $\function{I}{A}{\set{0,1}}$,
    se fizermos $B = I^{-1}(1)$,
    temos que $I = I_B$ (correspondência sobrejetiva).

    Concluímos que
    \begin{equation*}
      \abs{\parts{A}}
      =
      \abs{\set{0,1}}^{\abs{A}}
      =
      2^{\abs{A}},
    \end{equation*}
    onde a última igualdade é mostrada
    na Seção \ref{section:principio_multiplicativo}.
  \end{example}

  \begin{notation}
    Por causa do exemplo anterior,
    a família $\parts{A}$ também é denotada por $2^A$.
  \end{notation}



  Costumamos identificar,
  por exemplo,
  os conjuntos
  \begin{equation*}
    A \times (B \times C),
    \quad
    (A \times B) \times C
    \quad\text{e}\quad
    A \times B \times C.
  \end{equation*}
  a identificação leva,
  por exemplo,
  $(a, (b, c))$ em $(a, b, c)$.
  Esta identificação é \emph{natural},
  no sentido de que não depende de escolhas arbitrárias.
  Vejamos uma generalização desse procedimento.

  \begin{example}
    \label{example:identificar_axbxc:infinito}
    Seja $\Gamma$ um conjunto de índices e
    $A_\gamma$ ($\gamma \in \Gamma$) uma família de conjuntos.
    Suponha que
    \begin{equation*}
      \Gamma
      =
      \biguplus_{\lambda \in \Lambda} \Gamma_\lambda.
    \end{equation*}
    Então,
    identificamos os conjuntos
    \begin{equation*}
      \prod_{\gamma \in \Gamma}
      A_\gamma
      \quad\text{e}\quad
      \prod_{\lambda \in \Lambda}
      \left(
        \prod_{\gamma \in \Gamma_\lambda}
        A_\gamma
      \right).
    \end{equation*}
    De modo mais preciso,
    considere
    \begin{equation*}
      \functionarray{f}
                    {\prod_{\lambda \in \Lambda} \left(\prod_{\gamma \in \Gamma_\lambda} A_\gamma \right)}
                    {\prod_{\gamma \in \Gamma} A_\gamma}
                    {(x_\lambda)_{\lambda \in \Lambda}}{\bigvee_{\lambda \in \Lambda} x_\lambda}.
    \end{equation*}
    É fácil ver que $f$ é bijetiva,
    e que sua inversa é a função $g$ que leva $x$ em suas
    restrições $x|_{\Gamma_\lambda}$,
    com o contradomínio alterado para
    $\prod_{\gamma \in \Gamma_\lambda} A_\gamma$.
    Ou seja,
    \begin{equation*}
      \functionarray{g}
                    {\prod_{\gamma \in \Gamma} A_\gamma}
                    {\prod_{\lambda \in \Lambda} \left(\prod_{\gamma \in \Gamma_\lambda} A_\gamma \right)}
                    {x}{(g_\lambda(x))_{\lambda \in \Lambda}},
    \end{equation*}
    onde
    \begin{equation*}
      \functionarray{g_\lambda(x)}{\Gamma_\lambda}{\prod_{\gamma \in \Gamma_\lambda} A_\gamma}{\gamma}{x(\gamma)}
    \end{equation*}
    é praticamente igual a $x|_{\Gamma_\lambda}$,
    com a diferença do contradomínio,
    que não é o mesmo que o contradomínio de $x$.
  \end{example}
