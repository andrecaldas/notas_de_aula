\subsection{Funções}

  Uma função $f$ de $A$ em $B$,
  denotada
  \begin{equation*}
    \function{f}{A}{B},
  \end{equation*}
  é um objeto que associa a cada elemento $a \in A$,
  um determinado elemento de $B$.
  Denotamos este elemento por $f(a)$,
  dizemos que $f$ leva $a$ em $f(a)$,
  e escrevemos
  \begin{equation*}
    \functionarray{f}{A}{B}{a}{f(a)}.
  \end{equation*}
  Dizemos que $A$ é o domínio de $f$
  e $B$ o contradomínio.
  Quando $C \subset A$,
  denotamos por
  \begin{equation*}
    f(C)
    =
    \setsuchthat{b \in B}{\exists a \in C,\, b = f(a)}
  \end{equation*}
  o subconjunto de $B$ formado pela ``imagem'' dos elementos de $c$.
  Dizemos que $f(A)$ é a \emph{imagem} de $f$.
  Nem todos os elementos $b \in B$ necessitam ser tais
  que exista um $a \in A$ satisfazendo $f(a) = b$.
  Ou seja,
  pode ser que $f(A) \subsetneq B$.
  Quando $f(A) = B$,
  dizemos que $f$ é \emph{sobrejetiva},
  ou que $f$ é uma \emph{sobrejeção}.

  Da mesma forma,
  pode ser que $f(x) = f(y)$,
  mesmo que $x \neq y$.
  Para $x, y \in A$,
  quando
  \begin{equation*}
    x \neq y
    \Rightarrow
    f(x) \neq f(y),
  \end{equation*}
  dizemos que $f$ é \emph{injetiva},
  ou que $f$ é uma \emph{injeção}.
  Quando $f$ é injetiva e sobrejetiva,
  dizemos que $f$ é \emph{bijetiva},
  ou que $f$ é uma \emph{bijeção}.
  Quando $f$ é uma bijeção,
  a \emph{inversa} de
  $\function{f}{A}{B}$
  é a função
  $\function{g}{B}{A}$
  tal que
  \begin{equation*}
    g(b) = a
    \Leftrightarrow
    f(a) = b.
  \end{equation*}

  \begin{example}
    \label{example:naturals_to_even}
    A função
    \begin{equation*}
      \functionarray{f}{\naturals}{\naturals}{n}{2n}
    \end{equation*}
    associa a cada número natural um número par.
    Essa função é injetiva,
    mas não é sobrejetiva.
    Se $P = \set{0, 2, 4, \dotsc}$
    é o conjunto dos números naturais pares,
    então
    \begin{equation*}
      \functionarray{g}{\naturals}{P}{n}{2n}
    \end{equation*}
    é uma bijeção.
  \end{example}

  Para dois conjuntos $A$ e $B$ finitos,
  se $\function{f}{A}{B}$ é injetiva,
  então $\abs{A} \leq \abs{B}$.
  E se é sobrejetiva,
  então $\abs{A} \geq \abs{B}$.
  Esse fato nos motiva a estabelecer uma forma
  de tratar a \emph{cardinalidade} de dois conjuntos,
  mesmo que não sejam finitos.
  Dizemos que $\abs{A} = \abs{B}$ quando
  existir uma bijeção
  $\function{f}{A}{B}$.

% TODO: exercício
% Quando A e B são finitos,
% f é injetiva <=> bijetiva <=> sobrejetiva

  Dado $b \in B$,
  denotamos por
  \begin{equation*}
    f^{-1}(b)
    =
    \setsuchthat{a \in A}{f(a) = b}
  \end{equation*}
  o subconjunto de todos os elementos de $A$
  que são levados em $b$ por $f$.
  De modo mais geral,
  se $D \subset B$,
  denotamos por
  \begin{equation*}
    f^{-1}(D)
    =
    \setsuchthat{a \in A}{f(a) \in D}
  \end{equation*}
  o subconjunto de todos os elementos de $A$
  que são levados por $f$ em algum elemento de $D$.
  Chamamos $f^{-1}(D)$ de
  \emph{imagem inversa} de $D$ por $f$.

  Se $\function{f}{A}{B}$
  e $\function{g}{B}{C}$
  são funções, então podemos definir a função
  \begin{equation*}
    \functionarray{g \circ f}{A}{C}{x}{g(f(x))}.
  \end{equation*}
  Chamamos $g \circ f$ de
  \emph{composição} de $g$ com $f$.
  E quando $D \subset A$ e
  $\function{f}{A}{B}$,
  definimos a \emph{restrição} de $f$ a $D$ como
  \begin{equation*}
    \functionarray{f|_D}{D}{B}{x}{f(x)}.
  \end{equation*}

  Por vezes,
  inspirados pela ideia de $n$-uplas ordenadas,
  ao invés de escrevermos
  $\function{f}{A}{B}$,
  podemos escrever
  $(x_a)_{a \in A}$,
  onde $x_a = f(a)$ para todo $a \in A$.

  Por fim,
  se
  \begin{equation*}
    \function{f_\gamma}{A_\gamma}{B_\gamma}
  \end{equation*}
  é uma família de funções indexadas por $\gamma \in \Gamma$,
  então, fazendo
  \begin{align*}
    A
    &=
    \biguplus_{\gamma \in \Gamma} A_\gamma
    \\
    B
    &=
    \biguplus_{\gamma \in \Gamma} B_\gamma,
  \end{align*}
  definimos
  \begin{equation*}
    \functionarray{\bigvee_{\gamma \in \Gamma} f_\gamma}{A}{B}{a}{f_{\gamma(a)}(a)},
  \end{equation*}
  onde $\gamma(a) \in \Gamma$ é o único elemento de $\Gamma$
  tal que $a \in A_{\gamma(a)}$.
