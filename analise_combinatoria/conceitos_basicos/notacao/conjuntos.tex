\subsection{Conjuntos}

  Denotamos o número de elementos de um conjunto $A$
  por $\abs{A}$.
  Se $A$ e $B$ são conjuntos,
  $A \cup B$,
  $A \cap B$,
  $A \times B$
  e
  $A \setminus B$
  são a união,
  interseção
  produto
  e
  diferença entre $A$ e $B$.
  \begin{align*}
    x \in A \cup B
    &\Leftrightarrow
    x \in A \text{ ou } x \in B
    \\
    x \in A \cap B
    &\Leftrightarrow
    x \in A \text{ e } x \in B
    \\
    (a,b) \in A \times B
    &\Leftrightarrow
    a \in A \text{ e } b \in B
    \\
    A \setminus B
    &=
    \setsuchthat{x \in A}{x \not \in B}.
  \end{align*}
  Quando $A \cap B = \emptyset$,
  dizemos que $A$ e $B$ são disjuntos.
  Utilizamos a notação
  \begin{equation*}
    C
    =
    A \uplus B
  \end{equation*}
  para denotar dois fatos:
  $C = A \cup B$
  e
  $A \cap B = \emptyset$.
  Ou seja,
  $C$ é a \emph{união disjunta} de $A$ e $B$.

  É fácil imaginar união,
  interseção e produto de mais conjuntos.
  Assim, se $A_1, \dotsc, A_n$ são conjuntos,
  \begin{align*}
    x \in A_1 \cup \dotsb \cup A_n
    &\Leftrightarrow
    \exists j \in \nelements{n},\,
    x \in A_j
    \\
    x \in A_1 \cap \dotsb \cap A_n
    &\Leftrightarrow
    \forall j \in \nelements{n},\,
    x \in A_j
    \\
    (a_1, \dotsc, a_n) \in A_1 \times \dotsb \times A_n
    &\Leftrightarrow
    \forall j \in \nelements{n},\,
    a_j \in A_j.
  \end{align*}
  Dizemos que $A_1, \dotsc, A_n$ são
  (dois a dois) disjuntos quando
  \begin{equation*}
    j,k \in \nelements{n},\,
    j \neq k
    \Rightarrow
    A_j \cap A_k = \emptyset.
  \end{equation*}
  E usamos a notação
  $C = A_1 \uplus \dotsb \uplus A_n$
  para dizer que
  $C = A_1 \cup \dotsb \cup A_n$
  e que $A_1, \dotsc, A_n$ são disjuntos.
  Ou seja,
  $C$ é a união disjunta dos conjuntos
  $A_1, \dotsc, A_n$.

  Quando $A_1 = A_2 = \dotsb = A_n = A$,
  escrevemos $A^n$ no lugar de
  $A_1 \times \dotsb \times A_n$.
  Note que um elemento
  $a = (a_1, \dotsc, a_n) \in A^n$
  pode ser visto como uma função
  \begin{equation*}
    \functionarray{a}{\nelements{n}}{A}{j}{a_j}.
  \end{equation*}
  Assim,
  generalizando,
  escrevemos $A^B$ para denotar o conjunto
  das funções $\function{f}{B}{A}$.
  Neste caso,
  quando dizemos que $A^n$ pode ser visto como
  o conjunto das funções de $\nelements{n}$ em $A$,
  estamos identificando os elementos
  $f \in A^{\nelements{n}}$
  com os elementos
  $(f(1), \dotsc, f(n)) \in A^n$.

  Inspirados pela identificação de $A^n$ e $A^{\nelements{n}}$,
  se $\Gamma$ é um conjunto de índices e
  $A_\gamma$ ($\Gamma \in \gamma$) uma família de conjuntos,
  além da união e interseção desses conjuntos,
  podemos definir também o produto.
  \begin{align*}
    x
    \in
    \bigcup_{\gamma \in \Gamma} A_\gamma
    &\Leftrightarrow
    \exists \gamma \in \Gamma,\,
    x
    \in
    A_\gamma
    \\
    x
    \in
    \bigcap_{\gamma \in \Gamma} A_\gamma
    &\Leftrightarrow
    \forall \gamma \in \Gamma,\,
    x
    \in
    A_\gamma
    \\
    \prod_{\gamma \in \Gamma} A_\gamma
    &=
    \setsuchthat{\function{x}{\Gamma}{\bigcup_{\gamma \in \Gamma} A_\gamma}}{x(\gamma) \in A_\gamma}.
  \end{align*}
  Normalmente denotamos $x(\gamma)$ por $x_\gamma$.

  Novamente,
  dados $A_\gamma$ ($\gamma \in \Gamma$),
  escrevemos
  \begin{equation*}
    C
    =
    \biguplus_{\gamma \in \Gamma} A_\gamma
  \end{equation*}
  significa que $C$ é a união de todos os $A_\gamma$
  e que os $A_\gamma$ são dois a dois disjuntos.
  Também dizemos que $A_\gamma$ ($\gamma \in \Gamma$)
  particiona $C$,
  ou então, que $A_\gamma$ ($\gamma \in \Gamma$)
  é uma partição de $C$.

  Por fim,
  $\parts{A}$ é a família (o conjunto)
  de todos os subconjuntos de $A$.

  \begin{example}
    Seja $A = \set{1,2,3}$.
    Então,
    \begin{equation*}
      \parts{A}
      =
      \set
      {
        \begin{array}{lll}
          \emptyset, &A, &
          \\
          \set{1},   &\set{2},   &\set{3},
          \\
          \set{2,3}, &\set{1,3}, &\set{1,2}
        \end{array}
      }.
    \end{equation*}
  \end{example}
