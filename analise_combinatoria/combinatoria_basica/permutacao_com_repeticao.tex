\section{Permutação com Repetição}

  Como aplicação da decomposição de $A$
  em subconjuntos disjuntos $f^{-1}(b)$ ($b \in B$),
  onde $\function{f}{A}{B}$ é uma função qualquer,
  vamos estudar as \emph{permutações com repetição}.

  \begin{quote}
    Quantos anagramas tem a palavra
    \emph{arara}?
  \end{quote}

  Por que esse problema é mais difícil do que saber o
  número de permutações de \emph{ARar$\alpha$}?
  E se fosse
  \begin{tikzpicture}
    [
      baseline=(first.base),
      every node/.style={rectangle,draw}
    ]
    \node (first) {$A_1$};
    \node [right=1mm of first] (second) {$R_1$};
    \node [right=1mm of second] (third) {$A_2$};
    \node [right=1mm of third] (fourth) {$R_2$};
    \node [right=1mm of fourth] {$A_3$};
  \end{tikzpicture}?
  Neste caso,
  saberíamos que o número de permutações é $5!$.
  Seja então,
  \begin{equation*}
    A
    =
    \set{A_1, R_1, A_2, R_2, A_3}.
  \end{equation*}
  Vamos calcular o número de anagramas de
  \begin{tikzpicture}
    [
      baseline=(first.base),
      every node/.style={rectangle,draw}
    ]
    \node (first) {$A$};
    \node [right=1mm of first] (second) {$R_1$};
    \node [right=1mm of second] (third) {$A$};
    \node [right=1mm of third] (fourth) {$R_2$};
    \node [right=1mm of fourth] {$A$};
  \end{tikzpicture}.
  Para isso,
  seja $B$ o conjunto desses anagramas.
  Considere a função
  $\function{f}{A}{B}$ que ``remove'' os índices das letras A.
  Que leva,
  por exemplo,
  $A_1 A_3 R_2 R_1 A_2$
  em
  $A A R_2 R_1 A$.
  Neste caso,
  por exemplo,
  \begin{equation*}
    f^{-1}
    \left(
      A A R_2 R_1 A
    \right)
    =
    \set{
      \begin{array}{ll}
        A_1 A_2 \textcolor{gray}{R_2 R_1} A_3,
        &
        A_1 A_3 \textcolor{gray}{R_2 R_1} A_2
        \\
        A_2 A_1 \textcolor{gray}{R_2 R_1} A_3,
        &
        A_2 A_3 \textcolor{gray}{R_2 R_1} A_1
        \\
        A_3 A_1 \textcolor{gray}{R_2 R_1} A_2,
        &
        A_3 A_2 \textcolor{gray}{R_2 R_1} A_1
      \end{array}
    }.
  \end{equation*}
  É fácil ver que para cada $b \in B$,
  $\abs{f^{-1}(b)} = 3! = 6$.
  De fato,
  $f^{-1}(b)$ está em bijeção com $P_3$.
  Chamando de $C$ o conjunto dos anagramas de ARARA,
  podemos agora pensar na função
  $\function{g}{B}{C}$
  que remove os índices das letras R.
  Para cada anagrama $c$ de ARARA,
  temos que $\abs{g^{-1}(c)} = 2$,
  pois existem duas maneiras distintas de se colocar
  ínidices nas letras R.
  Sendo assim,
  \begin{equation*}
    \abs{P_A}
    =
    3!
    \abs{B}
    =
    3!
    2!
    \abs{C}.
  \end{equation*}
  Portanto,
  o número de anagramas de ARARA é
  \begin{equation*}
    \abs{C}
    =
    \frac{\abs{P_A}}{3!2!}
    =
    \frac{5!}{3!2!}
    =
    10.
  \end{equation*}

  É evidente que poderíamos ter considerado
  a função $g \circ f$ que remove todos os índices
  dos anagramas de $A_1 R_1 A_2 R_2 A_3$,
  transformando-os em anagramas de ARARA.
  Nesse caso,
  teríamos que
  $\abs{(g \circ f)^{-1}(c)} = 3!2! = 12$.

  \begin{exercise}
    Para cada anagrama $c$ de ARARA,
    construa uma bijeção entre
    $(g \circ f)^{-1}(c)$
    e
    $P_3 \times P_2$.
  \end{exercise}

  É muito comum,
  em análise combinatória,
  nos referirmos a multiconjuntos.
  A diferença entre conjunto e multiconjunto
  é que se estivermos tratando de conjuntos,
  então,
  \begin{equation*}
    A = \set{1,2,3}
    \quad\text{e}\quad
    B = \set{1,1,2,1,2,3}
  \end{equation*}
  são iguais.
  Em se trantando de conjuntos,
  nos interessa saber apenas se determinado elemento
  pertence ou não ao conjunto.
  Já um multiconjunto é algo similar,
  mas para cada elemento,
  associamos também o número de vezes que tal elemento
  ``aparece'' no multiconjunto.
  Assim,
  como multiconjuntos, $A$ e $B$ são distintos.
  E o multiconjunto $B$ é igual a $\set{1,1,1,2,2,3}$.

  É comum tratar de \emph{``permutações com repetição''}
  da mesma forma que tratamos a permutação,
  só que usando a linguagem de multiconjuntos.
  Por exemplo,
  se $A = \set{A,A,A,R,R}$,
  Podemos nos perguntar
  o número de permutações do multiconjunto $A$.
  Ou seja,
  o número de anagramas da palavra ARARA.

  Nesse curso,
  não utilizaremos a linguagem de multiconjuntos.
  Ao invés disso,
  utilizaremos as funções que ``removem'' os índices,
  como em $A_1 R_1 A_2 R_2 A_3$.
  O multiconjunto
  $\set{A,A,A,R,R}$
  nada mais é do que o conjunto
  $\set{A_1 R_1 A_2 R_2 A_3}$
  com os índices omitidos.
  Dessa forma,
  ao tratarmos de um multiconjunto $M'$,
  estamos de fato tratando de um conjunto
  $M \subset A \times \naturals$,
  mas ignorando em algum momento,
  a segunda coordenada.

  \begin{example}
    Ainda estudando os anagramas da palavra ARARA,
    considere o conjunto $A = \set{A, R}$.
    Então,
    ao invés do multiconjunto
    $M' = \set{A, A, A, R, R}$,
    trataremos do conjunto
    $M = \set{(A,1),(A,2), (A,3), (R,1), (R,2)}$,
    associado à aplicação
    \begin{equation*}
      \functionarray{\pi}{M}{A}{(a,n)}{a}.
    \end{equation*}
    A aplicação $\pi$ induz
    \begin{equation*}
      \functionarray{\tilde{\pi}}{P_M}{A^M}{x}{\pi \circ x}.
    \end{equation*}
    Os anagramas de ARARA correspondem à imagem de $\tilde{\pi}$.
    Repare que $\tilde{\pi}$
    nada mais é do que a função $g \circ f$ do argumento
    apresentado anteriormente.
  \end{example}

  Se $A$ é um conjunto qualquer
  e $M \subset A \times \naturals$,
  o argumento refeito
  utilizando-se a função
  \begin{equation*}
    \functionarray{\pi}{M}{A}{(a,n)}{a},
  \end{equation*}
  que deve ser entendida como uma função que remove
  os índices que acompanham as letras.

  \begin{definition}[Permutação com repetição]
    Seja $A$ um conjunto e
    $M$ um subconjunto finito de $A \times \naturals$.
    As \emph{permutações com repetição} de $M$,
    são os elementos do conjunto
    \begin{equation*}
      PR_M
      =
      \setsuchthat{\pi \circ p}{p \in P_M},
    \end{equation*}
    onde $\function{\pi}{M \subset A \times \naturals}{A}$
    é a projeção na primeira coordenada.
  \end{definition}

  Seja
  \begin{equation*}
    \functionarray{\tilde{\pi}}{P_M}{PR_M}{p}{\pi \circ p}.
  \end{equation*}
  Então,
  para cada $x \in PR_M$,
  qual é a cardinalidade de $\Pi^{-1}(x)$?
  É exatamente a cardinalidade de
  \begin{equation*}
    \prod_{a \in A} P_{\pi^{-1}(a)}.
  \end{equation*}
  Portanto,
  \begin{equation*}
    \abs{PR_M}
    =
    \frac{\abs{P_M}}{\prod_{a \in A} \abs{P_{\pi^{-1}(a)}}}.
  \end{equation*}
