\section{Núcleo da Transformação}

  O \emph{núcleo} de uma transformação linear
  é o conjunto de todos os vetores que são levados por $T$ no vetor nulo.

  \begin{definition}[Núcleo]
    \label{definition:kernel}
    O \emph{núcleo} ou \emph{kernel} 
    da transformação linear
    $\function{T}{V}{W}$ é o conjunto
    \begin{equation*}
      \kernel{T}
      =
      \setsuchthat{\vector{v} \in V}{T\vector{v} = \vector{0}}.
    \end{equation*}
  \end{definition}

  É fácil ver que o $\kernel{T}$ é um subsepaço de $V$.
  De fato,
  $\kernel{T} \neq \emptyset$,
  pois $T\vector{0} = \vector{0}$.
  E se $\vector{a}, \vector{b} \in \kernel{T}$,
  então
  $\alpha \vector{a} + \beta \vector{b} \in \kernel{T}$,
  pois
  \begin{align*}
    T(\alpha \vector{a} + \beta \vector{b})
    &=
    \alpha T\vector{a} + \beta T\vector{b}
    \\
    &=
    \alpha \vector{0} + \beta \vector{0}
    \\
    &=
    \vector{0}.
  \end{align*}

  \begin{notation}[Imagem inversa]
    \label{notation:pre_image}
    É comum usarmos a notação
    \begin{equation*}
      T^{-1}(\vector{a})
      =
      \setsuchthat{\vector{v} \in V}{T\vector{v} = \vector{a}}.
    \end{equation*}
    Nesse caso,
    $\kernel{T} = T^{-1}(\vector{0})$.
  \end{notation}

  Já vimos que resolver um sistema de equações lineares
  com $m$ linhas e $n$ incógnitas,
  corresponde a descobrir todos os vetores
  $\vector{v} \in \reals^n$
  tais que
  $T\vector{v} = \vector{w}$,
  onde $T$ e $\vector{w} \in \reals^m$ são dados.
  Com a \cref{notation:pre_image},
  resolver um tal sistema linear corresponde a determinar o conjunto
  $T^{-1}(\vector{w})$.
  E o núcleo $\kernel{T}$ nada mais é do que a solução do sistema homogêneo.


  \subsection{Injetividade}

    Se a transformação linear
    $\function{T}{V}{W}$
    é injetiva,
    então é evidente que
    $\kernel{T} = \set{\vector{0}}$,
    pois só pode haver um vetor de $V$ levado em
    $\vector{0} \in W$.

    Por outro lado,
    suponha que $T$ não seja injetiva.
    Ou seja,
    suponha que existam vetores distintos
    $\vector{a}, \vector{b} \in V$
    tais que
    $T\vector{a} = T\vector{b}$.
    Então,
    \begin{align*}
      T(\vector{a} - \vector{b})
      &=
      T\vector{a} - T\vector{b}
      \\
      &=
      \vector{0}.
    \end{align*}
    Mas como $\vector{a} \neq \vector{b}$,
    temos que
    \begin{equation*}
      \vector{0}
      \neq
      \vector{a} - \vector{b}
      \in
      \kernel{T}.
    \end{equation*}
    Ou seja,
    $T$ é injetiva exatamente quando $\kernel{T} = \set{\vector{0}}$.
    Resumindo{\ldots}

    \begin{proposition}
      Uma transformação linear
      $\function{T}{V}{W}$
      é injetiva se, e somente se,
      $\kernel{T} = \set{\vector{0}}$.
    \end{proposition}
