\section{Definição}

  Do ponto de vista da matemática,
  a definição de \emph{transformação linear} não pode ser dada
  com um exemplo e uma frase da forma:
  \begin{quote}
    As transformações ``desse tipo''
    são chamadas de \emph{transformações lineares}.
  \end{quote}
  Precisamos de uma definição mais precisa.
  Vamos definir o que são as \emph{transformações lineares}
  de $\reals^2$ em $\reals^2$.
  A definição que apresentaremos servirá também para aplicações
  entre espaços vetoriais mais gerais.
  Veja a \cref{definition:linear_transform}.

  \begin{definition}[Transformação Linear em $\reals^2$]
    \label{definition:linear_transform:r2}
    Uma aplicação $\function{T}{\reals^2}{\reals^2}$
    é chamada de \emph{transformação linear} quando
    para todo $\alpha \in \reals$ e
    quaisquer $\vector{p}, \vector{q} \in \reals^2$,
    \begin{enumerate}
      \item
        $T(\alpha \vector{p}) = \alpha T\vector{p}$; e

      \item
        $T(\vector{p} + \vector{q}) = T\vector{p} + T\vector{q}$.
    \end{enumerate}
  \end{definition}

  O leitor é convidado a demonstrar que o conceito de
  \emph{transformação linear} apresentado na
  Seção~\ref{section:linear_transforms:r2:motivation}
  é de fato uma transformação linear de acordo com o critério
  da Definição~\ref{definition:linear_transform:r2}.
  Assim,
  a rotação do exemplo~\ref{example:agle_sum}
  e a transformação do exemplo~\ref{example:kite_and_house:tablet}
  são exemplos de transformações lineares.
  Algumas transformações são um pouco mais patológicas.


  \begin{example}[Transformação nula]
    A transformação
    \begin{equation*}
      \functionarray{T}{\reals^2}{\reals^2}{v}{\vector{0}}
    \end{equation*}
    é uma \emph{transformação linear}.
    De fato,
    se $\vector{p}, \vector{q} \in \reals^2$ e $\alpha \in \reals$,
    então
    \begin{align*}
      T(\alpha \vector{p})
      &=
      \vector{0}
      =
      \alpha \vector{0}
      =
      \alpha T\vector{p}
      \\
      T(\vector{p} + \vector{q})
      &=
      \vector{0}
      =
      \vector{0} + \vector{0}
      =
      T\vector{p} + T\vector{q}.
    \end{align*}
    Essa transformação linear é um caso bastante degenerado
    em comparação ao que foi discutido
    na seção~\ref{section:linear_transforms:r2:motivation}.
  \end{example}


  \begin{example}
    A transformação
    \begin{equation*}
      \functionarray{T}{\reals^2}{\reals^2}{(a,b)}{(a,0)}
    \end{equation*}
    é uma transformação linear.
    É a \emph{``projeção ortogonal''} de $\vector{v} = (a,b)$
    no eixo das abscissas.

    \nftikzpic{01/03/figs/first_coordinate.tikz}

    Essa transformação também não se enxaixa muito bem
    no que foi discutido na seção~\ref{section:linear_transforms:r2:motivation},
    pois todos os pontos são levados em um ponto no eixo das abscissas.
    É um caso degenerado,
    onde o \emph{``reticulado de destino''} seria um \emph{``retângulo''} de altura $0$.
  \end{example}


  Quando a \emph{transformação linear}
  $\function{T}{\reals^2}{\reals^2}$
  é uma bijeção,
  sua inversa $T^{-1}$ também é uma \emph{transformação linear}.
  De fato,
  como $T$ é \emph{sobrejetiva},
  para quaisquer $x, y \in \reals^2$,
  existem $v, w \in \reals^2$ tais que
  $x = Tv$
  e
  $y = Tw$.
  Assim,
  \begin{align*}
    T^{-1}(x + y)
    &=
    T^{-1}(Tv + Tw)
    \\
    &=
    T^{-1}(T(v + w))
    \\
    &=
    v + w
    \\
    &=
    T^{-1}(x) + T^{-1}(y).
  \end{align*}
  E para qualquer $\alpha \in \reals$,
  \begin{align*}
    T^{-1}(\alpha x)
    &=
    T^{-1}(\alpha Tv)
    \\
    &=
    T^{-1}(T(\alpha v))
    \\
    &=
    \alpha v
    \\
    &=
    \alpha T^{-1}(x).
  \end{align*}

  Para aquele leitor que se confunde com o símbolo $T^{-1}$,
  sugerimos usar uma outra letra,
  $S$, por exemplo,
  para designar a função $T^{-1}$.
  E lembrar que
  \begin{align*}
    Sx &= v
    \\
    Sy &= w
    \\
    S(Tz) &= T(Sz) = z \quad \text{para todo $z$}.
  \end{align*}
  Por exemplo,
  se queremos mostrar que $S(x + y) = Sx + Sy$,
  \begin{align*}
    S(x + y)
    &=
    S(Tv + Tw)
    \\
    &=
    S(T(v + w))
    \\
    &=
    v + w
    \\
    &=
    S(x) + S(y).
  \end{align*}
