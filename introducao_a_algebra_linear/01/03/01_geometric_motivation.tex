\section{Motivação Geométrica}
  \label{section:linear_transforms:r2:motivation}

  A figura \ref{figure:linear_transform:house}
  ilustra uma maneira de transformarmos um desenho do plano.
  A ideia é quadricular o desenho original
  e fazer uma correspondência entre esse quadriculado
  e o reticulado de destino,
  formado de pequenos paralelogramos.

  \nftikzpic[figure:linear_transform:house]
    {01/03/figs/house_and_kite.tikz}


  Cada ponto do desenho original
  é levado em um ponto no novo reticulado.
  Qual é a regra que determina onde determinado ponto $P = (\alpha, \beta)$ é levado?
  As coordenadas de $P$ indicam que,
  partindo da origem,
  para chegarmos ao ponto $P$,
  devemos andar $\alpha$ unidades na direção e no sentido do vetor $\vector{e_1} = (1,0)$,
  e $\beta$ unidades na direção e no sentido do vetor $\vector{e_2} = (0,1)$.
  Estamos apenas dizendo que $P = \alpha \vector{e_1} + \beta \vector{e_2}$.
  Pela figura,
  é evidente que queremos que os pontos $\vector{e_1} = (1,0)$ e $\vector{e_2} = (0,1)$
  sejam transportados para os pontos correspondentes aos vetores $\vector{v_1}$ e $\vector{v_2}$,
  representados na figura~\ref{figure:image_of_the_base:r2}.
  Já o ponto $P$,
  ao ser transportado,
  deverá corresponder ao vetor $\alpha \vector{v_1} + \beta \vector{v_2}$.
  O que queremos é que nesse segundo sistema de coordendadas $(\vector{v_1}, \vector{v_2})$,
  o ponto $P$ seja transportado para o ponto $(\alpha, \beta)_{\family{R}}$,
  onde $\family{R} = (\vector{v_1}, \vector{v_2})$ é a base ordenada formada pelos vetores
  $\vector{v_1}$ e $\vector{v_2}$.

  \nftikzpic[figure:image_of_the_base:r2]
    {01/03/figs/image_of_the_base_in_r2.tikz}


  Note que pra fazermos os cálculos,
  não estamos de fato trabalhando com pontos.
  Não cosumamos \emph{somar} pontos,
  ou multiplicá-los por um escalar.
  Estamos trabalhando com vetores.
  Se escolhermos um ponto base $O$,
  podemos então falar do ponto $O + \vector{v}$,
  ao invés de falarmos do vetor $\vector{v}$.
  Com essa identificação,
  podemos usar todo o ferramental disponível ao se trabalhar com vetores:
  soma e produto por escalar,
  combinações lineares,
  conjuntos geradores,
  base,
  transformação linear,
  etc.
  Daqui por diante,
  não vamos mais fazer teoria com pontos,
  apenas vetores.
  Vamos deixar os pontos para os exemplos ou aplicações.

  Estamos então,
  falando de uma transformação
  $\function{T}{\reals^2}{\reals^2}$.
  O vetor $\vector{p}$
  (vamos usar letra minúscula, já que não é mais um \emph{ponto})
  é levado no vetor $T(\vector{p})$.
  É costume omitir os parênteses e escrever $T\vector{p}$.
  Acompanhando pela figura \ref{figure:linear_map:r2},
  fica claro que a imagem de $\vector{e_1}$ é o vetor $\vector{v_1}$,
  e que a imagem de $\vector{e_2}$ é o vetor $\vector{v_2}$.
  Como já explicado,
  a imagem do vetor $\vector{p} = p_1 \vector{e_1} + p_2 \vector{e_2}$ é alcançada se andarmos
  $p_1$ unidades do vetor $\vector{v_1}$ e em seguida,
  $p_2$ unidades do vetor $\vector{v_2}$.
  Ou seja,
  \begin{equation*}
    T\vector{p}
    =
    p_1 \vector{v_1}
    +
    p_2 \vector{v_2}.
  \end{equation*}
  Ou,
  numa notação mais precisa,
  \begin{equation*}
    \functionarray{T}{\reals^2}{\reals^2}{(p_1, p_2)}{p_1 \vector{v_1} + p_2 \vector{v_2}}.
  \end{equation*}
  Como já sabemos,
  as transformações desse tipo são as já conhecidas \emph{transformações lineares}.
  Não esqueça que
  \begin{equation*}
    \vector{v_1} = T \vector{e_1}
    \quad\text{e}\quad
    \vector{v_2} = T \vector{e_2}.
  \end{equation*}
  Assim,
  nossa transformação original pode ser expressa pela matriz
  \begin{equation*}
    \matrixrepresentation{T}
    =
    \matrixrepresentation{\vector{v_1} & \vector{v_2}}.
  \end{equation*}

  \nftikzpic[figure:linear_map:r2]
    {01/03/figs/linear_map_in_r2.tikz}
