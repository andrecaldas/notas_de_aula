\section{Números Complexos}

  Esta seção é um exemplo de como os conceitos aprendidos
  neste capítulo podem nos ajudar a entender melhor os números complexos.

  Não existe um número real $x$ tal que $x^2 = -1$.
  Mas podemos acrescentar a $\reals$ um novo símbolo $\imaginary$,
  tal que $\imaginary^2 = -1$.
  Podemos fazer isso
  \emph{preservando as propriedades} das operações de soma e produto.
  Fazemos soma e produto exatamente como fazemos com polinômios,
  tratando $\imaginary$ como se fosse uma variável.
  Mas ao final,
  substituímos $\imaginary^2$ por $-1$.
  \begin{align}
    \label{equation:complexes:sum}
    (a + b \imaginary)
    +
    (x + y \imaginary)
    &=
    (a + x) + (b + y) \imaginary
    \\
    \label{equation:complexes:product}
    \begin{split}
      (a + b \imaginary)
      (x + y \imaginary)
      &=
      ax
      +
      (ay + xb) \imaginary
      +
      by \imaginary^2
      \\
      &=
      (ax - by)
      +
      (ay + xb) \imaginary.
    \end{split}
  \end{align}
  E como tudo na vida,
  na matemática a gente também precisa de sorte! :-)

  O objetivo deste capítulo
  é mostrar ao leitor como essa construção algébrica
  resulta em um objeto com muitas propriedades geométricas.
  \textbf{Sorte?}

  Os números complexos são o conjunto
  \begin{equation*}
    \complexes
    =
    \setsuchthat{a + b\imaginary}{(a,b) \in \reals^2},
  \end{equation*}
  junto com as operações de soma e produto.
  A notação $a + b\imaginary$ é uma questão de gosto.
  Utilizando a identificação $(a,b) \sim a + b\imaginary$,
  também é muito comum falarmos do número complexo $(a,b)$.

  No entanto,
  há que se tomar cuidado!
  Existe uma operação de soma em $\reals^2$,
  e uma operação de soma em $\complexes$.
  Mas, por sorte,
  são a mesma coisa:
  soma coordenada a coordenada.
  Ou seja,
  a \cref{equation:complexes:sum} mostra que
  tratando $(a,b)$ e $(x,y)$ como números complexos
  \begin{equation*}
    (a + b\imaginary)
    +
    (x + y\imaginary)
    \sim
    (a,b)
    +
    (x,y).
  \end{equation*}

  Em $\reals^2$,
  temos o produto por escalar
  \begin{equation*}
    (z,w) = \alpha (x,y),
  \end{equation*}
  com $\alpha \in \reals$.
  Em $\complexes$,
  também podemos multiplicar um número real por um número complexo:
  \begin{equation*}
    \alpha(a + b\imaginary)
    =
    \alpha a + \alpha b\imaginary.
  \end{equation*}
  Com a identificação entre $\complexes$ e $\reals^2$,
  \begin{equation*}
    \alpha(a + b\imaginary)
    \sim
    \alpha(a,b).
  \end{equation*}

  Portanto,
  ao identificarmos $\complexes$ e $\reals^2$,
  estamos identificando mais do que os conjuntos $\complexes$ e $\reals^2$.
  Estamos também identificando-os como \emph{espaços vetoriais}:
  soma e produto por escalar (produto por número real)
  também são identificados.

  Mas e o produto de dois números complexos?
  Em $\complexes$,
  podemos multiplicar dois números,
  como mostra a \cref{equation:complexes:product}.
  Mas em $\reals^2$,
  a princípio não temos um produto
  \begin{equation*}
    (z,w) = (a,b)(x,y).
  \end{equation*}

  Seja $k = a + b\imaginary$ um número complexo.
  Vamos denotar por
  \begin{equation*}
    \functionarray{M_k}{\complexes}{\complexes}{z}{kz}
  \end{equation*}
  a transformação que toma um $z \in \complexes$ qualquer
  e multiplica por $k$.
  As propriedades de \emph{associatividade},
  \emph{comutatividade} e \emph{distributividade}
  em $\complexes$ mostram que $M_k$ é uma transformação linear.
  De fato,
  para $\alpha \in \reals$
  \begin{gather*}
    M_k(\alpha z)
    =
    k (\alpha z)
    =
    \alpha (k z)
    =
    \alpha M_k(z)
    \\
    M_k(z + w)
    =
    k (z + w)
    =
    kz + kw
    =
    M_k(z) + M_k(w).
  \end{gather*}

  Assim,
  identificando $\complexes$ com $\reals^2$,
  $M_k$ pode ser representado como uma matriz.
  Podemos descobrir as colunas da matriz,
  calculando $M_k$ em $(1,0)$ e $(0,1)$.
  Lembre-se que $(1,0) \sim 1$ e $(0,1) \sim \imaginary$.
  Portanto,
  as colunas de $\matrixrepresentation{M_k}$ são dadas por
  \begin{gather}
    \label{equation:complex_multiplication_basis}
    M_k(1)
    =
    k 1
    =
    k
    =
    a + b \imaginary
    \\
    M_k(\imaginary)
    =
    k \imaginary
    =
    -b + a \imaginary.
  \end{gather}
  Ou seja,
  \begin{equation*}
    \matrixrepresentation{M_k}
    =
    \matrixrepresentation{a & -b \\ b & a}.
  \end{equation*}
  Vamos analisar melhor essa matriz,
  que é parecida com a \emph{matriz de rotação}
  da \cref{section:rotation}.

  {\input{01/03/09/01_imaginary_product}}
  {\input{01/03/09/02_complex_product}}
