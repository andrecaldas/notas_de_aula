\section{Subespaços}
  \label{section:subspaces}

  No \cref{example:vector_sum:r3},
  tratamos o plano $xz$ em $\reals^3$,
  como se fosse $\reals^2$.
  Subconjuntos de $\reals^n$
  que com as mesmas operações de \emph{soma} e \emph{produto por escalar}
  se comportam como se fossem um $\reals^m$ dentro de $\reals^n$,
  são os \emph{subespaços vetoriais} de $\reals^n$.
  Dizemos que esses subconjuntos são
  \emph{fechados com relação à soma}
  e
  \emph{fechados com relação ao produto por escalar}.
  Antes de uma definição formal,
  vejamos alguns exemplos.

  \begin{example}[Plano $xz$]
    \label{example:plane_xz}
    Seja $P \subset \reals^3$,
    o \emph{plano $xz$}.
    Ou seja,
    \begin{equation*}
      P
      =
      \setsuchthat{\alpha \vector{e_1} + \beta \vector{e_3}}{\alpha, \beta \in \reals}.
    \end{equation*}
    Em coordenadas,
    \begin{equation*}
      P
      =
      \setsuchthat{(\alpha, 0, \beta) \in \reals^3}{\alpha, \beta \in \reals}.
    \end{equation*}

    \nftikzpic{01/05/figs/plane_xz.tikz}

    Restritas a $P$,
    as operações de soma e produto por escalar continuam bem definidas.
    Ou seja,
    \begin{align*}
      \vector{v},
      \vector{w}
      \in
      P
      &\Rightarrow
      \vector{v}
      +
      \vector{w}
      \in
      P
      \\
      \alpha \in \reals,\,
      \vector{v}
      \in
      P
      &\Rightarrow
      \alpha
      \vector{v}
      \in
      P.
    \end{align*}
    E temos uma bijeção natural
    entre $\reals^2$ e $P$,
    que preserva a soma e o produto por escalar.
    Ou seja,
    preserva a estrutura de \emph{espaço vetorial}.
    \begin{equation*}
      \functionarray{T}{\reals^2}{P}{(\alpha, \beta)}{\alpha \vector{e_1} + \beta \vector{e_3}}.
    \end{equation*}
  \end{example}



  Todo subconjunto $P \subset \reals^n$ não vazio que é
  \emph{fechado por produto por escalar}
  deve necessariamente conter $\vector{0}$.
  De fato,
  se $\vector{v} \in P$,
  então
  \begin{equation*}
    \vector{0}
    =
    0 \vector{v}
    \in
    P.
  \end{equation*}
  Para que um plano $P \subset \reals^3$ seja um subespaço,
  é necessário e suficente que $\vector{0} \in P$.
  Ou seja,
  que $P$ passe pela origem.

  \begin{example}[Plano qualquer contendo $\vector{0}$]
    \label{example:subspaces:plane}
    Suponha que $P \subset \reals^3$ seja um plano contendo $\vector{0}$.
    Sejam $\vector{v}, \vector{w} \in P$ tais que
    $\vector{0}$, $\vector{a}$ e $\vector{b}$ não são colineares.
    Então,
    \begin{equation*}
      P
      =
      \setsuchthat{\alpha \vector{a} + \beta \vector{b}}{\alpha, \beta \in \reals}
    \end{equation*}
    é o plano de $\reals^3$ que contém os pontos
    $\vector{0}$, $\vector{a}$ e $\vector{b}$.
    O leitor é convidado a demonstrar que o conjunto $P$ é 
    \emph{fechado com relação à soma}
    e
    \emph{fechado com relação ao produto por escalar}.
    Na verdade,
    neste exemplo,
    em nenhum momento utilizamos o fato de o espaço em questão ser $\reals^3$.
    Tudo funciona exatamente da mesma maneira se considerarmos $\reals^n$.

    Os conjuntos $P$ dessa forma são os
    \emph{subespaços de dimensão $2$}.
    Para que estejamos de fato tratando de um plano,
    e não de uma reta, tivemos que assumir que os pontos
    $\vector{0}$, $\vector{a}$ e $\vector{b}$
    não são colineares.
    Isso significa que os três pontos são distintos,
    e que não existe $\alpha \in \reals$ tal que
    $\vector{a} = \alpha \vector{b}$.

    Novamente,
    temos uma bijeção natural
    entre $\reals^2$ e $P$,
    que preserva a soma e o produto por escalar.
    Ou seja,
    preserva a estrutura de \emph{espaço vetorial}.
    \begin{equation*}
      \functionarray{T}{\reals^2}{P}{(\alpha, \beta)}{\alpha \vector{a} + \beta \vector{b}}.
    \end{equation*}
  \end{example}

  % TODO: exercício - pra que serve a hipótese de os três pontos serem distintos?

  Um exemplo ainda mais simples é o subespaço unidimensional.

  \begin{example}[Subespaço unidimensional]
    Seja $\vector{v} \in \reals^n$ um vetor não nulo.
    Então,
    o conjunto
    \begin{equation*}
      R
      =
      \setsuchthat{\alpha v}{\alpha \in \reals}
    \end{equation*}
    é um reta em $\reals^n$ que passa em $\vector{0}$.
    O conjunto $R$ é
    \emph{fechado com relação à soma}
    e
    \emph{fechado com relação ao produto por escalar}.
    Esses são os subespaços \emph{unidimensionais}.

    Assim como no caso do plano,
    temos uma bijeção natural
    entre $\reals$ e $R$,
    que preserva a soma e o produto por escalar.
    Ou seja,
    preserva a estrutura de \emph{espaço vetorial}.
    \begin{equation*}
      \functionarray{T}{\reals}{R}{\alpha}{\alpha \vector{v}}.
    \end{equation*}
  \end{example}


  \begin{definition}[Subespaço vetorial]
    Um subconjunto \textbf{não vazio} $W \subset \reals^n$
    é chamado de \emph{subespaço vetorial}
    quando é
    fechado com relação à soma
    e
    fechado com relação ao produto por escalar.
    Ou seja,
    \begin{enumerate}
      \item
        $\alpha \in \reals$,
        $\vector{v} \in W$
        $\Rightarrow$
        $\alpha \vector{v} \in W$.

      \item
        $\vector{v}, \vector{w} \in W$
        $\Rightarrow$
        $\vector{v} + \vector{w} \in W$.
    \end{enumerate}
  \end{definition}


  Com uma definição precisa,
  agora podemos discutir um exemplo
  que ao mesmo tempo que é evidente,
  é também intrigante,
  já que sua utilidade parece,
  à primeira vista,
  muito duvidosa.

  \begin{example}[Subespaços triviais]
    O subconjunto de $\reals^n$ dado por
    \begin{equation*}
      X
      =
      \set{\vector{0}}
    \end{equation*}
    é um subespaço de $\reals^n$ de dimensão $0$.
    E o conjunto $Y = \reals^n$ também é
    um subespaço de $\reals^n$.
    Alguns autores dizem que $X$ é o \emph{subespaço trivial} de $\reals^n$.
    Outros dizem que $X$ e $Y$ são os \emph{subespaços triviais} de $\reals^n$.
  \end{example}

  \begin{obs}
    Daqui por diante,
    falaremos de várias propriedades que se aplicam tanto a $\reals^n$
    quanto a qualquer subespaço de $\reals^n$.
    Apenas no \cref{chapter:formal_definitions}
    vamos definir \emph{espaços vetorias} de um modo mais geral.
    Veja a \cref{definition:vector_space}.

    Por enquanto,
    quando falarmos em \emph{espaços vetoriais},
    estaremos nos referindo apenas a $\reals^n$ e a seus subespaços.
    Mas sempre que nos referirmos a \emph{espaços vetoriais},
    os resultados serão verdadeiros também para
    \emph{espaços vetoriais} gerais,
    conforme a \cref{definition:vector_space}.
  \end{obs}
