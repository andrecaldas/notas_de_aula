\subsection{Subespaço Gerado}

  Dois vetores $\vector{v}, \vector{w} \in \reals^3$
  podem gerar um \emph{plano},
  quando
  $\vector{0}$, $\vector{v}$ e $\vector{w}$
  não são colineares.
  Ou seja,
  quando
  $\vector{v}$
  e
  $\vector{w}$
  não são paralelos.
  Podem gerar o \emph{espaço trivial} $\set{\vector{0}}$,
  quando
  $\vector{v} = \vector{w} = \vector{0}$.
  E podem gerar uma \emph{reta},
  quando não são paralelos e não são todos nulos.
  De qualquer forma,
  independentemente de que vetores
  $\vector{v}$ e $\vector{w}$ escolhemos,
  esses dois vetores \emph{geram} um \emph{subespaço} de $\reals^n$.
  Pode não ser um plano,
  mas será um \emph{subespaço}.

  De modo um pouco mais geral,
  se
  \begin{equation*}
    S
    =
    \set{\vector{v_1}, \dotsc, \vector{v_m}}
  \end{equation*}
  é um subconjunto finito de $\reals^n$,
  o subconjunto
  \begin{equation*}
    \generatedsubspace{S}
    =
    \setsuchthat{\alpha_1 \vector{v_1} + \dotsb + \alpha_m \vector{v_m}}
                {\alpha_1, \dotsc, \alpha_m \in \reals}
  \end{equation*}
  é o subespaço (de $\reals^n$) gerado por $S$.

  Podemos definir o \emph{subespaço gerado} por um subconjunto
  $S \subset \reals^n$ qualquer (mesmo que infinito).
  O leitor é convidado a fazer,
  por conta própria,
  definições alternativas.
  É sempre bom saber falar da mesma coisa de vários pontos de vista diferentes.

  \begin{definition}[Subespaço gerado]
    \label{definition:generated:subspace:rn}
    Seja $S \subset \reals^n$
    um subconjunto qualquer de $\reals^n$.
    O \emph{subespaço gerado} por $S$,
    denotado por $\generatedsubspace{S}$,
    é o menor subespaço $E$ de $\reals^n$,
    tal que
    \begin{equation*}
      S \subset E.
    \end{equation*}
    Quando $S = \set{\vector{v_1}, \dotsc, \vector{v_n}}$,
    escrevemos
    $\generatedsubspace{\vector{v_1}, \dotsc, \vector{v_n}}$.
  \end{definition}

  Para que a \cref{definition:generated:subspace:rn}
  faça sentido é necessário dar um significado preciso
  para a expressão
  \begin{quote}
    {\ldots}
    o menor subespaço $E \subset \reals^n$ tal que
    $S \subset E$.
  \end{quote}
  Queremos dizer três coisas:
  \begin{enumerate}
    \item
      \label{item:generated_subspace:subspace}
      Que $E$ é um subespaço de $\reals^n$.

    \item
      \label{item:generated_subspace:contains}
      Que $S \subset E$.

    \item
      \label{item:generated_subspace:minimal}
      Que se $F$ é um subespaço de $\reals^n$
      tal que $S \subset F$,
      então
      \begin{equation*}
        E
        \subset
        F.
      \end{equation*}
  \end{enumerate}
  Mas será que tal coisa existe?
  Por exemplo,
  \begin{quote}
    Qual é o menor número real maior do que $\alpha$?
  \end{quote}
  Queremos encontrar $a$ satisfazendo três coisas:
  \begin{enumerate}
    \item
      $a \in \reals$.

    \item
      $\alpha < a$.

    \item
      Se $b \in \reals$ é maior que $\alpha$,
      então
      \begin{equation*}
        a
        \leq
        b.
      \end{equation*}
  \end{enumerate}
  Tal número \textbf{não} existe!
  Qual é o menor número real maior que $0$?
  $0.1$?
  $0.01$?
  $0.001$?
  $0.00000001$?
  No entanto,
  para o subespaço da \cref{definition:generated:subspace:rn},
  a resposta é: \textbf{sim, existe}.
  Antes de mostrarmos que existe,
  vejamos um exemplo simples.

  \begin{example}
    \label{example:generated:unidimensional}
    Seja $\vector{v} \in \reals^3$ um vetor não nulo.
    Então,
    \begin{equation*}
      \generatedsubspace{\vector{v}}
      =
      \setsuchthat{\alpha \vector{v}}{\alpha \in \reals}.
    \end{equation*}
    De fato,
    se chamarmos o subespaço da direita de $W$,
    temos que $\vector{v} \in W$,
    e todo subespaço de $\reals^3$ que contém $\vector{v}$
    tem que conter todos os múltiplos de $\vector{v}$,
    e portanto,
    tem que conter $W$.
    Ou seja,
    $W$ é o \emph{menor} subespaço de $\reals^3$ que contém
    $\vector{v}$.
    Dessa forma,
    $\generatedsubspace{\vector{v}} = W$.
  \end{example}


  Uma outra forma de caracterizar o \emph{subespaço gerado}
  é através de \emph{combinações lineares}.
  Vamos recordar a \cref{definition:linear_combination}.

  Seja $S \subset \reals^n$ um subconjunto qualquer de $\reals^n$.
  Uma combinação linear de elementos de $S$
  é um vetor $\vector{v} \in \reals^n$ da forma
  \begin{equation*}
    \vector{v}
    =
    \alpha_1 \vector{v_1}
    + \dotsb +
    \alpha_k \vector{v_k},
  \end{equation*}
  onde $\alpha_1, \dotsc, \alpha_k \in \reals$
  e $\vector{v_1}, \dotsc, \vector{v_k} \in S$.

  Vamos,
  por agora,
  denotar por
  \begin{equation*}
    L_S
    =
    \setsuchthat{\alpha_1 \vector{v_1} + \dotsb + \alpha_k \vector{v_k}}
                {\alpha_1, \dotsc, \alpha_k \in \reals,\, \vector{v_1}, \dotsc, \vector{v_k} \in S}
  \end{equation*}
  o conjunto de todas as combinações lineares de elementos de $S$.
  Perceba que dizer que $S \subset V$
  é um \emph{subespaço} de $V$
  é o mesmo que dizer que $L_S = S$.
  A demonstração da proposição a seguir é muito semelhante
  ao \cref{example:generated:unidimensional}.

  \begin{proposition}
    Seja $V$ um espaço vetorial.
    Se $S \subset V$ é não vazio,
    então
    $\generatedsubspace{S}$ é o conjunto formado por
    \textbf{todas} as \emph{combinações lineares} de elementos de $S$.
  \end{proposition}

  \begin{proof}
    Se $W \subset V$ é um subespaço qualquer com $S \subset W$,
    então
    \begin{equation*}
      S
      \subset
      L_S
      \subset
      L_W
      =
      W.
    \end{equation*}
    Ou seja,
    $L_S$ é o \textbf{menor} subespaço de $V$ que contém $S$.
    Assim,
    \begin{equation*}
      \generatedsubspace{S}
      =
      L_S.
    \end{equation*}
  \end{proof}

  \begin{example}
    \label{example:generated_subspace:infinite_set}
    O conjunto
    \begin{equation*}
      S
      =
      \setsuchthat{\left(\frac{1}{n+1}, 1, 0\right) \in \reals^3}{n \in \naturals}
    \end{equation*}
    é um conjunto infinito que gera o plano $xy$.
  \end{example}
