\subsection{A matriz inversa como função}

  Uma função pode ter inversa à esquerda,
  quando é \emph{injetiva};
  e pode ter inversa à direta,
  quando é \emph{sobrejetiva}.
  Quando a função é \emph{bijetiva},
  ela tem apenas uma inversa à esquerda,
  que é também a única inversa à direita.
  % TODO: falar disso em ``funções''.

  Mais adiante,
  veremos que uma transformação linear
  $\function{T}{\reals^p}{\reals^q}$
  só pode ser \emph{bijetiva} quando $p = q$.
  E é por isso que só falamos em inversa
  de matrizes \emph{quadradas}.

  Vamos refazer o \cref{example:matrix_inverse:just_matrix:permutation}.

  \begin{example}[permutação de dois elementos]
    \label{example:matrix_inverse:permutation}
    A matriz
    \begin{equation*}
      M = \matrixrepresentation{0 & 1 \\ 1 & 0}
    \end{equation*}
    é a inversa dela mesma.
    Afinal de contas,
    vista como função,
    ela permuta $\vector{e_1}$ e $\vector{e_2}$:
    \begin{equation*}
      T_M\vector{e_1} = \vector{e_2}
      \quad\text{e}\quad
      T_M\vector{e_2} = \vector{e_1}.
    \end{equation*}
    Portanto,
    \begin{equation*}
      MM
      =
      \matrixrepresentation{T_M \circ T_M}
      =
      \matrixrepresentation{\identity}
      =
      I_2.
    \end{equation*}
  \end{example}

  Um outro caso que já vimos,
  é o \cref{example:matrix_inverse:just_matrix:simple_example}.

  \begin{example}
    \label{example:matrix_inverse:simple_example}
    A matriz
    \begin{equation*}
      \matrixrepresentation{1 & 1 \\ 1 & 0}
    \end{equation*}
    leva $\vector{e_2}$ em $\vector{e_1}$.
    Portanto,
    \textbf{se a inversa existir},
    deve levar $\vector{e_1}$ em $\vector{e_2}$.
    Assim,
    a primeira coluna da matriz inversa é formada pelo vetor $\vector{e_2}$:
    \begin{equation*}
      \matrixrepresentation{0 & * \\ 1 & *}.
    \end{equation*}

    Por outro lado,
    $(1,-1)$ é levado em
    $\vector{e_2}$:
    \begin{equation*}
      \matrixrepresentation{1 & 1 \\ 1 & 0}
      \matrixrepresentation{1 \\ -1}
      =
      \matrixrepresentation{1 \\ 1}
      -
      \matrixrepresentation{1 \\ 0}
      =
      \matrixrepresentation{0 \\ 1}.
    \end{equation*}
    A inversa, se existir, deve levar $\vector{e_2}$ em $(1,-1)$.
    Assim,
    a segunda coluna da inversa é formada pelo vetor $(1,-1)$.
    Portanto,
    se tem uma matriz que é a inversa de $\matrixrepresentation{1 & 1 \\ 1 & 0}$,
    tem que ser a matriz $\matrixrepresentation{0 & 1 \\ 1 & -1}$.
  \end{example}

  No \cref{example:matrix_inverse:simple_example},
  enfatizamos bastante a expressão
  \textbf{se a inversa existir}.
  Isso, porque o que de fato mostramos
  é que tal matriz \textbf{desfaz}
  a operação que a matriz original fez.
  Portanto,
  o que determinamos foi a \emph{inversa à esquerda}.
  Esta questão está posta na \cref{question:left_inverse_imply_right_inverse}.

  Já sabemos que
  existem matrizes que não tem inversa{\ldots}

  \begin{example}
    \label{example:matrix_inverse:no_inverse}
    Já vimos no \cref{example:matrix_inverse:just_matrix:no_inverse}
    que as matrizes a seguir não possuem inversa.
    Você consegue demonstrar isso de uma maneira simples?
    \begin{align*}
      \matrixrepresentation{0 & 0 \\ 0 & 0}
      \quad&\quad
      \matrixrepresentation{0 & 0 \\ 1 & 0}
      \\
      \matrixrepresentation{1 & 1 \\ 5 & 5}
      \quad&\quad
      \matrixrepresentation{3 & -2 \\ 6 & -4}
      \\
      \matrixrepresentation{1 & 2 & 3 \\ 2 & 4 & -1 \\ 4 & 8 & 5}
      \quad&\quad
      \matrixrepresentation{3 & 1 & 2 \\ 1 & 5 & -4 \\ 2 & 1 & 1}.
    \end{align*}
    Por exemplo,
    as duas primeiras matrizes
    \begin{equation*}
      \matrixrepresentation{0 & 0 \\ 0 & 0}
      \quad\text{e}\quad
      \matrixrepresentation{0 & 0 \\ 1 & 0}
    \end{equation*}
    levam qualquer múltiplo do vetor $\vector{e_2}$ em $(0,0)$.
    E portanto,
    não são \emph{injetivas}.
    Já a matriz
    \begin{equation*}
      \matrixrepresentation{1 & 1 \\ 5 & 5}
    \end{equation*}
    leva qualquer vetor em um múltiplo do vetor $(1,5)$.
    Assim,
    vetores que não são múltiplos de $(1,5)$
    não estão na imagem da transformação correspondente,
    que portanto,
    não é \emph{sobrejetiva}.

    E as outras?
    Você consegue dizer porque não são inversíveis?
  \end{example}
