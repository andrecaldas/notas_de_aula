\subsubsection{Exemplo: matriz de permutação}

  Uma função que \emph{permuta} as entradas
  de um vetor de $\reals^p$ é uma \emph{transformação linear}.
  Melhor do que ficar de blá, blá, blá{\ldots}
  é mostrar um exemplo!

  \begin{example}
    \label{example:permutation_matrix}
    Tomar um vetor
    $(x, y, z)$
    e transformá-lo em $(z, x, y)$,
    é o que faz a transformação
    \begin{equation*}
      \functionarray{T}{\reals^3}{\reals^3}{(x,y,z)}{x \vector{e_2} + y \vector{e_3} + z \vector{e_1}}.
    \end{equation*}
    Ela leva
    $\vector{e_1} \mapsto \vector{e_2}$,
    $\vector{e_2} \mapsto \vector{e_3}$
    e
    $\vector{e_3} \mapsto \vector{e_1}$.
    Assim,
    temos a representação matricial
    \begin{equation*}
      \matrixrepresentation{T}
      =
      \matrixrepresentation{0 & 0 & 1 \\ 1 & 0 & 0 \\ 0 & 1 & 0}.
    \end{equation*}
    Em cada linha tem apenas um número $1$.
    Os demais são zero.
    O mesmo ocorre em cada coluna:
    apenas uma entrada tem o número $1$,
    e as demais são zero.

    E, de fato,
    \begin{equation*}
      \matrixrepresentation{z \\ x \\ y}
      =
      \matrixrepresentation{0 & 0 & 1 \\ 1 & 0 & 0 \\ 0 & 1 & 0}
      \matrixrepresentation{x \\ y \\ z}.
    \end{equation*}
  \end{example}

  E se multiplicarmos uma matriz de \emph{permutação} por uma outra matriz?

  \begin{example}
    Continuando o \cref{example:permutation_matrix},
    vamos calcular
    \begin{equation*}
      \matrixrepresentation{| &|& | \\ \vector{b_1} & \vector{b_2} & \vector{b_3} \\ | &|& |}
      =
      \matrixrepresentation{| &|& | \\ \vector{a_1} & \vector{a_2} & \vector{a_3} \\ | &|& |}
      \matrixrepresentation{0 & 0 & 1 \\ 1 & 0 & 0 \\ 0 & 1 & 0}.
    \end{equation*}
    Para encontrar $\vector{b_1}$,
    tudo o que precisamos fazer é usar $\vector{e_1}$ como \emph{input}
    na matriz de permutação;
    e depois tomar o resultado e aplicá-lo na matriz seguinte.

    O vetor $\vector{e_1}$ aplicado na matriz de permutação retorna $\vector{e_2}$.
    Este,
    quando aplicado na matriz seguinte,
    retorna $\vector{a_2}$.
    Assim,
    $\vector{b_1} = \vector{a_2}$.

    Da mesma forma,
    $\vector{e_2}$ é levado em $\vector{e_3}$,
    que por sua vez é levado em $\vector{a_3}$.
    E, ao final,
    $\vector{e_3}$ é levado em $\vector{a_1}$.
  \end{example}

  De um modo geral,
  se
  \begin{equation*}
    \function{\sigma}{[n]}{[n]}
  \end{equation*}
  é uma permutação,
  podemos imaginar uma transformação $T_\sigma$,
  que leva a $j$-ésima coordenada do vetor para a posição $\sigma(j)$.
  Sendo assim,
  \begin{equation*}
    \functionarray{T_\sigma}{\reals^n}{\reals^n}{(x_1, \dotsc, x_n)}{(x_{\sigma^{-1}(1)}, \dotsc, x_{\sigma^{-1}(n)})}
  \end{equation*}
  é uma transformação linear.
  Repare que na fórmula de $T_\sigma$ utilizamos a \emph{permutação inversa} $\sigma^{-1}$!
  Para entender o porquê,
  basta notar que $\sigma$ nos diz em que posição a $j$-ésima coordenada vai parar.
  Mas para escrever o resultado em ordem,
  precisamos saber
  \begin{quote}
    - Quem é que vai parar na primeira posição?
    \\
    - Quem é que vai parar na segunda posição?
    \\
    Etc{\ldots}
  \end{quote}
  Quem vai parar na $j$-ésima posição
  é a coordenada $\sigma^{-1}(j)$,
  pois $\sigma\left(\sigma^{-1}(j)\right) = j$.

  No entanto,
  se pensarmos nos vetores da base canônica,
  $\vector{e_1}, \dotsc, \vector{e_n}$,
  é fácil ver que
  \begin{equation*}
    T_\sigma \vector{e_j}
    =
    \vector{e_{\sigma(j)}}.
  \end{equation*}
  E portanto,
  $T_\sigma$ é representada pela matriz
  \begin{equation*}
    \matrixrepresentation{T_\sigma}
    =
    \matrixrepresentation{| && | \\ \vector{e_{\sigma(1)}} & \hdots & \vector{e_{\sigma(n)}} \\ | && |}.
  \end{equation*}

  Quanto à multiplicação de uma matriz $A$,
  à direita, por uma permutação,
  temos que
  \begin{equation*}
    \matrixrepresentation{| && | \\ \vector{a_{\sigma(1)}} & \hdots & \vector{a_{\sigma(n)}} \\ | && |}
    =
    \matrixrepresentation{| && | \\ \vector{a_1} & \hdots & \vector{a_n} \\ | && |}
    \matrixrepresentation{| && | \\ \vector{e_{\sigma(1)}} & \hdots & \vector{e_{\sigma(n)}} \\ | && |}.
  \end{equation*}
  Isso é claro,
  pois com a composição das duas matrizes,
  $\vector{e_j}$ é levado em $\vector{e_{\sigma(j)}}$,
  que por sua vez é levado em $\vector{a_{\sigma(j)}}$.

  Estamos olhando para as \emph{colunas} da matriz $A$ como sendo o \emph{input},
  e as \emph{linhas} como sendo o \emph{output}.
  Quando permutamos à direita,
  ou seja,
  antes de aplicar a matriz $A$.
  Estamos permutando o \emph{input}.
  E por isso,
  a operação afeta as colunas de $A$.
  Se multiplicarmos à esquerda,
  estamos permutando o \emph{output}.

  \begin{example}
    O leitor está convidado a calcular o resultado de se multiplicar uma matriz $A$
    à esquerda por uma matriz de permutação $T_\sigma$.
    O resultado prático é o de se permutar as linhas da matriz,
    com a permutação inversa.

    Por exemplo,
    \begin{equation*}
      \matrixrepresentation{\text{---} & \vector{b_1} & \text{---} \\ \text{---} & \vector{b_2} & \text{---} \\ \text{---} & \vector{b_3} & \text{---}}
      =
      \matrixrepresentation{0 & 0 & 1 \\ 1 & 0 & 0 \\ 0 & 1 & 0}
      \matrixrepresentation{\text{---} & \vector{a_1} & \text{---} \\ \text{---} & \vector{a_2} & \text{---} \\ \text{---} & \vector{a_3} & \text{---}},
    \end{equation*}
    é tal que
    \begin{equation*}
      \vector{b_1} = \vector{a_3}
      \text{,}\quad
      \vector{b_2} = \vector{a_1}
      \quad\text{e}\quad
      \vector{b_3} = \vector{a_2}.
    \end{equation*}
  \end{example}

  De um modo geral,
  se $\sigma$ é uma permutação de $[n]$,
  \begin{equation*}
    \matrixrepresentation{\text{---} & \vector{b_1} & \text{---} \\ & \vdots & \\ \text{---} & \vector{b_n} & \text{---}}
    =
    \matrixrepresentation{T_\sigma}
    \matrixrepresentation{\text{---} & \vector{a_1} & \text{---} \\ & \vdots & \\ \text{---} & \vector{a_n} & \text{---}}
  \end{equation*}
  é tal que a coordenada $j$ é levada na coordenada $\sigma(j)$.
  Assim,
  \begin{equation*}
    \vector{b_{\sigma(j)}}
    =
    \vector{a_j}.
  \end{equation*}
  Ou, utilizando a permutação inversa,
  \begin{equation*}
    \vector{b_j}
    =
    \vector{a_{\sigma^{-1}(j)}}.
  \end{equation*}

  Esse argumento todo com as linhas pode ser feito utilizando a matriz transposta.
  Basta observar que
  \begin{equation*}
    (\matrixrepresentation{T_\sigma} A)^t
    =
    A^t
    \matrixrepresentation{T_\sigma}^t.
  \end{equation*}
  Ou seja,
  \begin{equation*}
    \matrixrepresentation{T_\sigma} A
    =
    \left(
      A^t
      \matrixrepresentation{T_\sigma}^t
    \right)^t.
  \end{equation*}
  A operação de transposição transforma linhas em colunas
  e vice-versa.
  É preciso, também, notar que
  $\matrixrepresentation{T_\sigma}^t = T_{\sigma^{-1}}$.
