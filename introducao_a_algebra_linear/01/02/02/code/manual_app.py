# NOT TO BE USED IN TEXT
# BADLY WRITTEN JUST
# JUST TO MAKE A SCREENSHOT

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

summer_ingredients = {'p': 10, 'c': 30}
winter_ingredients = {'p': 20, 'c': 15}

potato_nutrients = {'x': 3, 'y': 10, 'z': 8}
carrot_nutrients = {'x': 15, 'y': 7, 'z': 11}

class NutritionApp(Gtk.Window):
  def __init__ (self):
    super().__init__(title='App da Sopa - versão manual')

    main_box = Gtk.Paned()
    main_box.set_wide_handle(True)
    self.add(main_box)

    box = Gtk.HBox()
    main_box.add(box)
    box.set_margin_start(10)
    box.set_margin_end(10)
    box.set_margin_top(10)
    box.set_margin_bottom(10)

    input_box = Gtk.VBox()
    output_box = Gtk.VBox()
    box.add(input_box)
    box.add(output_box)

    self.summer = Gtk.SpinButton(value=0)
    self.winter = Gtk.SpinButton(value=0)

    for x in self.summer, self.winter:
        x.set_range(0, 1000)
        x.set_increments(1,10)
        x.connect('value-changed', self.update_total_ingredients)

    input_box.add(Gtk.Label(label='Tijelas de sopa de verão'))
    input_box.add(self.summer)
    input_box.add(Gtk.Label(label='Tijelas de sopa de inverno'))
    input_box.add(self.winter)

    self.result_ingredients = Gtk.Label()
    output_box.add(Gtk.Label(label='Total de ingredientes (g)'))
    output_box.add(self.result_ingredients)
    output_box.set_margin_start(50)
    output_box.set_margin_end(50)
    output_box.set_margin_top(50)
    output_box.set_margin_bottom(50)

    self.update_total_ingredients()


    box = Gtk.HBox()
    main_box.add(box)
    box.set_margin_start(10)
    box.set_margin_end(10)
    box.set_margin_top(10)
    box.set_margin_bottom(10)

    input_box = Gtk.VBox()
    output_box = Gtk.VBox()
    box.add(input_box)
    box.add(output_box)

    self.potatoes = Gtk.SpinButton(value=0)
    self.carrots = Gtk.SpinButton(value=0)

    for x in self.potatoes, self.carrots:
        x.set_range(0, 1000)
        x.set_increments(10,100)
        x.connect('value-changed', self.update_total_nutrients)

    input_box.add(Gtk.Label(label='Quantidade de batatas (g)'))
    input_box.add(self.potatoes)
    input_box.add(Gtk.Label(label='Quantidade de cenouras (g)'))
    input_box.add(self.carrots)

    self.result_nutrients = Gtk.Label()
    output_box.add(Gtk.Label(label='Nutrientes (mg)'))
    output_box.add(self.result_nutrients)
    output_box.set_margin_start(50)
    output_box.set_margin_end(50)
    output_box.set_margin_top(50)
    output_box.set_margin_bottom(50)

    self.update_total_nutrients()


  def update_total_nutrients (self, arg=None):
      p = self.potatoes.get_value()
      c = self.carrots.get_value()
      x = p * potato_nutrients['x'] + c * carrot_nutrients['x']
      y = p * potato_nutrients['y'] + c * carrot_nutrients['y']
      z = p * potato_nutrients['z'] + c * carrot_nutrients['z']

      self.result_nutrients.set_markup(f'<b>X:</b> {x:0.0f}\n<b>Y:</b> {y:0.0f}\n<b>Z:</b> {z:0.0f}')


  def update_total_ingredients (self, arg=None):
      s = self.summer.get_value()
      w = self.winter.get_value()
      p = s * summer_ingredients['p'] + w * winter_ingredients['p']
      c = s * summer_ingredients['c'] + w * winter_ingredients['c']

      self.result_ingredients.set_markup(f'<b>Batata:</b> {p:0.0f}\n<b>Cenoura:</b> {c:0.0f}')

app = NutritionApp()
app.connect("destroy", Gtk.main_quit)
app.show_all()
Gtk.main()
