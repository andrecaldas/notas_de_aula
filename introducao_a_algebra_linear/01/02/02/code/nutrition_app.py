import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

potato_nutrients = {'x': 3, 'y': 10, 'z': 8}
carrot_nutrients = {'x': 15, 'y': 7, 'z': 11}

class NutritionApp(Gtk.Window):
  def __init__ (self):
    super().__init__(title='App da Nutrição')

    fbox = Gtk.FlowBox()
    input_box = Gtk.VBox()
    output_box = Gtk.VBox()
    fbox.add(input_box)
    fbox.add(output_box)

    self.potatoes = Gtk.SpinButton(value=0)
    self.carrots = Gtk.SpinButton(value=0)

    for x in self.potatoes, self.carrots:
        x.set_range(0, 1000)
        x.set_increments(10,100)
        x.connect('value-changed', self.update_total_nutrients)

    input_box.add(Gtk.Label(label='Quantidade de batatas (g)'))
    input_box.add(self.potatoes)
    input_box.add(Gtk.Label(label='Quantidade de cenouras (g)'))
    input_box.add(self.carrots)

    self.result = Gtk.Label()
    output_box.add(Gtk.Label(label='Nutrientes (mg)'))
    output_box.add(self.result)

    self.update_total_nutrients()
    self.add(fbox)

  def update_total_nutrients (self, arg=None):
      p = self.potatoes.get_value()
      c = self.carrots.get_value()
      x = p * potato_nutrients['x'] + c * carrot_nutrients['x']
      y = p * potato_nutrients['y'] + c * carrot_nutrients['y']
      z = p * potato_nutrients['z'] + c * carrot_nutrients['z']

      self.result.set_markup(f'<b>X:</b> {x:0.0f}\n<b>Y:</b> {y:0.0f}\n<b>Z:</b> {z:0.0f}')

app = NutritionApp()
app.connect("destroy", Gtk.main_quit)
app.show_all()
Gtk.main()
