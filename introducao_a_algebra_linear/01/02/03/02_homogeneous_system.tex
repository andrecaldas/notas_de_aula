\subsection{Sistemas homogêneos}

  Um \emph{sistema linear homogêneo} é um sistema da forma
  \begin{equation*}
    T(\vector{v})
    =
    \vector{0},
  \end{equation*}
  onde $\function{T}{\reals^p}{\reals^q}$ é uma \emph{transformação linear}
  e $\vector{v}$ são as \emph{incógnitas}.

  O primeiro fato interessante sobre os \emph{sistemas homogêneos}
  é que ele sempre tem ao menos uma solução $\vector{v} = \vector{0}$.
  Pois para uma transformação linear é sempre verdade que
  \begin{equation*}
    T(\vector{0})
    =
    \vector{0}.
  \end{equation*}
  Esta é a \emph{solução trivial}.
  Será que existem outras?
  A resposta depende, claro, do sistema específico.

  Considere um sistema não necessariamente homogêneo
  \begin{equation}
    \label{equation:non_homogeneous_system}
    T(\vector{v})
    =
    \vector{a}.
  \end{equation}
  Suponha que esse sistema tenha uma solução que não é única.
  Ou seja,
  existem $\vector{s}, \vector{t} \in \reals^p$ \textbf{distintos}
  tais que $T\vector{s} = T\vector{t}$ ($= \vector{a}$).
  Neste caso,
  o sistema homogêneo associado tem solução não trivial,
  pois $\vector{s} - \vector{t} \neq \vector{0}$.
  E pela \emph{linearidade} de $T$,
  \begin{equation*}
    T(\vector{s} - \vector{t})
    =
    T\vector{s}
    -
    T\vector{t}
    =
    \vector{0}.
  \end{equation*}
  Por outro lado,
  quando $\vector{z}$ é uma solução qualquer do \emph{sistema homogêneo}
  e $\vector{s}$ é uma solução de \labelcref{equation:non_homogeneous_system},
  então $\vector{s} + \vector{z}$
  também é solução.

  Assim,
  denotando por
  \begin{equation*}
    Z
    =
    T^{-1}(\vector{0})
  \end{equation*}
  o conjunto de todas as soluções do sistema homogêneo,
  as soluções de \labelcref{equation:non_homogeneous_system} são dadas pelo conjunto
  \begin{equation*}
    \vector{s}
    +
    Z
    =
    \setsuchthat{\vector{s} + \vector{z}}{\vector{z} \in Z}.
  \end{equation*}

  Em particular,
  para um sistema linear,
  ou todas as soluções são únicas
  ou nenhuma solução é única.
  Quando $Z$ é um conjunto unitário,
  todas as soluções são únicas;
  e quando não é unitário,
  nenhuma solução é única.
  Mas é importante notar que
  \textbf{mesmo que as soluções não sejam únicas},
  podem haver escolhas para $\vector{a}$ tais que o sistema não tenha solução.

  \begin{example}
    Seja $\function{T}{\reals^3}{\reals^2}$
    a \emph{transformação linear} cuja representação matricial seja dada por
    \begin{equation*}
      \matrixrepresentation{T}
      =
      \matrixrepresentation{1 & 4 & 7 \\ 2 & 5 & 8 \\ 3 & 6 & 9}.
    \end{equation*}
    O \emph{sistema homogêneo} correpondente
    \begin{equation*}
      T\vector{v}
      =
      \vector{0}
    \end{equation*}
    tem mais de uma solução.
    Por exemplo,
    \begin{equation*}
      \matrixrepresentation{7\\8\\9}
      -
      \matrixrepresentation{4\\5\\6}
      =
      \matrixrepresentation{3\\3\\3}
      =
      \matrixrepresentation{4\\5\\6}
      -
      \matrixrepresentation{1\\2\\3}.
    \end{equation*}
    E, portanto,
    \begin{equation*}
      \matrixrepresentation{1\\2\\3}
      -
      2
      \matrixrepresentation{4\\5\\6}
      +
      \matrixrepresentation{7\\8\\9}
      =
      \matrixrepresentation{0\\0\\0}.
    \end{equation*}
    Ou seja,
    $T(1, -2, 1) = (0,0,0)$.

    O leitor está convidado a mostrar que
    \begin{equation*}
      T\vector{v}
      =
      (0,1,0)
    \end{equation*}
    não tem solução.
    Mas,
    no entanto,
    para $\vector{a} = (5,7,9)$,
    além da solução
    \begin{equation*}
      T(1,1,0)
      =
      (5,7,9),
    \end{equation*}
    temos também
    \begin{align*}
      T(1+1, 1-2, 0+1)
      &=
      T(2,-1,1)
      \\
      &=
      (2*1 - 4 + 7, 2*2 - 5 + 8, 2*3 - 6 + 9)
      \\
      &=
      (5,7,9).
    \end{align*}
  \end{example}

  Por fim,
  podemos também concluir que um sistema homogêneo pode
  \begin{enumerate}
    \item
      não ter solução;
    \item
      ter uma única solução; ou
    \item
      ter infinitas soluções.
  \end{enumerate}
  Isso,
  porque o conjunto $Z = T^{-1}(\vector{0})$ ou é unitário com
  \begin{equation*}
    Z
    =
    \set{\vector{0}};
  \end{equation*}
  ou é inifinito,
  pois, para todo $\alpha \in \reals$,
  \begin{equation*}
    T(\alpha\vector{z})
    =
    \alpha
    T\vector{z}
    =
    \alpha
    \vector{0}
    =
    \vector{0}.
  \end{equation*}
  Portanto,
  se $\vector{z} \in Z$ não é o vetor nulo,
  então $\alpha\vector{z} \in Z$ ($\alpha \in \reals$)
  são infinitas soluções distintas para o \emph{sistema homogêneo}.
