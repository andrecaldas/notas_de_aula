\subsection{Resolvendo sistemas lineares}

  Um sistema linear pode ser escrito na forma matricial
  \begin{equation*}
    A
    \matrixrepresentation{\vector{v}}
    =
    \matrixrepresentation{\vector{a}},
  \end{equation*}
  onde a matrix $m \times n$ $A$
  e $\vector{a} \in \reals^m$ são dados,
  e $\vector{v} \in \reals^n$ é o que queremos descobrir.

  Se $A$ é uma matriz inversível,
  e $\vector{v}$ uma solução do sistema,
  então
  \begin{equation*}
    \vector{v}
    =
    A^{-1}A
    \matrixrepresentation{\vector{v}}
    =
    A^{-1}
    \matrixrepresentation{\vector{a}}.
  \end{equation*}
  Usamos o fato de que $A^{-1}A = I$ para concluir que
  \textbf{se $\vector{v}$ é solução do sistema},
  então podemos determinar
  \begin{equation*}
    \vector{v}
    =
    A^{-1} \matrixrepresentation{\vector{a}}.
  \end{equation*}
  Por outro lado,
  o fato de que $AA^{-1} = I$ implica que
  $\vector{v} = A^{-1} \matrixrepresentation{\vector{a}}$
  é solução do sistema, pois
  \begin{equation*}
    A \vector{v}
    =
    A A^{-1}
    \matrixrepresentation{\vector{a}}
    =
    \matrixrepresentation{\vector{a}}.
  \end{equation*}
  Repare como foi importante o fato de $A^{-1}$ ser a inversa de $A$
  tanto quando multiplicado à esquerda,
  quanto quando multiplicado à direita.

  Vamos fazer um caso mais concreto,
  com números.
  Considere o sistema linear
  \begin{equation}
    \label{equation:matrix:inverse:linear_system}
    \systemofequations
    {
      \phantom{1}x + 2y + 3z = 7
      \\
      \phantom{0x} + \phantom{1}y +4z = 8
      \\
      5x + 6y + \phantom{0z} = 9
    }.
  \end{equation}
  Em termos de matrizes,
  o sistema assume a forma
  \begin{equation}
    \label{equation:matrix:inverse:linear_system:matrix_notation}
    \matrixrepresentation{1 & 2 & 3 \\ 0 & 1 & 4 \\ 5 & 6 & 0}
    \matrixrepresentation{x \\ y \\ z}
    =
    \matrixrepresentation{7 \\ 8 \\ 9}.
  \end{equation}
  Queremos determinar $x,y,z \in \reals$.

  Seja
  \begin{equation*}
    A
    =
    \matrixrepresentation{1 & 2 & 3 \\ 0 & 1 & 4 \\ 5 & 6 & 0}.
  \end{equation*}
  A inversa de $A$ é
  \begin{equation*}
    A^{-1}
    =
    \matrixrepresentation{-24 & 18 & 5 \\ 20 & -15 & -4 \\ -5 & 4 & 1}.
  \end{equation*}
  Portanto,
  a \textbf{única solução} do sistema da \cref{equation:matrix:inverse:linear_system:matrix_notation}
  é dada por
  \begin{align*}
    \matrixrepresentation{\vector{v}}
    &=
    \matrixrepresentation{-24 & 18 & 5 \\ 20 & -15 & -4 \\ -5 & 4 & 1}
    \matrixrepresentation{7 \\ 8 \\ 9}
    \\
    &=
    7
    \matrixrepresentation{-24 \\ 20 \\ -5}
    +
    8
    \matrixrepresentation{18 \\ -15 \\ 4}
    +
    9
    \matrixrepresentation{5 \\ -4 \\ 1}
    \\
    &=
    \matrixrepresentation{-168 \\ 140 \\ -35}
    +
    \matrixrepresentation{144 \\ -120 \\ 32}
    +
    \matrixrepresentation{45 \\ -36 \\ 9}
    \\
    &=
    \matrixrepresentation{21 \\ -16 \\ 6}.
  \end{align*}
  Assim,
  descobrimos que $\vector{v} = (21, -16, 6)$.
  De fato,
  \begin{equation*}
    \systemofequations
    {
      \phantom{1\times}21 - 2\times 16 + 3 \times 6 = 7
      \\
      \phantom{0\times 21} - \phantom{1\times} 16 + 4 \times 6 = 8
      \\
      5 \times 21 - 6 \times 16 \phantom{+ 0 \times 6} = 9
    }.
  \end{equation*}

  Por vezes,
  nem mesmo da matriz nós precisamos.
  O sistema da \cref{equation:matrix:inverse:linear_system}
  pode ser pensado em termos de uma \emph{transformação linear}.
  Se $\function{T}{\reals^3}{\reals^3}$ é a \emph{transformação linear}
  representada pela matriz
  \begin{equation*}
    \matrixrepresentation{T}
    =
    \matrixrepresentation{1 & 2 & 3 \\ 0 & 1 & 4 \\ 5 & 6 & 0},
  \end{equation*}
  o sistema pode ser escrito na forma
  \begin{equation*}
    T(x,y,z)
    =
    (7,8,9).
  \end{equation*}
  Um exemplo será dado a seguir,
  quando falarmos sobre \emph{sistemas lineares homogêneos}.
