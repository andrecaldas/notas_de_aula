\section{Matrizes como Funções}
  \label{section:matrices_as_functions}

  Uma matriz $q \times p$
  \begin{equation*}
    A
    =
    \matrixrepresentation{| && | \\ \vector{a_1} & \cdots & \vector{a_p} \\ | && |}
  \end{equation*}
  pode ser vista como uma função
  \begin{equation*}
    \functionarray{T}{\reals^p}{\reals^q}{(x_1, \dotsc, x_p)}{x_1 \vector{a_1} + \dotsb + x_p \vector{a_p}}.
  \end{equation*}
  Funções de $\reals^p$ em $\reals^q$ que são desse tipo,
  são chamadas de \emph{transformações lineares}
  e serão estudadas com mais detalhes
  no \cref{chapter:linear_transforms_using_matrices}.
  As \emph{transformações lineares} são
  \emph{máquinas} de fazer combinações lineares.
  Você escolhe os coeficientes $x_1, \dotsc, x_p$,
  e $T$ faz a combinação linear dos vetores $\vector{a_1}, \dotsc, \vector{a_p}$
  usando esses coeficientes:
  \begin{equation*}
    T(x_1, \dotsc, x_p)
    =
    x_1 \vector{a_1} + \dotsb + x_p \vector{a_p}.
  \end{equation*}

  Assim,
  $T$ pode ser vista como um \emph{dispositivo}{\ldots}
  uma \emph{máquina}{\ldots}
  uma \emph{caixinha} com \emph{input} e \emph{output}.
  Se pensarmos nos vetores $\vector{x} \in \reals^p$
  e
  $T\vector{x} \in \reals^q$
  como matrizes coluna
  \begin{equation*}
    \matrixrepresentation{| \\ \vector{x} \\ |}
    =
    \matrixrepresentation{x_1 \\ \vdots \\ x_p}
    \quad\text{e}\quad
    \matrixrepresentation{| \\ T\vector{x} \\ |},
  \end{equation*}
  a transformação $T$ fica ainda mais parecida com as \emph{``caixinhas''}:
  \begin{equation}
    \label{equation:matrix_bY_column_is_Tv}
    \matrixrepresentation{| \\ T \vector{x} \\ |}
    =
    \matrixrepresentation{| && | \\ \vector{a_1} & \cdots & \vector{a_q} \\ | && |}
    \matrixrepresentation{x_1 \\ \vdots \\ x_p}.
  \end{equation}
  Do lado direito entra o vetor $(x_1, \dotsc, x_p)$,
  e do lado esquerdo sai o resultado:
  $T(x_1, \dotsc, x_p)$.
  % TODO: Fazer um desenho.

  De fato,
  quando definimos o produto de uma matriz por uma matriz coluna
  na \cref{subsection:product:matrix_by_column},
  o fizemos de modo que a \cref{equation:matrix_bY_column_is_Tv}
  seja verdadeira.

  \nftikzpic{01/02/figs/matrix_as_a_function.tikz}

  \begin{example}
    A matriz da tabela nutricional do \cref{example:nutrition_facts:table}
    pode ser usada para construir um dispositivo{\ldots}
    um \emph{APP}!!!
    Veja o projeto do \emph{app} na \cref{figure:nutrition_app}.

    Você coloca a quantidade de batatas e cenouras
    e o \emph{App da Nutrição} mostra para você a quantidade de
    nutrientes X, Y e Z.

    \nftikzpic[figure:nutrition_app]
        {01/02/figs/nutrition_app.tikz}

    Em python, por exemplo, o nosso \emph{app} poderia ser assim{\ldots}
    (se você não tem interesse em programação, apenas ignore!)

    Os cálculos (produto de matrizes) são feitos nas linhas 41--43.
    Lá está o produto de matrizes
    \begin{equation*}
      \matrixrepresentation{x \\ y \\ z}
      =
      \matrixrepresentation{3 & 15 \\ 10 & 7 \\ 8 & 11}
      \matrixrepresentation{p \\ c}
      =
      p
      \matrixrepresentation{3 \\ 10 \\ 8}
      +
      c
      \matrixrepresentation{15 \\ 7 \\ 11}.
    \end{equation*}

    \pythonfile[highlightlines=41-43]{01/02/02/code/nutrition_app.py}
  \end{example}
 

  \begin{intriguing question}
    \label{question:has_matrix_representation}
    Dada uma função
    $\function{T}{\reals^p}{\reals^q}$,
    existe um critério simples para saber se esta função
    pode ser representada por uma matriz $q \times p$?
  \end{intriguing question}

  Suponha que por algum motivo saibamos que a função
  $\function{T}{\reals^p}{\reals^q}$
  pode ser representada por uma matriz.
  Nesse caso,
  chamamos $T$ de \emph{transformação linear}.
  Como podemos determinar a matriz correspondente à transformação $T$?

  Repare que dada a matriz
  $A = \matrixrepresentation{\vector{a_1} \cdots \vector{a_p}}$,
  a primeira coluna de $A$ é
  \begin{equation*}
    \matrixrepresentation{| \\ \vector{a_1} \\ |}
    =
    \matrixrepresentation{| & | && | \\ \vector{a_1} & \vector{a_2} &\cdots & \vector{a_p} \\ | & | && |}
    \matrixrepresentation{1 \\ 0 \\ \vdots \\ 0},
  \end{equation*}
  o produto de $A$
  pelo primeiro vetor da base canônica:
  $\matrixrepresentation{\vector{e_1}}$.
  De modo geral,
  obtemos a $j$-ésima coluna de $A$
  quando aplicamos em $A$ o vetor $\vector{e_j}$:
  \begin{equation*}
    \matrixrepresentation{| \\ \vector{a_j} \\ |}
    =
    A
    \matrixrepresentation{| \\ \vector{e_j} \\ |}.
  \end{equation*}

  Assim,
  a matriz correspondente à transformação $T$ é dada por
  \begin{equation*}
    \matrixrepresentation{T}
    =
    \matrixrepresentation{| && | \\ T\vector{e_1} & \hdots & T\vector{e_p} \\ | && |}.
  \end{equation*}
  Em outras palavras,
  é a matriz cuja $j$-ésima coluna é dada pelo vetor
  $T\vector{e_j}$.
  Dessa forma,
  sabendo que $T$ é uma \emph{transformação linear},
  \textbf{é \emph{``fácil''} determinar a matriz correspondente}
  $\matrixrepresentation{T}$:
  é a matriz cuja primeira coluna
  é dada por $T\vector{e_1}$,
  a segunda é dada por $T\vector{e_2}$,
  etc.

  \begin{example}
    \label{example:table_composition:soup}
    Suponha agora
    que lá na cidade de \emph{fimdomundo}
    tem uma loja que vende dois tipos de sopa.
    A sopa de verão, com mais cenoura
    (para ajudar no bronzeado),
    e a sopa de inverno, com mais batata
    (para reserva de lipídios, para proteção contra o frio).
    Do \cref{example:potato_and_carrot_as_vectors},
    sabemos a quantidade de nutrientes em cada grama de batata e cada grama de cenoura.
    A quantidade de batata e cenoura por tijela de sopa
    também pode ser representada em uma tabela.

    \begin{center}
      \begin{tabular}{lcc}
        \firsthline
        & \textbf{verão} & \textbf{inverno}
        \\\hline
        \textbf{batata}  & 10 & 20
        \\
        \textbf{cenoura} & 30 & 15
        \\\lasthline
      \end{tabular}
    \end{center}

    Ou então,
    uma matriz:
    \begin{equation*}
      S
      =
      \matrixrepresentation{10 & 20 \\ 30 & 15}.
    \end{equation*}
    Podemos encarar $S$ como uma função de $\reals^2$ em $\reals^2$,
    que nos diz a quantidade de batatas e cenouras correspondente
    a determinadas quantidade de sopas de verão e inverno.
    Agora,
    se você quisesse saber a quantidade total de cada nutriente
    nas sopas que você comprou,
    você pode abrir o \emph{soup app},
    ver a quantidade de batatas e cenouras,
    copiar--e--colar o resultado no \emph{nutri app}
    para saber a quantidade de nutrientes.

    \nftikzpic{01/02/figs/soup_app.tikz}

    Assumimos que o cliente não está interessado nas quantidades
    de batata e cenoura.
    O cliente tem sim,
    interesse em saber as quantidades dos nutrientes X, Y e Z.
    O aplicativo, convenhamos, não é lá muito inteligente.

    Da mesma maneira,
    talvez o proprietário do restaurante não devesse colocar duas tabelas,
    uma com as quantidades de ingredientes por sopa
    e outra com as quantidades de nutrientes por ingrediente.
    Faria muito mais sentido,
    uma tabela com as quantidades de nutrientes por sopa!
    Precisamos saber como podemos mesclar as tabelas
    \begin{center}
      \begin{tabular}{lcc}
        \firsthline
        & \textbf{verão} & \textbf{inverno}
        \\\hline
        \textbf{batata}  & 10 & 20
        \\
        \textbf{cenoura} & 30 & 15
        \\\lasthline
      \end{tabular}
      \hspace{.5cm}
      \begin{tabular}{lcc}
        \firsthline
        & \textbf{batata} & \textbf{cenoura}
        \\\hline
        \textbf{nutriente X} & 3 & 15
        \\
        \textbf{nutriente Y} & 10 & 7
        \\
        \textbf{nutriente Z} & 5 & 12
        \\\lasthline
      \end{tabular}
    \end{center}
    para formar a seguinte tabela:
    \begin{center}
      \begin{tabular}{lcc}
        \firsthline
        & \textbf{verão} & \textbf{inverno}
        \\\hline
        \textbf{nutriente X} & $v_x$ & $i_x$
        \\
        \textbf{nutriente Y} & $v_y$ & $i_y$
        \\
        \textbf{nutriente Z} & $v_z$ & $i_z$
        \\\lasthline
      \end{tabular}
    \end{center}

    A primeira coluna da tabela são
    as quantidades de nutrientes na \emph{sopa de verão}.
    Basta calcularmos quantos nutrientes tem em 10 gramas de batata
    e 30 gramas de cenoura.
    Ou seja,
    basta aplicar, em $N$ (matriz do \cref{example:potato_and_carrot_as_vectors}),
    a primeira coluna da matriz $S$:
    \begin{equation*}
      \matrixrepresentation{v_x \\ v_y \\ v_z}
      =
      \matrixrepresentation{3 & 15 \\ 10 & 7 \\ 5 & 12}
      \matrixrepresentation{10 \\ 30}
      =
      10
      \matrixrepresentation{3 \\ 10 \\ 5}
      +
      30
      \matrixrepresentation{15 \\ 7 \\ 12}.
    \end{equation*}
    O mesmo vale para a segunda coluna.
    O resultado:
    \begin{center}
      \begin{tabular}{lcc}
        \firsthline
        & \textbf{verão} & \textbf{inverno}
        \\\hline
        \textbf{nutriente X} & 480 & 285
        \\
        \textbf{nutriente Y} & 310 & 305
        \\
        \textbf{nutriente Z} & 410 & 280
        \\\lasthline
      \end{tabular}
    \end{center}
  \end{example}

  No \cref{example:table_composition:soup},
  vimos que o produto $NS$ nada mais é do que a tabela que representa
  o \emph{dispositivo} resultante de se concatenar os \emph{dispositivos} $S$ e $N$.
  O produto é a \emph{composição} das duas tabelas.
  Uma pergunta que precisa ser respondida é:

  \begin{intriguing question}
    \label{question:composition_of_matrix_is_matrix}
    Considere as transformações
    $T_N$ e $T_S$ correspondentes às matrizes $N$ e $S$.
    Será que $T_N \circ T_S$ é sempre uma função que pode ser representada por uma matriz?
    Em outras palavras,
    quando $T_1$ e $T_2$ são transformações lineares que podem ser compostas,
    será que $T_1 \circ T_2$ também é uma transformação linear?
  \end{intriguing question}

  \begin{example}
    A função
    \begin{equation*}
      \functionarray{f}{\reals^2}{\reals^2}{(x,y)}{(x+2, x+y)}
    \end{equation*}
    não é uma \emph{transformação linear}.
    Ou seja,
    não pode ser representada por uma matriz $2 \times 2$,
    da maneira que estamos fazendo.
    Para qualquer matriz $M$ de tamanho $2 \times 2$,
    é verdade que
    \begin{equation*}
      M
      \matrixrepresentation{0 \\ 0}
      =
      \matrixrepresentation{0 \\ 0}.
    \end{equation*}
    Ou seja,
    \begin{equation*}
      T_M(0,0)
      =
      (0,0)
      \neq
      (2,0)
      =
      f(0,0).
    \end{equation*}
    De modo geral,
    toda \emph{transformação linear} $f$ tem que satisfazer
    $f(\vector{0}) = \vector{0}$.
  \end{example}

  \begin{example}
    A função
    \begin{equation*}
      \functionarray{f}{\reals^2}{\reals^2}{(x,y)}{(xy, x+y)}
    \end{equation*}
    não pode ser representada por uma matriz.

    Note que toda matriz $M$ de tamanho $2 \times 2$ satisfaz o seguinte:
    \begin{equation*}
      M
      \matrixrepresentation{2 \\ 2}
      =
      M
      \left(
        2
        \matrixrepresentation{1 \\ 1}
      \right)
      =
      2
      M
      \matrixrepresentation{1 \\ 1}.
    \end{equation*}
    E portanto,
    \begin{equation*}
      T_M(2,2)
      =
      2T_M(1,1).
    \end{equation*}
    No entanto,
    \begin{equation*}
      f(2,2)
      =
      (4,4)
      \neq
      2
      (1,2)
      =
      2
      f(1,1).
    \end{equation*}
  \end{example}

  {\input{01/02/02/01_matrix_product_as_function_composition}}
  {\input{01/02/02/02_inverse_matrix_as_a_function}}
