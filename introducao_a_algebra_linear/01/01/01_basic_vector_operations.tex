\section{Operações Básicas em $\reals^n$}

  Assim como com as batatas e cenouras
  no \cref{example:potato_and_carrot_as_vectors},
  ou a soma dos deslocamentos
  no \cref{example:cities_in_a_map},
  dados $\vector{a}, \vector{b} \in \reals^n$,
  a \emph{soma} de dois vetores $\vector{a}$ e $\vector{b}$
  é o vetor $\vector{c}$ dado por
  \begin{equation*}
    \vector{c}
    =
    (a_1 + b_1, \dotsc, a_n + b_n).
  \end{equation*}
  Ou seja,
  para calcular a soma de dois vetores,
  fazemos o cálculo coordenada a coordenada.
  Escrevemos
  $\vector{c} = \vector{a} + \vector{b}$.

  A soma de vetores é evidentemente comutativa e associativa:
  \begin{align*}
    \vector{a} + \vector{b}
    &=
    \vector{b} + \vector{a}
    \\
    (\vector{a} + \vector{b}) + \vector{c}
    &=
    \vector{a} + (\vector{b} + \vector{c}).
  \end{align*}

  Chamamos os elementos de $\reals^n$ de \emph{vetores}
  e os elementos de $\reals$ de \emph{escalares}.
  Dado um número $\alpha \in \reals$ e $\vector{a} \in \reals^n$,
  o \emph{produto} de $\vector{a}$ pelo \emph{escalar} $\alpha$
  é o vetor
  \begin{equation*}
    \alpha \vector{a}
    =
    (\alpha a_1, \dotsc, \alpha a_n).
  \end{equation*}
  Tanto a \emph{soma} quanto o \emph{produto por escalar}
  são feitos \emph{termo a termo}.
  Além das propriedades já mencionadas,
  a \emph{soma} e o \emph{produto por escalar} também satisfazem:
  \begin{enumerate}
    \item
      $0 \vector{a} = \vector{0}$,
      onde
      $\vector{0} = (0, \dotsc, 0)$.

    \item
      $1 \vector{a} = \vector{a}$.

    \item
      $(\alpha \beta) \vector{a} = \alpha (\beta \vector{a})$.

    \item
      $(\alpha + \beta) \vector{a} = \alpha \vector{a} + \beta \vector{a}$.

    \item
      $\alpha (\vector{a} + \vector{b}) = \alpha \vector{a} + \alpha \vector{b}$.
  \end{enumerate}
  O leitor certamente não terá dificuldades
  em interpretar essas propriedades
  em termos de batatas e cenouras!

  A diferença entre dois vetores
  \begin{equation*}
    \vector{c}
    =
    \vector{a}
    -
    \vector{b}
  \end{equation*}
  é o vetor $\vector{c}$ tal que
  \begin{equation*}
    \vector{b}
    =
    \vector{a}
    +
    \vector{c}.
  \end{equation*}
  Geometricamente,
  fica claro que $\vector{c}$ é o vetor que sai
  da pontinha de $\vector{b}$ e vai até a pontinha de $\vector{a}$.
  Como a operação de soma é feita coordenada a coordenada,
  a diferença também é.

  \nftikzpic[vector_difference]
    {01/01/figs/vector_difference.tikz}

  O conjunto $\reals^n$,
  juntamente com as operações de soma e produto por escalar
  formam o que chamamos de \emph{espaço vetorial}.

  \begin{definition}
    \label{definition:vector_space}
    \underline{Neste curso},
    um \emph{espaço vetorial}
    é um subconjunto não vazio $V \subset \reals^n$
    (para algum $n \in \naturals$),
    onde é possível restringir as operações de
    \emph{soma} e \emph{produto por escalar}.
    Ou seja,
    \begin{gather*}
      \vector{a},\vector{b} \in V
      \Rightarrow
      \vector{a} + \vector{b} \in V
      \\
      \alpha \in \reals
      \text{ e }
      \vector{a} \in V
      \Rightarrow
      \alpha \vector{a} \in V.
    \end{gather*}

    Quando $V \subset W$ são espaços vetoriais,
    dizemos que $V$ é subespaço de $W$.
    Note que as operações de \emph{soma}
    e \emph{produto por escalar} de $V$ são a restrição a $V$
    das operações de $W$.
  \end{definition}

  O fato de um \emph{espaço vetorial} $V$ não ser vazio
  significa que $\vector{0} \in V$,
  pois
  \begin{equation*}
    \vector{v} \in V
    \Rightarrow
    \vector{0} = 0\vector{v} \in V.
  \end{equation*}

  \begin{example}
    O \emph{plano $xy$}
    \begin{equation*}
      P
      =
      \setsuchthat{(x,y,0)}{x,y \in \reals}
    \end{equation*}
    é um \emph{espaço vetorial}.
    Um \emph{subespaço} de $\reals^3$.
  \end{example}

  \begin{obs}
    \label{obs:vector_space:general_definition}
    A definição de espaço vetorial
    é mais geral do que a que apresentamos na \cref{definition:vector_space}.
    O que é importante é ter um conjunto não vazio $V$
    onde estão definidas operações de \emph{soma} e \emph{produto por escalar}
    que tenham propriedades semelhantes às de $\reals^n$.
  \end{obs}
