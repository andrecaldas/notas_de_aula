\subsection{Reta}

  Provavelmente o leitor já conhece a \emph{equação da reta}
  em alguma forma.
  Vamos primeiramente discutir várias maneiras
  \textbf{horríveis} de falar sobre a \emph{reta}.

  Uma reta pode ser especificada como um conjunto de pontos
  \begin{equation*}
    r
    =
    \setsuchthat{(x,y) \in \reals^2}{y = a x + b},
  \end{equation*}
  onde $a, b \in \reals$ são constantes.
  A fórmula $y = ax + b$ nos permite algumas interpretações geométricas:
  $a$ é o coeficiente angular da reta,
  e $b$ é o ponto que a reta corta o eixo das ordenadas.

  \nftikzpic{01/01/03/figs/straight_line_as_a_graph.tikz}


  Mas espere!
  Existem retas que não cortam o eixo das ordenadas.
  Existe também o próprio eixo, que é uma reta.
  As retas $x = a$ não estão representadas,
  pois não são o gráfico de uma função da forma
  $f(x) = ax + b$.

  \nftikzpic{01/01/03/figs/straight_line_x_constant.tikz}


  Uma maneira melhor, seria
  \begin{equation}
    r
    =
    \setsuchthat{(x,y) \in \reals^2}{a x + b y = c},
  \end{equation}
  onde $a, b, c \in \reals$ são constantes.
  Vamos retornar a essa equação,
  na \cref{section:inner_product:geometric_aplications},
  depois que falarmos de produto interno.

  Outra maneira (\emph{horrível}) de especificar uma reta,
  é utilizando um parâmetro $t$ e várias funções de $\reals$ em $\reals$:
  \begin{align*}
    x(t)
    &=
    a t + p
    \\
    y(t)
    &=
    b t + q,
  \end{align*}
  onde $a, b, p, q \in \reals$ são constantes.
  Parece bastante claro que o uso excessivo de coordenadas
  torna a geometria difícil de ser compreendida.
  Repare que o sistema de equações que determina os pontos da reta
  a partir do parâmetro $t$ pode ser escrita na forma vetorial.
  \begin{equation*}
    r(t)
    =
    P
    +
    t\vector{v},
  \end{equation*}
  onde $P = (p,q)$ e $\vector{v} = (a,b) \neq (0,0)$.
  É como se no \emph{``instante''} $t = 0$,
  estivéssemos no ponto $P = (p,q)$,
  nos movendo com velocidade (constante) $\vector{v} = (a,b)$.
  Ou seja,
  a cada unidade de tempo,
  nos deslocamos na direção e no sentido do vetor $\vector{v}$,
  com velocidade igual a $\norm{\vector{v}}$.

  \nftikzpic{01/01/03/figs/straight_line_with_velocity.tikz}


  Repare que ao utilizarmos a linguagem vetorial,
  não precisamos mais do sistema de coordenadas!
  Em coordenadas,
  a equação ficaria
  \begin{equation*}
    r(t)
    =
    (p,q)
    +
    t (a,b).
  \end{equation*}
  O que equivale a
  \begin{align*}
    x(t) &= p + at
    \\
    y(t) &= q + bt.
  \end{align*}
  A equação vetorial continuará valendo,
  mesmo quando não estivermos restritos ao plano.
  Em um espaço vetorial $V$ qualquer,
  uma reta que passa pelo ponto $P \in V$,
  com direção igual à do vetor não nulo $\vector{v} \in V$,
  é a o conjunto
  \begin{equation*}
    r
    =
    \setsuchthat{P + t \vector{v}}{t \in \reals}.
  \end{equation*}

  Se $A, B \in \reals^2$ são pontos distintos do plano,
  então,
  a reta que passa em $A$ e $B$ pode ser parametrizada
  usando-se $P = A$ e $\vector{v} = (B - A)$.
  Mas existem outras parametrizações que também funcionam.
  Por exemplo,
  \begin{align*}
    r(t) &= B + t^3(A - B),
    \\
    r(t) &= A + t(B - A),
    \\
    r(t) &= 2B - A + 5t(B - A),
    \\
    r(t) &= \hdots
  \end{align*}

  \nftikzpic{01/01/03/figs/straight_line_through_ab.tikz}


  Pra ser rigoroso,
  $r(t)$ é uma função de $\reals$ em $\reals^2$ que leva
  cada $t \in \reals$ em um ponto da reta.
  \begin{equation*}
    \functionarray{r}{\reals}{\reals^2}{t}{P + t\vector{v}}.
  \end{equation*}
  Segmentos de reta e semi retas podem ser especificados
  restringindo o domínio de definição.
  Por exemplo,
  o segmento de reta que liga os pontos distintos $A, B \in \reals^2$
  pode ser parametrizado por
  \begin{equation*}
    \functionarray{r}{[0,1]}{\reals^2}{t}{A + t(B-A)}.
  \end{equation*}
  Que é quase a mesma coisa que já haviamos escrito para a reta.
  Mas agora,
  a função $r$ está definida apenas para $t \in [0,1]$.

  \nftikzpic{01/01/03/figs/segment_from_a_to_b.tikz}
