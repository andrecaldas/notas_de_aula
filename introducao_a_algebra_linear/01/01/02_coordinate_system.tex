\section{Sistemas de Coordenadas}

  Vetores de $\reals^2$ são simplesmente pares de números reais
  que podem ser somados (e subtraídos),
  e multiplicados por um número real (escalar).
  Quando falamos de \emph{pontos e vetores no plano},
  a princípio,
  não estamos falando de $\reals^2$.
  O ponto é esse \emph{``pontinho''} que a gente desenha.

  \nftikzpic{01/01/figs/a_dot.tikz}

  O vetor é essa \emph{``setinha''} que liga dois pontos.

  \nftikzpic{01/01/figs/a_vector_from_a_to_b.tikz}

  Se $A$ e $B$ são pontos e $\vector{v}$ é um vetor,
  faz sentido falar do ponto $A + \vector{v}$,
  e faz sentido falar do vetor $B - A$.
  Mas não faz muito sentido falar de $A + B$.
  A princípio,
  pontos e vetores são coisas diferentes,
  de natureza diferente,
  e muitas vezes é melhor deixá-los assim.
  Mas se quisermos,
  podemos escolher um \emph{ponto base} $O$,
  e então identificar os pontos do plano com os vetores do plano.
  Se falarmos do ponto (correspondente ao vetor) $\vector{v}$,
  na verdade,
  estamos falando do ponto $O + \vector{v}$.
  E se falarmos do vetor (correspondente ao ponto) $A$,
  na verdade,
  estamos nos referindo ao vetor $A - O$.

  \nftikzpic{01/01/figs/identify_dots_and_vectors.tikz}

  Com essa interpretação geométrica feita até agora,
  não faz muito sentido somar um vetor $B - A$,
  que sai de $A$ e vai pra $B$,
  com um vetor $D - C$,
  que sai de um outro ponto $C \neq B$,
  e vai até $D$!
  
  \nftikzpic{01/01/figs/summing_with_different_base_points.tikz}

  Para evitar esse problema,
  quando duas \emph{setas}
  forem paralelas,
  tiverem o mesmo tamanho
  e apontarem no mesmo sentido,
  vamos dizer que são o mesmo \emph{vetor}.

  Tudo o que fizemos para pontos no plano,
  poderia ter sido feito para pontos no espaço.
  Estamos então,
  com duas noções diferentes de \emph{vetores}.
  Se pensarmos em $\reals^2$
  ---
  ou de modo mais geral,
  $\reals^n$
  ---
  temos pares de números,
  com operação de soma e produto por escalar
  feitos \emph{coordenada a coordenada}.
  Se pensarmos no plano
  ---
  ou no espaço
  ---
  temos \emph{``pontinhos''} e \emph{``setinhas''}.
  O coneceito que vai fazer a ligação entre o plano e $\reals^2$,
  é o \emph{sistema de coordenadas}.
  O \emph{eixo coordenado} serve para especificarmos um vetor $\vector{v}$
  em termos de suas \emph{coordenadas}:
  $\vector{v} = (v_1, v_2)$.

  \nftikzpic{01/01/figs/axis.tikz}


  Com a interpretação geométrica usando \emph{``pontos''} e \emph{``setas''},
  mesmo sem eixos coordenados,
  podíamos falar de adição e produto por escalar.

  \nftikzpic{01/01/figs/two_consecutive_displacements.tikz}

  O vetor $\vector{v}$,
  saindo do ponto $A$ com coordenadas $(a_1, a_2)$
  e indo para o ponto $B$ de coordenadas $(b_1, b_2)$
  será identificado com o vetor $(b_1 - a_1, b_2 - a_2) \in \reals^2$.
  Usando essa identificação com $\reals^2$,
  fica mais fácil falar de soma de vetores do plano
  sem nos preocuparmos com as tais \emph{setinhas}.

  Temos então,
  a princípio,
  duas maneiras de somar vetores.
  Temos a maneira geométrica,
  utilizando \emph{setas}.
  E temos a maneira algébrica,
  fazendo a soma coordenada a coordenada.
  A figura \ref{figure:vector:geometric_and_algebraic_equivalence}
  deve ser suficiente para convencer o leitor de que
  ambas as maneiras são equivalentes.

  \nftikzpic[figure:vector:geometric_and_algebraic_equivalence]
    {01/01/figs/geometric_and_algebraic_equivalence.tikz}

  Assim,
  para somar dois vetores,
  não precisamos que a ponta de um esteja no bumbum do outro.
  O mais comum é desenhar todos os vetores
  partindo de um mesmo ponto.
  Geometricamente,
  a soma é feita como mostrado
  na figura \ref{figure:vector:addition_with_a_parallelogram}.

  \nftikzpic[figure:vector:addition_with_a_parallelogram]
    {01/01/figs/addition_with_a_parallelogram.tikz}


  Sabendo que os pontos de vista \emph{geométrico} e \emph{algébrico}
  são equivalentes, podemos escolher o que for mais conveniente.
  A seguir,
  vejamos um exemplo de um problema geométrico
  que é mais fácil resolvido com álgebra.
  E depois,
  um problema algébrico
  que é mais facilmente resolvido com o ponto de vista geométrico.

  \begin{example}[adição é comutativa]
    Do ponto de vista geométrico,
    como podemos mostrar que a adição de vetores é \emph{comutativa}?
    Será que a situação
    da figura \ref{figure:vector:can_addition_not_commute}
    nunca ocorre?

    \nftikzpic[figure:vector:can_addition_not_commute]
      {01/01/figs/can_addition_not_commute.tikz}

    Por outro lado,
    do ponto de vista algébrico
    é muito fácil ver que adição de vetores de $\reals^n$ é comutativa:
    \begin{align*}
      \vector{v} + \vector{w}
      &=
      (v_1, \dotsc, v_n)
      +
      (w_1, \dotsc, w_n)
      \\
      &=
      (v_1 + w_1, \dotsc, v_n + w_n)
      \\
      &=
      (w_1 + v_1, \dotsc, w_n + v_n)
      \\
      &=
      (w_1, \dotsc, w_n)
      +
      (v_1, \dotsc, v_n)
      =
      \vector{w} + \vector{v}.
    \end{align*}
    Ou seja,
    sabendo da equivalência entre os pontos de vista
    \emph{geométrico} e \emph{algébrico},
    concluímos que, de fato,
    a situação da figura \ref{figure:vector:can_addition_not_commute} nunca ocorre!

    \nftikzpic{01/01/figs/addition_is_commutative.tikz}
  \end{example}

  O exemplo a seguir mostra um problema cuja formulação é algébrica,
  e cuja solução é facilmente compreendida utilizando um ponto de vista geométrico.

  \begin{example}
    Por algum motivo,
    estamos interessados em investigar combinações de dois vetores
    $\vector{v}$ e $\vector{w}$ que sejam do tipo
    \begin{equation*}
      \alpha \vector{v}
      +
      \beta \vector{w},
    \end{equation*}
    onde $\alpha \geq 0$ e $\beta \geq 0$.
    Geometricamente é bastante fácil ver
    quais vetores podem ou não podem serem escritos dessa maneira.
    Veja a figura \ref{figure:linear_combination:cone}.

    \nftikzpic[figure:linear_combination:cone]
      {01/01/figs/cone.tikz}
  \end{example}

  Agora temos intuição \emph{geométrica} (desenho)
  e intuição \emph{algébrica} (cálculos) sobre um mesmo conceito.
  A intuição \emph{geométrica} não é melhor que a \emph{algébrica},
  ou vice-versa.
  É importante conhecermos as duas e lançarmos mão da que for mais adequada
  ao problema sendo tratado.

  O que tratamos aqui,
  vale também para $\reals^3$.
  Com um pouco de esforço,
  podemos extrapolar essas ideias para $\reals^4$,
  ou mesmo para $\reals^n$.
  Cada pessoa gosta de enxergar vetores em $\reals^n$ de uma maneira diferente.
  Mas quando usamos apenas \emph{álgebra},
  não precisamos nos preocupar com isso.
  Afinal de contas,
  estamos falando simplesmente de $n$-uplas de números.

  Muita gente gosta de pensar na quarta dimensão como se fosse o \emph{``tempo''}.
  O leitor pode pensar no tempo, se preferir.
  Mas lembre-se que $\reals^n$ é simplesmente uma $n$-upla ordenada
  com operações de \emph{soma} e \emph{produto por escalar} definidos.
  E,
  como no \cref{example:potato_and_carrot_as_vectors},
  é bem possível que a quarta dimensão seja a \emph{``couve''}!!!
  Quando o número de dimensões aumenta muito,
  a geometria continua nos fornecendo ideias e intuição.
  Mas às vezes, o mais fácil é pensar
  nos elementos de $\reals^4$ ou $\reals^{23}$
  como sendo sequências de $4$ ou $23$ números, respectivamente.
