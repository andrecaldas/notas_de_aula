\section{Composição de funções}
  \label{section:function_composition}

  É importante que o leitor tenha em mente,
  que uma função não é uma fórmula,
  mas uma correspondência.
  Por vezes,
  podemos expressar essa correspondência usando uma fórmula.

  A rigor,
  não deveríamos dizer que $f(x)$ é uma função.
  A função é $f$.
  Enquanto que $f(x)$ é o valor que $f$ toma em $x$.
  Podemos imaginar uma função
  $\function{f}{A}{B}$
  como uma máquina,
  um dispositivo{\ldots}
  uma \emph{``caixinha''}!

  \nftikzpic[function_as_a_box]
    {00/02/figs/function_as_a_box.tikz}

  Um dipositivo que sempre que recebe uma entrada (input) $x$,
  emite uma resposta (output) $f(x)$.
  Se é,
  por exemplo,
  uma máquina onde você coloca uma fruta e sai suco dessa fruta,
  não podemos colocar uma bola de tênis!
  Os únicos \emph{inputs} permitidos são as frutas.
  Ou seja,
  o \emph{domínio} de $f$ é o conjunto das frutas.
  É bom também deixarmos claro quem é o contradomínio.
  Poderia ser o conjunto de todos os tipos de bebida,
  ou o conjunto de todos os tipos de líquido,
  ou simplesmente,
  o conjunto de todos os tipos de suco de fruta.

  \nftikzpic[fruit_juice_function]
    {00/02/figs/fruit_juice_function.tikz}

  Imagine uma função
  \begin{equation*}
    \function{g}{\text{SUCOS}}{\text{PICOLÉS}}.
  \end{equation*}
  Se ligássemos a saída de $f$ com a entrada de $g$,
  teríamos uma máquina
  \begin{equation*}
    \function{g \circ f}{\text{FRUTAS}}{\text{PICOLÉS}}.
  \end{equation*}
  Veja a \cref{fruit_to_popsicle}.

  \nftikzpic[fruit_to_popsicle]
    {00/02/figs/fruit_to_popsicle.tikz}

  De um modo geral,
  dadas duas funções
  $\function{f}{A}{B}$
  e
  $\function{g}{B}{C}$,
  definimos a composição $g \circ f$,
  como sendo a função de $A$ em $C$ que toma $x \in A$,
  e leva em $g(f(x))$.
  De modo mais rigoroso,
  \begin{equation*}
    \functionarray{g \circ f}{A}{C}{x}{g(f(x))}.
  \end{equation*}
  O nome da função é $g \circ f$.
  Usamos a notação $(g \circ f)(x)$ para aplicarmos
  $x \in A$ na função $g \circ f$.
  No final das contas,
  \begin{equation*}
    (g \circ f)(x)
    =
    g(f(x)).
  \end{equation*}


  Podemos complicar ainda mais,
  e pensar na composição de mais funções{\ldots}
  por exemplo, dadas as funções
  \begin{align*}
    &\function{h}{A}{B}
    \\
    &\function{g}{B}{C}
    \\
    &\function{f}{C}{D},
  \end{align*}
  qual é o significado de
  $f \circ g \circ h$?
  Veja a \cref{figure:triple_composition_at_once}.
  É como se ligássemos a saída de $h$ na entrada de $g$,
  e a saída de $g$ na entrada de $f$,
  para formarmos uma ``caixa''
  \begin{equation*}
    \functionarray{f \circ g \circ h}{A}{D}{x}{f\left(g\left(h\left(x\right)\right)\right)}.
  \end{equation*}

  \nftikzpic[figure:triple_composition_at_once]
    {00/02/figs/triple_composition_at_once.tikz}

  Por outro lado,
  $g \circ h$ é o nome da ``caixa'' que leva $x$ em $g(h(x))$.
  Vamos chamar essa ``caixa'' de $G = g \circ h$.
  A notação $f \circ (g \circ h)$ representa
  ---
  como ilustrado na \cref{figure:triple_composition_right_first}
  ---
  a composição de $f$ com a ``caixa'' $g \circ h$.
  Temos
  \begin{equation*}
    \functionarray{f \circ (g \circ h)}{A}{D}{x}{f\left((g \circ h)(x)\right)}.
  \end{equation*}

  Da mesma forma,
  se compusermos a caixa $f \circ g$ com $h$,
  temos
  \begin{equation*}
    \functionarray{(f \circ g) \circ h}{A}{D}{x}{(f \circ g)\left(h(x)\right)}.
  \end{equation*}
  Mas no final das contas,
  é tudo a mesma coisa!
  \begin{align*}
    (f \circ g \circ h) (x)
    &=
    f\left(g\left(h(x)\right)\right)
    \\
    (f \circ (g \circ h)) (x)
    =
    f\left( (g \circ h)(x) \right)
    &=
    f\left(g\left(h(x)\right)\right)
    \\
    ((f \circ g) \circ h) (x)
    =
    (f \circ g)\left( h(x) \right)
    &=
    f\left(g\left(h(x)\right)\right).
  \end{align*}

  \nftikzpic[figure:triple_composition_right_first]
    {00/02/figs/triple_composition_right_first.tikz}

  Com ou sem as ``caixas intermediárias'' $f \circ g$ e $g \circ h$,
  o resultado corresponde a tomar $x$,
  passar por $h$,
  depois por $g$
  e
  depois por $f$.
  Ou seja,
  a operação de \emph{composição} é \emph{associativa}:
  \begin{equation}
    \label{equation:function_composition:associativity}
    f \circ (g \circ h)
    =
    f \circ g \circ h
    =
    (f \circ g) \circ h.
  \end{equation}


  \begin{obs}
    \label{obs:function_composition:associativity}
    A composição de funções é \emph{associativa}:
    \begin{equation*}
      (f \circ g) \circ h
      =
      f \circ (g \circ h).
    \end{equation*}
    Mas a composição não é \emph{comutativa}!
    Ou seja,
    pode acontecer de $f \circ g$ ser diferente de $g \circ f$.
  \end{obs}


  \begin{example}
    Sejam
    \begin{align*}
      &\functionarray{f}{\reals}{\reals}{x}{\abs{x}}
      \\
      &\functionarray{g}{\reals}{\reals}{x}{-x}.
    \end{align*}
    Então,
    \begin{align*}
      &\functionarray{f \circ g}{\reals}{\reals}{x}{\abs{x}}
      \\
      &\functionarray{g \circ f}{\reals}{\reals}{x}{-\abs{x}}.
    \end{align*}
  \end{example}


  O leitor familiarizado com produto de matrizes
  deve lembrar que o produto de matrizes é associativo,
  mas não é comutativo.
  E pode ser que, dadas duas matrizes $A$ e $B$,
  o produto $AB$ esteja definido,
  mas o produto $BA$ não esteja.

  \begin{obs}
    Sejam
    $\function{f}{\reals^3}{\reals^2}$
    e
    $\function{g}{\reals^4}{\reals^3}$.
    Então,
    $f \circ g$ está definido,
    mas $g \circ f$ não está.
    O primeiro caso está definido porque
    a saída de $g$ (dimensão $3$) é igual
    à entrada de $f$.
    O segundo caso não está definido porque
    a saída de $f$ (dimensão $2$) NÃO é igual
    à entrada de $g$ (dimensão $4$).

    Da mesma forma,
    se $F$ é uma matriz $2 \times 3$
    e $G$ é uma matriz $3 \times 4$,
    então o produto $FG$ está definido porque
    o número de linhas de $G$ (dimensão $3$) é igual
    ao número de colunas de $F$.
    Mas $GF$ não está definido porque
    o número de linhas de $F$ (dimensão $2$) NÃO é igual
    ao número de colunas de $G$ (dimensão $4$).

    Ainda vamos falar sobre produto de matrizes no curso.
    Lembre-se desta semelhança entre o produto de matrizes
    e a composição de funções.
  \end{obs}
