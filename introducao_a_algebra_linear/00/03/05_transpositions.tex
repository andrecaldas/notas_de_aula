\section{Transposições}

  Uma permutação $\sigma$ que troca dois números distintos $i$ e $j$,
  e que deixa os demais fixos,
  é denotada por $\sigma = \transposition{i}{j}$
  e é chamada de \emph{permutação simples} ou \emph{transposição}.
  As permutações simples tem duas propriedades especialmente interessantes.

  \begin{lemma}
    \label{lemma:transposition_sign}
    Se $\sigma$ é simples, então
    \begin{equation*}
      \sign(\sigma)
      =
      -1.
    \end{equation*}
  \end{lemma}

  \begin{proof}
    Assumindo que $i < j$,
    basta notar
    ---
    observando a \cref{figure:graphical_representation:transposition}
    ---
    que as linhas saindo de $i$ e $j$ se cruzam uma vez (ímpar)
    e depois cada uma das duas cruza as retas $i+1$ até $j-1$ (par).
    Assim,
    o número de cruzamentos é ímpar.
    Ou seja,
    $\sign(\sigma) = -1$.

    \nftikzpic[figure:graphical_representation:transposition]
              {00/03/figs/graphical_representation_transposition.tikz}
  \end{proof}

  \begin{lemma}
    \label{lemma:permutation_is_product_of_transpositions}
    Toda permutação $\sigma$, de um conjunto finito $A$,
    pode ser escrita como composição de transposições.
  \end{lemma}

  \begin{obs}
    Normalmente,
    com respeito à permutação identidade
    ---
    que não permuta ninguém,
    mantendo todos os elementos no mesmo lugar
    ---
    entendemos que ela é um produto de \emph{zero} transposições.
  \end{obs}

  \begin{proof}
    Vamos mostrar por indução no número de elementos que não são mantidos fixos.
    Se $\sigma$ não permuta nenhum elemento,
    então o enunciado do lema é verdadeiro.

    Assumindo que o lema vale quando
    o número de elementos que não são mantidos fixos é menor que $n$,
    vamos mostrar que vale quando esse número é igual a $n$.
    Seja $j \in A$ um elemento que não é mantido fixo por $\sigma$.
    Faça $k = \sigma(j)$.
    Pela escolha de $j$,
    $k$ é diferente de $j$.
    Além disso,
    como $\sigma$ é injetiva,
    $k$ também não é mantido fixo.

    Seja $\tau = \transposition{j}{k}$ a transposição que permuta $j$ e $k$.
    Observe que a permutação
    $\gamma = \sigma \tau$
    mantém $k$ fixo.
    De fato,
    \begin{equation*}
      \gamma(k)
      =
      \sigma(\tau(k))
      =
      \sigma(j)
      =
      k.
    \end{equation*}
    Além disso,
    $\gamma$ mantém fixos todos os elementos que já eram mantidos fixos por $\sigma$.
    De fato,
    se $p$ é um tal elemento,
    \begin{equation*}
      \sigma(\tau(p))
      =
      \sigma(p)
      =
      p.
    \end{equation*}
    Aqui,
    a segunda igualdade se deve ao fato de $p$ ser mantido fixo por $\sigma$.
    E a primeira igualdade se deve ao fato de $p$ ser distinto de $j$ e $k$,
    já que $p$ é mantido fixo por $\sigma$, mas $j$ e $k$ não são.

    Pela hipótese de indução,
    $\gamma$ é um produto de transposições.
    Então,
    \begin{equation*}
      \sigma
      =
      \sigma \tau \tau
      =
      \gamma \tau
    \end{equation*}
    também é,
    pois $\tau$ é uma transposição.
  \end{proof}

  % TODO: exemplo e exercícios com casos particulares da demonstração.

  Agora,
  com os \cref{lemma:transposition_sign,lemma:permutation_is_product_of_transpositions},
  podemos interpretar o \emph{sinal} de uma permutação como sendo a paridade
  do número de transposições necessárias para escrevê-la.
  A permutação $\sigma$ pode ser escrita como o produto de um número \textbf{ímpar} de transposições
  exatamente quando $\sign(\sigma) = -1$.
  E pode ser escrita com um número \textbf{par} de transposições
  exatamente quando $\sign(\sigma) = +1$.

  \begin{corollary}
    Escolhido $n \in \nonzeronaturals$,
    existe uma única função
    \begin{equation*}
      \function{\sign}{\permutations{n}}{\set{-1,1}}
    \end{equation*}
    tal que
    \begin{align*}
      \sign(\sigma) = 1
      &\Leftrightarrow
      \text{$\sigma$ é o produto de um núemro \textbf{par} de transposições}
      \\
      \sign(\sigma) = -1
      &\Leftrightarrow
      \text{$\sigma$ é o produto de um núemro \textbf{ímpar} de transposições}.
    \end{align*}
  \end{corollary}

  \begin{proof}
    O \cref{lemma:permutation_is_product_of_transpositions}
    mostra que
    \begin{equation*}
      \sigma
      =
      \tau_1 \dotsb \tau_n,
    \end{equation*}
    onde todas as $\tau_j$ são transposições.
    Pelo \cref{lemma:transposition_sign},
    \begin{align*}
      \sign(\sigma)
      &=
      \sign(\tau_1 \dotsb \tau_n)
      \\
      &=
      \sign(\tau_1) \dotsb \sign(\tau_n)
      \\
      &=
      (-1)^n.
    \end{align*}
    Essa igualdade conclui a demonstração.
  \end{proof}

  A existência da função $\sign$ tem implicações intrigantes!

  \begin{example}[Identidade é par]
    A permutação \emph{identidade}
    \begin{equation*}
      \functionarray{\identity}{[n]}{[n]}{x}{x}
    \end{equation*}
    é o elemento de $\permutations{n}$ que ``não faz nada''.
    Se
    \begin{equation*}
      \identity
      =
      \tau_1 \dotsb \tau_k,
    \end{equation*}
    onde $\tau_1, \dotsc, \tau_k$ são transposições,
    então $k$ é necessariamente par.
  \end{example}
