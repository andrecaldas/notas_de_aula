\section{Escolhendo em $A^n$}

  No \cref{example:sum_power},
  vimos como o conjunto $A^n$ representa todas as escolhas possíveis
  de uma sequência de $n$ elementos de $A$.
  Vamos exercitar um outra maneira de expressar o conjunto $A^n$.

  Quando o conjunto tem $k$ elementos,
  podemos enumerá-los:
  \begin{equation*}
    A = \set{x_1, x_2, \dotsc, x_k}.
  \end{equation*}
  Enumerar os elementos de $A$ equivale a escolher
  uma \emph{bijeção}
  \begin{equation*}
    \function{p}{\set{1, \dotsc, k}}{A}.
  \end{equation*}

  \begin{example}
    \label{example:enumerate_a_set}
    Se
    \begin{equation*}
      A
      =
      \set{\elA, \elB, \elC},
    \end{equation*}
    uma enumeração de $A$ é dada pela seguinte tabela.
    \begin{center}
      \begin{tabular}{ccc}
        \toprule
        1 & 2 & 3
        \\\hline
        \elA & \elB & \elC
        \\\bottomrule
      \end{tabular}
    \end{center}
    Esta mesma enumeração é dada pela função
    \begin{equation*}
      \functionarray{p}{\set{1,2,3}}{A}{n}
                    {
                      \begin{cases}
                       \elA, &n = 1
                        \\
                       \elB, &n = 2
                        \\
                       \elC, &n = 3.
                      \end{cases}
                    }
    \end{equation*}
  \end{example}

  Assim,
  fixada uma enumeração,
  cada $a \in A^n$ corresponde a uma função
  \begin{equation*}
    \function{f}{\set{1, \dotsc, n}}{\set{1, \dotsc, k}}.
  \end{equation*}
  Para cada $f$, temos o elemento de $A^n$
  \begin{equation*}
    a^f
    =
    (x_{f(1)}, \dotsc, x_{f(n)}).
  \end{equation*}

  \begin{example}
    Vamos continuar o \cref{example:enumerate_a_set},
    onde o conjunto
    \begin{equation*}
      A
      =
      \set{\elA, \elB, \elC}
    \end{equation*}
    está enumerado pela função $p$,
    na ordem em que aparecem listados.

    Com respeito a $a = (\elB, \elC) \in A^2$,
    podemos dizer que
    \begin{enumerate}
      \item
        a entrada $a_1 = \elB$ corresponde ao \textbf{segundo} elemento de $A$; e
      \item
        a entrada $a_2 = \elC$ corresponde ao \textbf{terceiro} elemento de $A$.
    \end{enumerate}
    Dada a enumeração $p$,
    podemos pensar em $a \in A^2$ como equivalente à função
    \begin{equation*}
      \functionarray{f_a}{\set{1,2}}{\set{1,2,3}}{n}
                    {
                      \begin{cases}
                        2, &n = 1
                        \\
                        3, &n = 2.
                      \end{cases}
                    }
    \end{equation*}
    Já o elemento $b = (\elC, \elC) \in A^2$,
    corresponde à função constante
    \begin{equation*}
      \functionarray{f_b}{\set{1,2}}{\set{1,2,3}}{n}{3}.
    \end{equation*}
  \end{example}

  Para facilitar,
  vamos denotar por
  \begin{equation*}
    [n]
    =
    \set{1, \dotsc, n}
  \end{equation*}
  o conjunto dos números naturais de $1$ até $n$.

  \begin{example}
    \label{example:sum_power:indexes}
    Continuando o \cref{example:sum_power},
    ao invés de escrevermos
    \begin{equation*}
      (x + y)^n
      =
      \sum_{a \in \set{x,y}^n} a_1 \dotsb a_n,
    \end{equation*}
    podemos escrever
    \begin{equation*}
      (x_1 + x_2)^n
      =
      \sum_{\function{f}{[n]}{[2]}} x_{f(1)} \dotsb x_{f(n)},
    \end{equation*}
    onde a soma é feita sobre todas as funções $\function{f}{[n]}{[2]}$.
  \end{example}

  O objetivo desta seção é que o leitor se acostume com esse tipo de notação
  do \cref{example:sum_power:indexes}.

% TODO: transformar em exercício.
  %% \begin{example}
  %%   \label{example:multilinear:polynome}
  %%   Seja $P$ o cojunto dos polinômios em $x$.
  %%   Suponha que,
  %%   sabe-se lá por quais motivos,
  %%   a função
  %%   \begin{equation*}
  %%     \function{G}{P \times P}{\reals}
  %%   \end{equation*}
  %%   satisfaz as seguintes propriedades:
  %%   \begin{enumerate}
  %%     \item
  %%       $G(p(x) + q(x), r(x)) = G(p(x), r(x)) + G(q(x), r(x))$.

  %%     \item
  %%       $G(\alpha p(x), r(x)) = \alpha G(p(x), r(x))$.

  %%     \item
  %%       $G(p(x), r(x) + s(x)) = G(p(x), r(x)) + G(p(x), s(x))$.

  %%     \item
  %%       $G(p(x), \alpha r(x)) = \alpha G(p(x), r(x))$.

  %%     \item
  %%       $G(x^m, x^n) = m^4 - n^3$.
  %%   \end{enumerate}
  %%   Então,
  %%   por exemplo,
  %%   \begin{align*}
  %%     G(2 x^2, 5 x^3)
  %%     &=
  %%     2 G(x^2, 5 x^3)
  %%     \\
  %%     &=
  %%     10 G(x^2, x^3)
  %%     \\
  %%     &=
  %%     10 (2^4 - 3^3)
  %%     \\
  %%     &=
  %%     10 (16 - 27)
  %%     =
  %%     -110.
  %%   \end{align*}
  %%   Ou então,
  %%   \begin{align*}
  %%     G(2 x^2 + 3 x^3, 7 x + 5 x^3)
  %%     &=
  %%     G(2 x^2, 7 x + 5 x^3) + G(3 x^3, 7 x + 5 x^3)
  %%     \\
  %%     &=
  %%     G(2 x^2, 7 x) + G(2 x^2, 5 x^3)
  %%     +
  %%     G(3 x^3, 7 x) + G(3 x^3, 5 x^3)
  %%     \\
  %%     &=
  %%     14 G(x^2, x) + 10 G(x^2, x^3)
  %%     +
  %%     21 G(x^3, x) + 15 G(x^3, x^3)
  %%     \\
  %%     &=
  %%     \dotsb
  %%   \end{align*}

  %%   De um modo geral,
  %%   se
  %%   $p(x) = a_1 x^{k-1} + a_2 x^{k-2} + \dotsb + a_k x^0$
  %%   e
  %%   $q(x) = b_1 x^{k-1} + b_2 x^{k-2} + \dotsb + b_k x^0$,
  %%   onde $k$ é o máximo dos graus dos polinômios $p$ e $q$.
  %%   Então,
  %%   \begin{align*}
  %%     G(p(x), q(x))
  %%     &=
  %%     \sum_{\function{f}{[2]}{[k]}}
  %%     a_{f(1)}
  %%     b_{f(2)}
  %%     G(x^{(k-f(1))}, x^{(k-f(2))})
  %%     \\
  %%     &=
  %%     \sum_{\function{f}{[2]}{[k]}}
  %%     a_{f(1)}
  %%     b_{f(2)}
  %%     \left(
  %%       (k-f(1))^4 - (k-f(2))^3
  %%     \right).
  %%   \end{align*}
  %% \end{example}
