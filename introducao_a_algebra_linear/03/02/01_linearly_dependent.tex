\section{Excluindo Vetores Linearmente Dependentes}

  Se $V$ é um espaço vetorial,
  todo subconjunto $S \subset V$ gera um subespaço de $V$.
  Se $S$ é linearmente independente,
  então é base do espaço que gera.

  Dizer que $S$ é linearmente independente,
  é o mesmo que dizer que se retirarmos um vetor qualquer de $S$,
  o subespaço gerado será menor.
  E dizer que $S$ \textbf{não é} linearmente independente,
  é o mesmo que dizer que existe um vetor em $S$
  que pode ser retirado de $S$ sem modificar o subespaço gerado.
  Ou seja,
  $S$ é \emph{linearmente dependente} quando existe
  $\vector{v} \in S$ tal que
  \begin{equation*}
    \generatedsubspace{S \setminus \set{\vector{v}}}
    =
    \generatedsubspace{S}.
  \end{equation*}
  Veja a \cref{definition:linear_independence}.

  \begin{proposition}
    \label{proposition:extract_basis:finite_generator}
    Seja $V$ um espaço vetorial,
    $L \subset V$ um subconjunto linearmente independente
    e
    $G \subset V$ um conjunto gerador \textbf{finito}
    que contém $L$.
    Então
    existe uma base $B$ de $V$ tal que
    \begin{equation*}
      L
      \subset
      B
      \subset
      G.
    \end{equation*}
  \end{proposition}

  \begin{proof}
    Enquanto $G$ for linearmente dependente,
    podemos retirar um elemento de $G$ e construir um conjunto
    $G_1 \subsetneq G$ que gera o mesmo subespaço que $G$.
    Podemos ainda fazer mais,
    e garantir que o elemento retirado de $G$ não é um elemento de $L$.
    Assim,
    garantiremos que o gerador $G_1$ é tal que
    \begin{equation*}
      L
      \subset
      G_1
      \subsetneq
      G.
    \end{equation*}
    
    Como $G$ é finito,
    esse processo de retirada de vetores só pode ser repetido um número finito de vezes.
    Podemos então escolher
    $G_n \subset \dotsb \subset G_1 \subset G$
    por esse processo,
    de modo que para todo $j$,
    \begin{align*}
      L
      &\subset
      G_j
      \\
      \generatedsubspace{G_j}
      &=
      V,
    \end{align*}
    e de modo que não seja possível retirar mais nenhum elemento de $G_n$
    sem deixar de gerar $V$.

    Se não podemos retirar nenhum elemento de $G_n$ sem reduzir o espaço gerado,
    então $G_n$ é linearmente independente.
    Como é também gerador,
    concluímos que $G_n$ é base de $V$ que contém $L$.

    Só ficou faltando mostrar que de fato, na construção de $G_j$
    podemos retirar um elemento que não está em $L$.
    Isso fica como exercício:
    \cref{exercise:you_can_avoid_elements_in_a_linearly_independent_set}.
  \end{proof}

  O argumento
  da \cref{proposition:extract_basis:finite_generator}
  pode ser feito de modo mais elegante.
  Faremos isso
  na \cref{section:extending_linearly_independent_sets}.

  \begin{obs}
    Perceba que um conjunto linearmente independente $L$ pode ser vazio.
    \begin{equation*}
      \generatedsubspace{\emptyset}
      =
      \set{\vector{0}}.
    \end{equation*}
  \end{obs}
