\section{Estendendo Conjuntos Linearmente Independentes}
  \label{section:extending_linearly_independent_sets}

  Queremos começar com um conjunto linearmente independente
  e aumentá-lo de modo a construir uma base.

  \begin{lemma}
    \label{lemma:extending_a_linear_independent_set}
    Se $V$ é um espaço vetorial
    e $L \subset V$ é linearmente independente,
    então,
    para qualquer
    \begin{equation*}
      \vector{v}
      \in
      V \setminus \generatedsubspace{L},
    \end{equation*}
    $L \cup \set{\vector{v}}$ é linearmente independente e estritamente maior que $L$.
  \end{lemma}

  \begin{proof}
    Suponha que
    $M = L \cup \set{\vector{v}}$
    seja linearmente dependente.
    Então,
    existem vetores distintos
    $\vector{v_1}, \dotsc, \vector{v_k} \in M$
    e
    $\alpha_1, \dotsc, \alpha_n \in \reals$
    não nulos
    tais que
    \begin{equation*}
      \alpha_1 \vector{v_1}
      + \dotsb +
      \alpha_k \vector{v_k}
      =
      \vector{0}.
    \end{equation*}
    Como $L$ é linearmente independente,
    algum dos $\vector{v_j}$ tem que ser igual a $\vector{v}$.
    Reordenando os índices,
    vamos assumir que $\vector{v_1} = \vector{v}$.
    Mas isso significaria que
    \begin{equation*}
      \vector{v}
      =
      \frac{1}{\alpha_1}
      \left(
        \alpha_2 \vector{v_2}
        + \dotsb +
        \alpha_k \vector{v_k}
      \right)
      \in
      \generatedsubspace{L}.
    \end{equation*}
    Mas como $\vector{v}$ foi escolhido de modo que
    $\vector{v} \not \in \generatedsubspace{L}$,
    podemos concluir que
    $M$ não é linearmente dependente.
  \end{proof}

  Note que o vetor $\vector{v}$
  do \cref{lemma:extending_a_linear_independent_set}
  só existe quando $L$ não é gerador.
  Ou seja,
  quando $L$ não é uma base.
  O lema pode ser entendido como:
  \begin{quote}
    Todo conjunto linearmente independente que não é uma base
    pode ser ``aumentado'' de modo que o conjunto aumentado continue sendo
    linearmente independente.
  \end{quote}
  Ou então:
  \begin{quote}
    Se um conjunto linearmente independente $B$ não pode ser ``aumentado''
    presevando a independência linear,
    então $B$ é uma base.
  \end{quote}


  Agora,
  a prometida demonstração mais elegante
  da \cref{proposition:extract_basis:finite_generator}.

  \begin{proof}[(demonstração mais elegante da \cref{proposition:extract_basis:finite_generator})]
    Seja
    $B \subset G$
    um subconjunto de $G$ que contém $L$,
    que é linearmente independente,
    e que não pode ser aumentado sem perder essas propriedades.
    Note que $B$ existe porque $G$ é finito
    e $L$ possuí as propriedades listadas:
    $L$ contém $L$ e é linearmente independente.

    Ora,
    se $B$ não pode ser aumentado,
    então,
    do \cref{lemma:extending_a_linear_independent_set},
    \begin{equation*}
      G \subset \generatedsubspace{B}.
    \end{equation*}
    Ou seja,
    $B$ é gerador.
    Como é também linearmente indepentende,
    então $B$ é uma base.
  \end{proof}

  \begin{obs}
    Na demonstração anterior,
    um matemático diria que $B$ é um subconjunto de $G$ que contém $L$ e que é maximal.
    Não dizemos que $B$ é o maior porque existem vários que satisfazem essas condições.
    Por exemplo,
    \begin{align*}
      B_1
      &=
      \set{(1,0), (0,1)}
      \\
      B_2
      &=
      \set{(1,0), (1,1)}
      \\
      B_3
      &=
      \set{(1,0), (5,7)}
    \end{align*}
    são todos linearmente independentes que contém $L = \set{(1,0)}$,
    todos estão contidos em
    $G = \set{(1,0), (0,1), (1,1), (5,7}$
    e nenhum deles pode ser ``aumentado'' sem deixar de ser linearmente independente.
  \end{obs}


  Mesmo com esses resultados anteriores,
  será que não é possível ir ``aumentando'' indefinidamente o conjunto linearmente independente?
  Será que não podemos construir uma sequência infinita de conjuntos lineamente independentes
  \begin{equation*}
    L_1
    \subsetneq
    L_2
    \subsetneq
    \dotsb
    \subsetneq
    L_k
    \subsetneq
    \dotsb?
  \end{equation*}


  \begin{proposition}
    \label{proposition:lineraly_independent_set_is_finite}
    Todo subconjunto
    $L \subset \reals^n$
    que é linearmente independente
    tem no máximo $n$ elementos.
  \end{proposition}

  \begin{proof}
    Se $L$ tiver mais que $n$ elementos,
    podemos escolher $S \subset L$ com exatamente $n + 1$ elementos.
    Então,
    pela \cref{proposition:extract_basis:finite_generator},
    existe uma base $B \subset \reals^n$
    tal que
    \begin{equation*}
      S
      \subset
      B
      \subset
      S \cup \set{\vector{e_1}, \dotsc, \vector{e_n}}.
    \end{equation*}
    Mas sabemos
    da \cref{proposition:two_finite_basis:cardinality}
    que todas as bases finitas de $\reals^n$
    possuem exatamente $n$ elementos.
    Ou seja,
    $B$ tem $n$ elementos e portanto $S$ não pode ter mais que $n$ elementos.
  \end{proof}

  \begin{corollary}
    Se $V \subset \reals^n$
    é um espaço vetorial,
    então todas as bases de $V$ são finitas e tem a mesma cardinalidade (quantidade de elementos).
  \end{corollary}

  \begin{proof}
    Sejam $B_1$ e $B_2$ bases de $V$.
    Então,
    pela \cref{proposition:lineraly_independent_set_is_finite},
    ambos os conjuntos são finitos.
    Agora,
    da \cref{proposition:two_finite_basis:cardinality},
    sabemos que $B_1$ e $B_2$ tem a mesma cardinalidade.
  \end{proof}

  Agora que sabemos que todo espaço vetorial $V \subset \reals^n$
  possuem uma base,
  e que todas são finitas com a mesma cardinalidade,
  podemos definir o que é a \emph{dimensão} do espaço vetorial $V$.

  \begin{definition}
    Se
    $V \subset \reals^n$
    é um espaço vetorial,
    então sua \emph{dimensão} é a cardinalidade de qualquer uma de suas bases.
  \end{definition}
