\section{Determinante da Composta}

  Considere as transformação linear
  \begin{equation*}
    \function{T}{\reals^n}{\reals^n}.
  \end{equation*}
  Usando o determinante,
  podemos construir a transformação multilinear alternada
  \begin{equation*}
    \functionarray{D_T}{\reals^n \times \dotsb \times \reals^n}{\reals}
                  {(\vector{v_1}, \dotsc, \vector{v_n})}{\determinantop(T\vector{v_1}, \dotsc, T\vector{v_n})}.
  \end{equation*}
  Pela \cref{proposition:determinant:unicity},
  sabemos que $D_T$ é um múltiplo de $\determinantop$.

  \begin{lemma}
    \label{lemma:linear_transform_determinant:multiplying_constant}
    A constante $\alpha \in \reals$
    que satisfaz
    $D_T = \alpha \determinantop$
    é
    \begin{equation*}
      \alpha
      =
      \determinantop(T).
    \end{equation*}
    Dito de outra forma,
    \begin{equation*}
      \determinantop(T\vector{v_1}, \dotsc, T\vector{v_n})
      =
      \determinantop(T)
      \determinantop(\vector{v_1}, \dotsc, \vector{v_n}).
    \end{equation*}
  \end{lemma}

  \begin{proof}
    A primeira parte é uma reformulação
    da \cref{proposition:determinant:unicity}.
    Ou seja,
    \begin{align*}
      \alpha
      &=
      D_T(\vector{e_1}, \dotsc, \vector{e_n})
      \\
      &=
      \determinantop(T\vector{e_1}, \dotsc, T\vector{e_n})
      \\
      &=
      \determinantop(T).
    \end{align*}
    Onde a última igualdade nada mais é do que nossa definição
    de $\determinantop(T)$.
    Veja a \cref{definition:determinant}.

    A segunda parte é só uma questão de substituir a definição
    de $\alpha$ e $D_T$.
    \begin{align*}
      \determinantop(T\vector{v_1}, \dotsc, T\vector{v_n})
      &=
      D_T(\vector{v_1}, \dotsc, \vector{v_n})
      \\
      &=
      \determinantop(T)
      \determinantop(\vector{v_1}, \dotsc, \vector{v_n}).
    \end{align*}
  \end{proof}

  O \cref{lemma:linear_transform_determinant:multiplying_constant}
  mostra uma outra maneira de definir o conceito de
  determinante de uma transformação linear.
  O determinante da transformação linear $\function{T}{\reals^n}{\reals^n}$
  é o número $\determinantop(T)$ que satisfaz
  \begin{equation*}
    \determinantop(T\vector{v_1}, \dotsc, T\vector{v_n})
    =
    \determinantop(T)
    \determinantop(\vector{v_1}, \dotsc, \vector{v_n}).
  \end{equation*}
  Em particular,
  se $T$ é sobrejetiva,
  então $\determinantop(T) \neq 0$.
  Para verificar isso,
  basta tomar
  $\vector{v_1}, \dotsc, \vector{v_n}$
  tais que
  $T\vector{v_1} = \vector{e_1}, \dotsc, T\vector{v_n} = \vector{e_n}$,
  para obter
  \begin{equation*}
    1
    =
    \determinantop(T\vector{v_1}, \dotsc, T\vector{v_n})
    =
    \determinantop(T)
    \determinantop(\vector{v_1}, \dotsc, \vector{v_n}).
  \end{equation*}

  Agora,
  com duas transformações lineares
  \begin{equation*}
    \function{S}{\reals^n}{\reals^n}
    \quad\text{e}\quad
    \function{T}{\reals^n}{\reals^n},
  \end{equation*}
  podemos falar em
  $\determinantop(S)$,
  $\determinantop(T)$,
  $\determinantop(S \circ T)$
  e
  $\determinantop(T \circ S)$.

  \begin{proposition}
    Sejam
    \begin{equation*}
      \function{S}{\reals^n}{\reals^n}
      \quad\text{e}\quad
      \function{T}{\reals^n}{\reals^n}
    \end{equation*}
    transformações lineares.
    Então
    \begin{equation*}
      \determinantop(S \circ T)
      =
      \determinantop(S)
      \determinantop(T).
    \end{equation*}
  \end{proposition}

  \begin{proof}
    Basta calcular
    $\determinantop(S \circ T)$.
    Para facilitar o entendimento,
    vamos escrever
    $\vector{v_1} = T\vector{e_1}, \dotsc, \vector{v_n} = T\vector{e_n}$
    e utilizar
    a \cref{proposition:determinant:unicity}.
    \begin{align*}
      \determinantop(S \circ T)
      &=
      \determinantop(S(T\vector{e_1}), \dotsc, S(T\vector{e_n}))
      \\
      &=
      \determinantop(S\vector{v_1}, \dotsc, S\vector{v_n})
      \\
      &=
      \determinantop(S)
      \determinantop(\vector{v_1}, \dotsc, \vector{v_n})
      \\
      &=
      \determinantop(S)
      \determinantop(T\vector{e_1}, \dotsc, \vector{e_n})
      \\
      &=
      \determinantop(S)
      \determinantop(T).
    \end{align*}
  \end{proof}
