\section{Definição}

  Fixado $\vector{a} \in \reals^n$,
  \begin{equation*}
    \functionarray{\alternativeinnerproduct{\vector{a}}{\cdot}}
                  {\reals^n}{\reals}{\vector{v}}{\innerproduct{\vector{a}}{\vector{v}}}
    \quad\text{e}\quad
    \functionarray{\alternativeinnerproduct{\cdot}{\vector{a}}}
                  {\reals^n}{\reals}{\vector{v}}{\innerproduct{\vector{v}}{\vector{a}}}
  \end{equation*}
  são transformações lineares.
  Por isso,
  dizemos que o produto interno
  \begin{equation*}
    \functionarray{\alternativeinnerproduct{\cdot}{\cdot}}
                  {\reals^n \times \reals^n}{\reals}{(\vector{v}, \vector{w})}{\innerproduct{\vector{v}}{\vector{w}}}
  \end{equation*}
  é \emph{bilinear}.

  O determinante $n \times n$
  \begin{equation*}
    \functionarray{\determinantop}
                  {\overbrace{\reals^n \times \dotsb \times \reals^n}^{\text{$n$-vezes}}}{\reals}
                  {(\vector{v}, \vector{w})}{\innerproduct{\vector{v}}{\vector{w}}}
  \end{equation*}
  é uma aplicação $n$-linear.
  Por exemplo,
  fixados
  $\vector{a_2}, \cdots, \vector{a_n} \in \reals^n$,
  a aplicação
  \begin{equation*}
    \functionarray{T}{\reals^n}{\reals}{\vector{v}}{\determinantop(\vector{v}, \vector{a_2}, \cdots, \vector{a_n})}
  \end{equation*}

  \begin{definition}[Transformação Multilinear]
    Dizemos que uma transformação
    \begin{equation*}
      \function{T}{V_1 \times \dotsb \times V_n}{W},
    \end{equation*}
    onde $V_1, \dotsc, V_n$ e $W$ são espaços vetoriais
    é \emph{$n$-linear} quando fixados
    $\vector{a_1} \in V_1, \dotsc, \vector{a_n} \in V_n$,
    cada uma das transformações
    \begin{equation*}
      \functionarray{T_j}{V_j}{W}{\vector{v_j}}
                    {T(\vector{a_1}, \dotsc, \vector{a_{j-1}}, \vector{a_j}, \dotsc, \vector{a_n})}
    \end{equation*}
    é linear.
    Quando não queremos enfatizar o número $n$,
    dizemos apenas que $T$ é \emph{multilinear}.
  \end{definition}

  \begin{example}
    \label{example:multilinear:bilinear_form:transformation}
    Se $\function{T}{\reals^p}{\reals^n}$ é linear,
    então
    \begin{equation*}
      \functionarray{J}{\reals^n \times \reals^p}{\reals}{(\vector{v}, \vector{w})}{\innerproduct{\vector{v}}{(T\vector{w})}}
    \end{equation*}
    é bilinear.
  \end{example}

  \begin{example}
    \label{example:multilinear:bilinear_form:matrix}
    Vamos identificar as matrizes $1 \times 1$ com o conjunto dos números reais.
    Seja $V$ o conjunto das matrizes $1 \times n$,
    e $W$ o das matrizes $p \times 1$.
    Se $M$ é uma matriz $n \times p$,
    então
    \begin{equation*}
      \functionarray{J}{V \times W}{\reals}
                    {(A, B)}{A M B}
    \end{equation*}
    é bilinear.
    De fato,
    essa transformação é essencialmente a mesma
    do \cref{example:multilinear:bilinear_form:transformation}.
  \end{example}

  O determinanter também tem a propriedade de ser uma transformação linear \emph{alternada}.

  \begin{definition}[Transformação multilinear alternada]
    \label{definition:alternate_multilinear}
    Se $\function{T}{V^n}{W}$ é uma transformação $n$-linear,
    dizemos que $T$ é \emph{alternada} quando para qualquer permutação
    $\sigma \in P_n$,
    \begin{equation*}
      T(\vector{a_{\sigma(1)}}, \vector{a_{\sigma(2)}}, \dotsc, \vector{a_{\sigma(n)}})
      =
      \sign{\sigma}
      T(\vector{a_{1}}, \vector{a_{2}}, \dotsc, \vector{a_{n}}).
    \end{equation*}
  \end{definition}

  A \cref{definition:alternate_multilinear}
  é mais fácilmente compreendida se dissermos simplesmente que
  \begin{quote}
    quando trocamos dois vetores de lugar,
    o sinal de $T$ muda.
  \end{quote}

  \begin{example}[O determinante é alternado]
    Sejam
    $\vector{a_j} = (a_{j1}, \dotsc, a_{jn}) \in \reals^n$
    e
    $\sigma \in P_n$.
    Então,
    \begin{align*}
      \determinantop(\vector{a_{\sigma(1)}}, \dotsc, \vector{a_{\sigma(n)}})
      &=
      \sum_{\gamma \in P_n}
      \sign{\gamma}
      a_{\gamma(\sigma(1))} \dotsb a_{\gamma(\sigma(n))}
      \\
      &=
      \sign{\sigma}^{-1}
      \sum_{\gamma \in P_n}
      \sign{\gamma}
      \sign{\sigma}
      a_{\gamma(\sigma(1))} \dotsb a_{\gamma(\sigma(n))}
      \\
      &=
      \sign{\sigma}^{-1}
      \sum_{\gamma \in P_n}
      \sign{\gamma \circ \sigma}
      a_{(\gamma \circ \sigma)(1)} \dotsb a_{(\gamma \circ \sigma)(n)}
      \\
      &=
      \sign{\sigma}^{-1}
      \sum_{\beta \in P_n}
      \sign{\beta}
      a_{\beta(1)} \dotsb a_{\beta(n)}
      \\
      &=
      \sign{\sigma}^{-1}
      \determinantop(\vector{a_1}, \dotsc, \vector{a_n})
      \\
      &=
      \sign{\sigma}
      \determinantop(\vector{a_1}, \dotsc, \vector{a_n}).
    \end{align*}
  \end{example}

  Ao falar de transformações multilineares alternadas,
  tem gente que gosta de dizer que
  \begin{quote}
    ``quando trocamos dois vetores, o sinal muda''.
  \end{quote}
  Tem gente que prefere dizer que
  \begin{quote}
    ``quando permutamos os vetores,
    o valor da transformação é multiplicado pelo sinal da permutação''.
  \end{quote}
  Existem maneira diferentes de caracterizar o significa uma transformação linear ser alternada.
  Qualquer uma dessas caracterizações poderia ter sido usada no lugar
  da \cref{definition:alternate_multilinear}.

  \begin{proposition}
    \label{proposition:alternate_linear:characterizations}
    Dada uma transformação $n$-linear
    $\function{T}{V^n}{W}$.
    São equivalentes
    \begin{enumerate}
      \item
        \label{item:proposition:alternate_linear:characterizations:permutation}
        Se $\sigma \in P_n$,
        \begin{equation*}
          T(\vector{a_{\sigma(1)}}, \vector{a_{\sigma(2)}}, \dotsc, \vector{a_{\sigma(n)}})
          =
          \sign{\sigma}
          T(\vector{a_{1}}, \vector{a_{2}}, \dotsc, \vector{a_{n}}).
        \end{equation*}

      \item
        \label{item:proposition:alternate_linear:characterizations:transposition}
        Ao alternarmos a posição de dois vetores em
        \begin{equation*}
          T(\vector{a_{1}}, \vector{a_{2}}, \dotsc, \vector{a_{n}}),
        \end{equation*}
        o ``resultado'' é multiplicado por $-1$.

      \item
        \label{item:proposition:alternate_linear:characterizations:dependence}
        Se os vetores
        $\vector{a_{1}}, \vector{a_{2}}, \dotsc, \vector{a_{n}}$
        são linearmente dependentes,
        então
        \begin{equation*}
          T(\vector{a_{1}}, \vector{a_{2}}, \dotsc, \vector{a_{n}})
          =
          0.
        \end{equation*}

      \item
        \label{item:proposition:alternate_linear:characterizations:repeated_vector}
        Se dois vetores em
        $\vector{a_{1}}, \vector{a_{2}}, \dotsc, \vector{a_{n}}$
        são iguais,
        então
        \begin{equation*}
          T(\vector{a_{1}}, \vector{a_{2}}, \dotsc, \vector{a_{n}})
          =
          0.
        \end{equation*}
    \end{enumerate}
  \end{proposition}

  \begin{proof}
  \end{proof}
