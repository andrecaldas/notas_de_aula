\begin{tikzpicture}
  [
    caption =
    {%
      $P_{\vector{w}}$ é uma ``projeção \underline{com sinal}''.
      A \emph{direção} do vetor $\vector{w}$,
      nos dá o \emph{``tamanho da projeção''}.
      E o \emph{sentido} nos dá o sinal.
    }
  ]


  \coordinate (O) at (0,0);
  \coordinate (v) at (50:5);
  \coordinate (u) at (170:5);
  \coordinate (w) at (10:6);
  \coordinate (v projection) at ($(O)!(v)!(w)$);
  \coordinate (u projection) at ($(O)!(u)!(w)$);

  % Straight line
  \draw
    ($(O)!-5cm!(w)$)
    --
    ($(O)!7cm!(w)$);


  % Angle
  \draw
    (O) pic
    [
      draw, red, ->, thick,
      fill=green!10,
      angle radius=2cm,
      pic text={$\theta$},
    ]{angle=w--O--v};


  % Vectors
  \draw[vector]
    (O) --node[above, sloped]{$\vector{v}$} (v);
  \draw[vector]
    (O) --node[above]{$\vector{u}$} (u);
  \draw[vector, blue]
    (O) --node[above, near end]{$\vector{w}$} (w);


  % Projections
  \draw[help lines, dashed]
    (u) -- ($(u projection)!-1.4cm!(u)$)
    (v) -- ($(v projection)!-1.4cm!(v)$)
    ($(v) - (v projection)$) -- ($(v projection)!-1.4cm!(v) - (v projection)$);


  \draw[signed extension line]
    ($(v projection)!-1.1cm!(v) - (v projection)$)
    -- node[above,sloped] {$P_{\vector{w}}(\vector{v}) > 0$}
    ($(v projection)!-1.1cm!(v)$);

  \draw[signed extension line]
    ($(u projection)!-1.2cm!(u) - (u projection)$)
    -- node[above,sloped] {$P_{\vector{w}}(\vector{u}) < 0$}
    ($(u projection)!-1.2cm!(u)$);
\end{tikzpicture}
