\subsection{Plano e Hiperplano em $\reals^n$}

  Em $\reals^3$,
  um plano pode ser definido de diversas maneiras:
  \begin{enumerate}
    \item
      \label{item:plane:three_points}
      O plano que passa por três pontos distintos dados
      $\vector{a}$, $\vector{b}$ e $\vector{c}$.

    \item
      \label{item:plane:two_directions}
      O plano que passa no ponto $\vector{p}$
      e que é tangente às direções $\vector{v}$ e $\vector{w}$.

    \item
      \label{item:plane:normal}
      O plano que passa no ponto $\vector{p}$
      e que é \emph{normal} à direção dada pelo vetor $\vector{n}$.
  \end{enumerate}

  Os \cref{item:plane:three_points,item:plane:two_directions}
  são facilmente intercambiáveis.
  De fato,
  fazendo
  \begin{equation*}
    \vector{p}
    =
    \vector{a},
    \quad
    \vector{b}
    =
    \vector{a} + \vector{v}
    \quad\text{e}\quad
    \vector{c}
    =
    \vector{a} + \vector{w},
  \end{equation*}
  passamos
  do \cref{item:plane:two_directions}
  ao \cref{item:plane:three_points}.
  Podemos voltar fazendo
  \begin{equation*}
    \vector{a}
    =
    \vector{p},
    \quad
    \vector{v}
    =
    \vector{b} - \vector{a}
    \quad\text{e}\quad
    \vector{w}
    =
    \vector{c} - \vector{a}.
  \end{equation*}

  Com o ponto $\vector{a}$
  e as direções
  $\vector{v}$ e $\vector{w}$,
  o plano é dado pelo conjunto
  \begin{equation}
    \label{equation:plane:two_directions}
    P
    =
    \setsuchthat{\vector{a} + s\vector{v} + t\vector{w}}{s,t \in \reals}.
  \end{equation}
  Essa construção pode ser feita também em outras dimensões.
  Não precisamos nos restringir a $\reals^3$.
  Em qualquer $\reals^n$,
  se $\vector{v}, \vector{w} \in \reals^n$ são linearmente independentes
  e $\vector{a} \in \reals^n$ é um vetor qualquer,
  então
  a \cref{equation:plane:two_directions}
  define um plano em $\reals^n$.

  \begin{example}
    O conjunto
    \begin{equation*}
      P
      =
      \setsuchthat{(s,0,t,0)}{s,t \in \reals}
    \end{equation*}
    é um plano em $\reals^4$.
    É o que provavelmente chamaríamos de
    \emph{plano x-z} em $\reals^4$.
  \end{example}

  Por outro lado,
  o \cref{item:plane:normal}
  pode ser usado em $\reals^n$ para falar de um \emph{hiperplano}.
  Dado um ponto qualquer em $\vector{p} \in \reals^n$
  e uma direção não nula $\vector{n} \in \reals^n$,
  o \emph{hiperplano} que passa em $\vector{p}$ e é normal ao vetor $\vector{n}$
  é o conjunto
  \begin{align*}
    H
    &=
    \setsuchthat{\vector{p} + \vector{v}}{\vector{v} \perp \vector{n}}.
    \\
    &=
    \setsuchthat{\vector{p} + \vector{v}}{\vector{v} \in \reals^n,\, \innerproduct{\vector{v}}{\vector{n}}}.
  \end{align*}
  Ou então,
  \begin{align*}
    H
    &=
    \setsuchthat{\vector{q} \in \reals^n}{\innerproduct{(\vector{q}-\vector{p})}{\vector{n}} = 0}
    \\
    &=
    \setsuchthat{\vector{q} \in \reals^n}{\innerproduct{\vector{q}}{\vector{n}} = \innerproduct{\vector{p}}{\vector{n}}}.
  \end{align*}
  Em $\reals^3$,
  \emph{plano} e \emph{hiperplano} é a mesma coisa.

  \begin{example}[Plano em $\reals^3$]
    O plano $P$ que passa em $(1,2,3)$ e é normal ao vetor $(4,5,6)$
    é formado pelos pontos $(x,y,z)$ que satisfazem a equação
    \begin{equation*}
      (x,y,z) \cdot (4,5,6)
      =
      (1,2,3) \cdot (4,5,6).
    \end{equation*}
    Ou seja,
    \begin{equation*}
      (x,y,z) \in P
      \Leftrightarrow
      4x + 5y + 6z
      =
      32.
    \end{equation*}
  \end{example}

  Em $\reals^2$,
  um hiperplano é o mesmo que uma reta.

  \begin{example}[A reta em $\reals^2$]
    \label{example:hyperplane:line}
    Em $\reals^2$,
    a reta
    \begin{equation*}
      r
      =
      \setsuchthat{(1,2) + t(3,4)}{t \in \reals}
    \end{equation*}
    pode ser vista como a reta que passa no ponto $(1,2)$ e é normal ao vetor $(-4,3)$.

    De fato,
    se $s$ é a reta que passa em $(1,2)$ e é normal a $(-4,3)$,
    então
    \begin{align*}
      s
      &=
      \setsuchthat{(x,y) \in \reals^2}{\innerproduct{(x,y)}{(-4,3)} = \innerproduct{(1,2)}{(-4,3)}}
      \\
      &=
      \setsuchthat{(x,y) \in \reals^2}{-4x + 3y = 2}
      \\
      &=
      \setsuchthat{(x,y) \in \reals^2}{-4x + 3y = 2}
      \\
      &=
      \setsuchthat{(x,y) \in \reals^2}{y = \frac{2}{3} + \frac{4}{3}x}
      \\
      &=
      \setsuchthat{\left(0, \frac{2}{3}\right) + t\left(1,\frac{4}{3}\right)}{t \in \reals}
      \\
      &=
      \setsuchthat{\left(0, \frac{2}{3}\right) + \left(1,\frac{4}{3}\right) + (t-1)\left(1,\frac{4}{3}x\right)}{t \in \reals}
      \\
      &=
      \setsuchthat{(1,2) + \frac{t-1}{3}(3,4)}{t \in \reals}
      \\
      &=
      \setsuchthat{(1,2) + s(3,4)}{s \in \reals}
      \\
      &=
      r.
    \end{align*}
  \end{example}
