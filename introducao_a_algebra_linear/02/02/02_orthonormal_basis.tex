\section{Bases Ortonormais}

  Se ao invés da base canônica
  $\vector{e_1}, \dotsc, \vector{e_n} \in \reals^n$
  usarmos uma outra base
  $\vector{b_1}, \dotsc, \vector{b_n} \in \reals^n$
  formada por vetores de norma $1$
  e ortogonais entre si,
  podemos usar o produto interno para facilmente determinar
  como podemos escrever um vetor qualquer $\vector{a}$
  na base
  $\vector{b_1}, \dotsc, \vector{b_n} \in \reals^n$.

  \begin{definition}[Base Ortonormal]
    Seja $V$ um espaço vetorial e
    $\vector{b_1}, \dotsc, \vector{b_n} \in V$
    uma base de $V$ é \emph{ortonormal} quando é formada por vetores de norma $1$
    que são ortogonais entre si.
    Em outras palavras,
    é uma base que satisfaz
    \begin{equation*}
      \innerproduct{\vector{b_j}}{\vector{b_k}}
      =
      \begin{cases}
        0, &j \neq k
        \\
        1, &j = k
      \end{cases}.
    \end{equation*}
  \end{definition}

  Geometricamente é bastante intuitivo que se quisermos escrever
  $\vector{a}$ na base ortonormal
  $\vector{b_1}, \dotsc, \vector{b_n} \in \reals^n$,
  basta fazer
  \begin{equation*}
    \vector{a}
    =
    \alternativeinnerproduct{\vector{a}}{\vector{b_1}}
    \vector{b_1}
    +
    \dotsb
    +
    \alternativeinnerproduct{\vector{a}}{\vector{b_n}}
    \vector{b_n}.
  \end{equation*}
  Mas deixando a geometria de lado,
  podemos usar as propriedades do produto interno
  para verificar isso.
  De fato,
  como
  $\vector{b_1}, \dotsc, \vector{b_n} \in \reals^n$
  é uma base,
  podemos escrever $\vector{a}$ como combinação linear desses vetores:
  \begin{equation*}
    \vector{a}
    =
    \alpha_1
    \vector{b_1}
    +
    \dotsb
    +
    \alpha_n
    \vector{b_n}.
  \end{equation*}
  Basta então encontrar $\alpha_1, \dotsc, \alpha_n \in \reals$.
  Para tanto,
  vamos fazer o produto interno de $\vector{a}$ com $\vector{b_j}$:
  \begin{align*}
    \innerproduct{\vector{a}}{\vector{b_j}}
    &=
    \innerproduct
    {
      \alpha_1
      \vector{b_1}
      + \dotsb +
      \alpha_n
      \vector{b_n}
    }{\vector{b_j}}
    \\
    &=
    \alpha_1 \innerproduct{\vector{b_1}}{\vector{b_j}}
    + \dotsb +
    \alpha_n \innerproduct{\vector{b_n}}{\vector{b_j}}
    \\
    &=
    \alpha_j \innerproduct{\vector{b_j}}{\vector{b_j}}
    \\
    &=
    \alpha_j,
  \end{align*}
  onde as últimas igualdades são consequência do fato de
  $\innerproduct{\vector{b_k}}{\vector{b_j}}$
  ser igual a $0$ quando $k \neq j$
  e igual a $1$ quando $k = j$.
  Portanto,
  para encontrarmos $\alpha_j$,
  basta calcularmos
  $\innerproduct{\vector{a}}{\vector{b_j}}$.

  \begin{obs}
    Sem geometria alguma,
    a conclusão do último parágrafo decorre apenas das propriedades básicas do produto interno.
    Por isso,
    na \cref{definition:canonical_inner_product},
    chamamos nosso produto interno de \emph{produto interno \textbf{canônico}}.
    Existem outros produtos internos que possuem as mesmas propriedades
    e podem ser usados de modo similar.
  \end{obs}
