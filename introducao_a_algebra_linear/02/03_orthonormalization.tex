\chapter{Processo de Ortonormalização de Gram–Schmidt}

  Em $\reals^n$,
  temos a base ortonormal
  $\vector{e_1}, \dotsc, \vector{e_n}$.
  Mas se estivermos em um espaço vetorial que não seja $\reals^n$?
  Será que conseguimos uma base ortonormal para esse espaço?

  Suponha que
  $\vector{b_1}, \dotsc, \vector{b_n} \in V$
  seja uma base para $V$.
  Vamos usar o produto interno para construir uma base ortonormal
  $\vector{a_1}, \dotsc, \vector{a_n} \in V$.

  Primeiro,
  faça
  \begin{equation*}
    \vector{a_1}
    =
    \frac{\vector{b_1}}{\norm{\vector{b_1}}}.
  \end{equation*}
  Escolhido
  $\vector{a_1}$,
  podemos tomar
  $\vector{b_2}$
  e remover a componente $\vector{a_1}$ para ter um vetor ortogonal a $\vector{a_1}$
  \begin{equation*}
    \vector{n}
    =
    \vector{b_2}
    -
    (\innerproduct{\vector{b_2}}{\vector{a_1}})
    \vector{a_1}.
  \end{equation*}
  Agora,
  é só normalizá-lo para obter
  \begin{align*}
    \vector{a_2}
    &=
    \frac
    {
      \vector{b_2}
      -
      (\innerproduct{\vector{b_2}}{\vector{a_1}})
      \vector{a_1}
    }
    {
      \norm
      {
        \vector{b_2}
        -
        (\innerproduct{\vector{b_2}}{\vector{a_1}})
        \vector{a_1}
      }
    }
    \\
    &=
    \frac
    {
      \vector{b_2}
      -
      (\innerproduct{\vector{b_2}}{\vector{a_1}})
      \vector{a_1}
    }
    {
      \sqrt
      {
        \norm{\vector{b_2}}^2
        -
        (\innerproduct{\vector{b_2}}{\vector{a_1}})^2
      }
    }.
  \end{align*}
  Podemos então continuar o processo e fazer
  \begin{align*}
    \vector{a_3}
    &=
    \frac
    {
      \vector{b_3}
      -
      (\innerproduct{\vector{b_3}}{\vector{a_1}})
      \vector{a_1}
      -
      (\innerproduct{\vector{b_3}}{\vector{a_2}})
      \vector{a_2}
    }
    {
      \norm
      {
        \vector{b_3}
        -
        (\innerproduct{\vector{b_3}}{\vector{a_1}})
        \vector{a_1}
        -
        (\innerproduct{\vector{b_3}}{\vector{a_2}})
        \vector{a_2}
      }
    }
    \\
    &=
    \frac
    {
      \vector{b_3}
      -
      (\innerproduct{\vector{b_3}}{\vector{a_1}})
      \vector{a_1}
      -
      (\innerproduct{\vector{b_3}}{\vector{a_2}})
      \vector{a_2}
    }
    {
      \sqrt
      {
        \norm{\vector{b_3}}^2
        -
        (\innerproduct{\vector{b_3}}{\vector{a_1}})^2
        -
        (\innerproduct{\vector{b_3}}{\vector{a_2}})^2
      }
    }.
  \end{align*}
  De um modo geral,
  se fizermos
  \begin{align*}
    \vector{a_j}
    &=
    \frac
    {
      \vector{b_j}
      -
      \sum_{k=1}^{j-1}
      (\innerproduct{\vector{b_j}}{\vector{a_k}})
      \vector{a_k}
    }
    {
      \norm
      {
        \vector{b_j}
        -
        \sum_{k=1}^{j-1}
        (\innerproduct{\vector{b_j}}{\vector{a_k}})
        \vector{a_k}
      }
    }
    \\
    &=
    \frac
    {
      \vector{b_j}
      -
      \sum_{k=1}^{j-1}
      (\innerproduct{\vector{b_j}}{\vector{a_k}})
      \vector{a_k}
    }
    {
      \sqrt
      {
        \norm{\vector{b_j}}^2
        -
        \sum_{k=1}^{j-1}
        (\innerproduct{\vector{b_j}}{\vector{a_k}})^2
      }
    },
  \end{align*}
  obtemos uma base ortonormal
  $\vector{a_1}, \dotsc, \vector{a_n}$.
  Esse é o \emph{processo de ortonormalização de Gram–Schmidt}.

  \begin{example}
    Se fizermos o processo de ortonormalização para a base de $\reals^4$
    \begin{align*}
      \vector{b_1} = (1,0,0,0)
      \quad&\quad
      \vector{b_2} = (1,1,0,0)
      \\
      \vector{b_3} = (1,1,1,0)
      \quad&\quad
      \vector{b_4} = (1,1,1,1),
    \end{align*}
    teremos a base ortonormal canônica
    \begin{align*}
      \vector{a_1} = (1,0,0,0)
      \quad&\quad
      \vector{a_2} = (0,1,0,0)
      \\
      \vector{a_3} = (0,0,1,0)
      \quad&\quad
      \vector{a_4} = (0,0,0,1).
    \end{align*}
    Mas se tomarmos a mesma base na ordem inversa:
    \begin{align*}
      \vector{b_1} = (1,1,1,1)
      \quad&\quad
      \vector{b_2} = (1,1,1,0)
      \\
      \vector{b_3} = (1,1,0,0)
      \quad&\quad
      \vector{b_4} = (1,0,0,0),
    \end{align*}
    obteremos
    \begin{align*}
      \vector{a_1}
      &=
      \frac{1}{2}
      (1,1,1,1)
      &
      \vector{a_2}
      &=
      \frac{1}{2\sqrt{3}}
      (1, 1, 1, -3)
      \\
      \vector{a_3}
      &=
      \frac{1}{\sqrt{6}}
      (1, 1, -2, 0)
      &
      \vector{a_4}
      &=
      \frac{1}{\sqrt{2}}
      (1, -1, 0, 0).
    \end{align*}
  \end{example}

  {\input{02/03/exercicios}}
